<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Backend Routes

Route::get('ea-xpanel', 'admin\Dashboard@index');
Route::any('ea-xpanel/register', 'admin\User@register');

Route::any('ea-xpanel/courses/{nom?}', 'admin\Course@index');
Route::any('ea-xpanel/plan/{nom?}', 'admin\Plan@index');

Route::any('ea-xpanel/candidate', 'admin\Candidate@index');
Route::any('ea-xpanel/candidate/add/{nom?}', 'admin\Candidate@add');
Route::any('ea-xpanel/user/profile', 'admin\Candidate@profile');

Route::any('ea-xpanel/test', 'admin\Test@index');
Route::any('ea-xpanel/test/status/{status}/{id}', 'admin\Test@status');
Route::any('ea-xpanel/test/add/{nom?}', 'admin\Test@add');
Route::any('ea-xpanel/test/test_questions/{nom}', 'admin\Test@test_questions');
Route::any('ea-xpanel/test/view_questions/{nom}', 'admin\Test@view_questions');
Route::any('ea-xpanel/test_details/{nom}', 'admin\Test@ctest');
Route::any('ea-xpanel/instructions/{nom?}', 'admin\Test@instruction');

Route::any('ea-xpanel/online-exam/{slug}', 'admin\Exam@index');
Route::any('ea-xpanel/feedback/{slug}', 'admin\Exam@feedback');
Route::any('ea-xpanel/result/{uid}/{tid}', 'admin\Exam@result');
Route::any('ea-xpanel/exams/', 'admin\Exam@tests');

Route::any('ea-xpanel/instruction', 'admin\Instructions@index');
Route::any('ea-xpanel/instruction/add/{nom?}', 'admin\Instructions@add');

Route::any('ea-xpanel/subject/{nom?}', 'admin\Subject@index');
Route::any('ea-xpanel/subject/copy/{nom}', 'admin\Subject@copy');

Route::any('ea-xpanel/topic/{nom?}', 'admin\Topic@index');

Route::any('ea-xpanel/category/{nom?}', 'admin\news_category@index');

Route::any('ea-xpanel/question', 'admin\Question@index');
Route::any('ea-xpanel/question/add/{nom?}', 'admin\Question@add');
Route::any('ea-xpanel/question/copy/{nom?}', 'admin\Question@copy');

Route::any('ea-xpanel/paragraph', 'admin\Paragraph@index');
Route::any('ea-xpanel/paragraph/add/{nom?}', 'admin\Paragraph@add');

Route::any('ea-xpanel/blog', 'admin\Blog@index');
Route::any('ea-xpanel/blog/add/{nom?}', 'admin\Blog@add');

Route::any('ea-xpanel/page', 'admin\Pages@index');
Route::any('ea-xpanel/page/add/{nom?}', 'admin\Pages@add');

Route::any('ea-xpanel/news', 'admin\News@index');
Route::any('ea-xpanel/news/add/{nom?}', 'admin\News@add');

Route::any('ea-xpanel/reading', 'admin\Reading@index');
Route::any('ea-xpanel/reading/add/{nom?}', 'admin\Reading@add');

Route::any('ea-xpanel/website-settings', 'admin\Setting@index');
Route::any('ea-xpanel/general-settings', 'admin\Setting@general');
Route::any('ea-xpanel/change-password', 'admin\User@change_password');
Route::any('ea-xpanel/logout', 'admin\User@logout');


Route::post('ea-xpanel/ajax/user_login', 'admin\Ajax@user_login');
Route::post('ea-xpanel/ajax/upload-image', 'admin\Ajax@upload_image');
Route::post('ea-xpanel/ajax/{action}', 'admin\Ajax@index');

// Frontend Routes
Route::get('/', function() {
    return '<center><h1>Welcome To Online Exam</h1></center>';
});
// Route::get('/', 'HomeController@index');
// Route::get('contact', 'ContactController@index');
// Route::get('blog/', 'BlogController@index');
// Route::any('test/{nom}', 'Test@index');
// Route::any('bank/', 'Test@bank');
// Route::get('blog/{slug}', 'BlogController@single');
// Route::get('page/{slug}', 'PagesController@index');
// Route::get('news/{slug}', 'NewsController@index');
// Route::get('news/single/{slug}', 'NewsController@single');
// Route::get('english', 'EnglishController@index');
// Route::get('question/{course}/{subject}/{topic}', 'Question@index');
// Route::any('exam', 'ExamController@index');
// Route::get('reading', 'Reading@index');
// Route::get('{course_slug}/{subject_slug}', 'Topic@index');
