<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('course', 'api\CourseController@index');
Route::post('user/login', 'api\UserController@login');
Route::post('user/register', 'api\UserController@register');
Route::post('subject', 'api\SubjectController@index');
Route::get('profile', 'api\ProfileController@index');
Route::post('topic', 'api\TopicController@index');
Route::post('question', 'api\QuestionController@index');