<?php

namespace App\Model;
use DB;

use Illuminate\Database\Eloquent\Model;

class SubjectModel extends Model {
    protected $table 		= "subjects";
    protected $primaryKey 	= "subject_id";
}
