<?php

namespace App\Model;
use DB;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model {
    protected $table 		= "users";
    protected $primaryKey 	= "user_id";
}
