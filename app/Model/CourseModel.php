<?php

namespace App\Model;
use DB;

use Illuminate\Database\Eloquent\Model;

class CourseModel extends Model {
    protected $table 		= "courses";
    protected $primaryKey 	= "course_id";
}
