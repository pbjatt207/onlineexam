<?php

namespace App\Model;
use DB;

use Illuminate\Database\Eloquent\Model;

class TopicModel extends Model {
    protected $table 		= "topics";
    protected $primaryKey 	= "topic_id";
}
