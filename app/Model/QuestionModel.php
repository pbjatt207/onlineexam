<?php

namespace App\Model;
use DB;

use Illuminate\Database\Eloquent\Model;

class QuestionModel extends Model {
    protected $table 		= "questions";
    protected $primaryKey 	= "question_id";
}
