<?php

namespace App\Model;
use DB;

use Illuminate\Database\Eloquent\Model;

class ProfileModel extends Model {
    protected $table 		= "candidates";
    protected $primaryKey 	= "candidate_id";
}
