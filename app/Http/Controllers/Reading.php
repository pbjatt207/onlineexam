<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

class Reading extends BaseController {
    public function index(Request $request) {

    	$title     = "Reading | English Aliens";

        $records   = DB::table('readings')
                        ->where('reading_is_deleted','N')
                        ->where('reading_is_visible','Y')
                        ->paginate(1);

    	$page      = "reading";
    	$data      = compact('page', 'title', 'records');
    	return view('frontend/layout', $data);
    }
}

