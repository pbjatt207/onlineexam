<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ContactController extends BaseController
{
    public function index() {
    	$title = "Contact Us | English Aliens";
    	$page = "contact";
    	$data = compact('page', 'title');
    	return view('frontend/layout', $data);
    }
}
