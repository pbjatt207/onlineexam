<?php
namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use App\Query;

class Topic extends BaseController {
    public function index( Request $request, $id = NULL ) {
        $q = new Query();
        $search = $request->input('search');
    	if ($request->isMethod('post')) {
    		$input = $request->input('record');
            $tname = $input['topic_name'];
            $tcourse = $input['topic_course'];
            $tsubject = $input['topic_subject'];
            $exist = DB::table('topics')
                    ->where('topic_name', $tname)
                    ->where('topic_course', $tcourse)
                    ->where('topic_course', $tcourse)
                    ->where('topic_subject', $tsubject)
                    ->where('topic_is_deleted', 'N')
                    ->first();

            if(!empty($input)) {
        		$input['topic_slug']      = \Str::slug($input['topic_name'], '-');
                $input['topic_is_para']   = !empty($input['topic_is_para']) ? $input['topic_is_para'] : "N";

                if(empty($id)) {
                    if (empty($exist)) {
            		    DB::table('topics')->insert( $input );
                        $id = DB::getPdo()->lastInsertId();
                        $mess = "Data inserted.";
                    } else {
                        $mess = "Data already exist.";
                    }
                } else {
                    DB::table('topics')->where('topic_id', $id)->update( $input );
                    $mess = "Data updated";
                }

                $slug = $q->create_slug($input['topic_name'], "topics", "topic_slug", "topic_id", $id);
                DB::table('topics')->where('topic_id', $id)->update( array('topic_slug' => $slug) );

        		return redirect()->back()->with('success', $mess);
            }

            $check = $request->input('check');

            if(!empty($check)) {
                $arr = array(
                    "topic_is_deleted" => "Y"
                );
                DB::table('topics')->whereIn('topic_id', $check)->update( $arr );

                return redirect()->back()->with('success', 'Selected record(s) deleted.');
            }
	    }

        $page_no = $request->input('page');
        $offset  = !empty($page_no) ? $page_no - 1 : 0;
	    $query = DB::table('topics')
                    ->join('courses', 'topics.topic_course', '=', 'courses.course_id')
                    ->join('subjects', 'topics.topic_subject', '=', 'subjects.subject_id')
                    ->select('topics.*','courses.course_name', 'subjects.subject_name')
                    ->where('topic_is_deleted','N');

          $search = array();
          if(!empty($request->input('search'))) {
              $search = $request->input('search');
              if(!empty($search['keyword'])) {
                  $query->where('topic_name', 'LIKE', '%'.$search['keyword'].'%');
              }

              if(!empty($search['topic_course'])) {
                  $query->where('topic_course', $search['topic_course']);
              }

              if(!empty($search['topic_subject'])) {
                  $query->where('topic_subject', $search['topic_subject']);
              }
          }

  	    $records = $query->paginate(10);

        $courses = DB::table('courses')->where('course_is_deleted', 'N')->get();
        $edit = $subjects = $topics = array();
        if(!empty($id)) {
            $edit       = DB::table('topics')->where('topic_id',$id)->first();
            
            if(!empty($edit->topic_course)) {
                $subjects = DB::table('subjects')->where('subject_course', $edit->topic_course)->where('subject_is_deleted','N')->get();
                $topics = DB::table('topics')->where('topic_subject', $edit->topic_subject)->where('topic_is_deleted','N')->get();
            }
        }

    	$page 	= "topic";          
    	$data 	= compact('page', 'records', 'offset', 'courses', 'subjects', 'edit', 'topics', 'search');
    	return view('backend/layout', $data);
    }
}
