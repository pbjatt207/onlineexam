<?php
namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Query;
use DB;
use Hash;

class Pages extends BaseController {
    public function index( Request $request, $id = NULL ) {
    	

        $page_no = $request->input('page');
        $offset  = !empty($page_no) ? $page_no - 1 : 0;
	    $records = DB::table('pages')->paginate(10);
	    
    	$page 	= "pages";
    	$data 	= compact('page', 'records', 'offset');
    	return view('backend/layout', $data);
    }

    public function add( Request $request, $id = NULL ) {
        $q = new Query();

        if ($request->isMethod('post')) {
            $input = $request->input('record');

            if(!empty($input)) {
                if(empty($id)) {
                    DB::table('pages')->insert( $input );
                    $id = DB::getPdo()->lastInsertId();
                    $mess = "Data inserted.";
                } else {
                    DB::table('pages')->where('page_id', $id)->update( $input );
                    $mess = "Data updated";
                }

                $slug = $q->create_slug($input['page_title'], "pages", "page_slug", "page_id", $id);
                DB::table('pages')->where('page_id', $id)->update( array('page_slug' => $slug) );

                if ($request->hasFile('page_image')) {
                    if(!empty($edit->page_image) && file_exists(public_path().'/imgs/pages/'.$edit->page_image)) {
                        unlink(public_path().'/imgs/pages/'.$edit->page_image);
                    }
                    $image           = $request->file('page_image');
                    $name            = $id.'.'.$image->getClientOriginalExtension();
                    $destinationPath = 'public/imgs/pages';
                    $image->move($destinationPath, $name);

                    if(!empty($edit->page_image)) {
                        $name .= "?v=".uniqid();
                    }

                    DB::table('pages')->where('page_id', $id)->update( array('page_image' => $name) );
                }

                return redirect()->back()->with('success', $mess);
            }
        }

        $page_no = $request->input('page');
        $offset  = !empty($page_no) ? $page_no - 1 : 0;
        $records = DB::table('pages')->paginate(10);

        $edit = $subjects = $topics = array();
        if(!empty($id)) {
            $edit = DB::table('pages')->where('page_id',$id)->first();
        }
        
        $page   = "edit_pages";
        $data   = compact('page', 'records', 'offset', 'edit');
        return view('backend/layout', $data);
    }
}
