<?php
namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use Hash;

class Paragraph extends BaseController {
    public function index( Request $request, $id = NULL ) {
    	if ($request->isMethod('post')) {
            $check = $request->input('check');

            if(!empty($check)) {
                $arr = array(
                    "para_is_deleted" => "Y"
                );
                DB::table('paragraphs')->whereIn('para_id', $check)->update( $arr );

                return redirect()->back()->with('success', 'Selected record(s) deleted.');
            }
	    }

        $page_no = $request->input('page');
        $offset  = !empty($page_no) ? $page_no - 1 : 0;
	    $records = DB::table('paragraphs')
                    ->join('courses', 'paragraphs.para_course', '=', 'courses.course_id')
                    ->join('subjects', 'paragraphs.para_subject', '=', 'subjects.subject_id')
                    ->join('topics', 'paragraphs.para_topic', '=', 'topics.topic_id')
                    ->select('paragraphs.*','courses.course_name', 'subjects.subject_name', 'topics.topic_name')
                    ->where('para_is_deleted','N')
                    ->paginate(10);
	    
    	$page 	= "paragraph";
    	$data 	= compact('page', 'records', 'offset');
    	return view('backend/layout', $data);
    }

    public function add( Request $request, $id = NULL ) {

        if ($request->isMethod('post')) {
            $input    = $request->input('record');
            $pname    = $input['para_name'];
            $pdesc    = $input['para_description'];
            $pcourse  = $input['para_course'];
            $psubject = $input['para_subject'];
            $ptopic   = $input['para_topic'];
            $exist    = DB::table('paragraphs')
                        ->where('para_name', $pname)
                        ->where('para_description', $pdesc)
                        ->where('para_course', $pcourse)
                        ->where('para_subject', $psubject)
                        ->where('para_topic', $ptopic)
                        ->where('para_is_deleted', 'N')
                        ->first();

            if(!empty($input)) {

                if(empty($id)) {
                    if (empty($exist)) {
                        DB::table('paragraphs')->insert( $input );
                        $mess = "Data inserted.";
                    } else {
                        $mess = "Data already exist.";
                    }
                } else {
                    DB::table('paragraphs')->where('para_id', $id)->update( $input );
                    $mess = "Data updated";
                }

                return redirect()->back()->with('success', $mess);
            }
        }

        $page_no = $request->input('page');
        $offset  = !empty($page_no) ? $page_no - 1 : 0;
        $records = DB::table('subjects')->join('courses', 'subjects.subject_course', '=', 'courses.course_id')->select('subjects.*','courses.course_name')->where('course_is_deleted','N')->paginate(10);

        $edit = $subjects = $topics = array();
        if(!empty($id)) {
            $edit = DB::table('paragraphs')->where('para_id',$id)->first();

            if(!empty($edit->para_course)) {
                $subjects = DB::table('subjects')->where('subject_course', $edit->para_course)->where('subject_is_deleted','N')->get();
            }

            if(!empty($edit->para_subject)) {
                $topics = DB::table('topics')->where('topic_subject', $edit->para_subject)->where('topic_is_deleted','N')->get();
            }
        }

        $courses = DB::table('courses')->where('course_is_deleted', 'N')->get();
        
        $page   = "add_paragraph";
        $data   = compact('page', 'records', 'offset', 'courses', 'subjects', 'topics', 'edit');
        return view('backend/layout', $data);
    }
}
