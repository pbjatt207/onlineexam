<?php
namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use Hash;
use App\Query;

class Candidate extends BaseController {
    public function index( Request $request, $id = NULL ) {
        $setting = DB::table('settings')->first();
        $query = DB::table('candidates as candi')
                 ->join('users as user', 'candi.candidate_user', '=', 'user.user_id')
                 ->where('user_is_deleted', 'N');

        $search = array();
        if(!empty($request->input('search'))) {
            $search = $request->input('search');
            if(!empty($search['keyword'])) {
                $query->where('candidate_f_name', 'LIKE', '%'.$search['keyword'].'%');
            }

            if(!empty($search['roll'])) {
                $query->where('candidate_roll_number', $search['roll']);
            }
        }

        $records = $query->paginate(10);
        $subjects = $topics = array();
        if(!empty($search)) {

            if(!empty($search['que_course'])) {
                $subjects = DB::table('subjects')->where('subject_course', $search['que_course'])->where('subject_is_deleted','N')->get();
            }

            if(!empty($search['que_subject'])) {
                $topics = DB::table('topics')->where('topic_subject', $search['que_subject'])->where('topic_is_deleted','N')->get();
            }
        }

        $courses = DB::table('courses')->where('course_is_deleted', 'N')->get();

        // print_r($request->input('search'));

        $get_parmas = $request->input();

        if ($request->isMethod('post')) {
            $check = $request->input('check');

            if(!empty($check)) {
                $arr = array(
                    "user_is_deleted" => "Y"
                );
                DB::table('users')->whereIn('user_id', $check)->update( $arr );

                return redirect()->back()->with('success', 'Selected record(s) deleted.');
            }
        }

    	$page 	= "candidate";
    	$data 	= compact('page', 'records', 'setting', 'get_parmas');
    	return view('backend/layout', $data);
    }

    public function add( Request $request, $id = NULL ) {
        $q = new Query();
        $courses = DB::table('courses')->where('course_is_deleted', 'N')->get();
        $candidates = DB::table('candidates')->where('candidate_id', $id)->where('candidate_is_deleted', 'N')->first();
        $setting = DB::table('settings')->first();
        if ($request->isMethod('post')) {
            $input = $request->input('record');

            $exists = DB::table('users')
                        ->where('user_mobile', $input['candidate_mobile_no'])
                        ->where('user_email', $input['candidate_email'])
                        ->where('user_is_deleted', 'N')
                        ->first();
            $is_exists = DB::table('candidates')
                        ->where('candidate_mobile_no', $input['candidate_mobile_no'])
                        ->where('candidate_email', $input['candidate_email'])
                        ->where('candidate_is_deleted', 'N')
                        ->first();

            if(!empty($input)) {
                $arr = array(
                    'user_fname'        =>  $input['candidate_f_name'],
                    'user_lname'        =>  $input['candidate_l_name'],
                    'user_name'         =>  $input['candidate_f_name'].$input['candidate_l_name'],
                    'user_login'        =>  $input['candidate_mobile_no'],
                    'user_mobile'        =>  $input['candidate_mobile_no'],
                    'user_email'        =>   $input['candidate_email'],
                    'user_role'         =>   2,
                    'user_courses'      =>   implode(",", $input['user_courses']),
                    'user_created_on'   =>   $input['candidate_created_on']

                );

                if(!empty($input['candidate_password'])) {
                    $arr['user_password'] = password_hash($input['candidate_password'], PASSWORD_BCRYPT, ['cost' => 10]);
                }

                if(empty($id)) {
                    if (empty($is_exists) && empty($exists)) {
                        DB::table('users')->insert( $arr );
                        $uid = DB::getPdo()->lastInsertId();

                        unset($input['candidate_password']);
                        unset($input['user_courses']);

                        $input['candidate_user']  = $uid;
                        $input['candidate_name']  = $input['candidate_f_name']." ".$input['candidate_l_name'];


                        DB::table('candidates')->insert( $input );

                        $id = DB::getPdo()->lastInsertId();

                        DB::table('candidates')->where('candidate_id', $id)->update(['candidate_roll_number' => $setting->site_roll_pre.sprintf("%06d",$id)]);

                        $mess = "Data inserted.";
                        return redirect()->back()->with('success', $mess);
                    } else {
                        $mess = "Mobile of email already exist.";
                    }
                } else {
                    DB::table('users')->where('user_id', $candidates->candidate_user)->update( $arr );
                    unset($input['candidate_password']);
                    unset($input['user_courses']);
                    DB::table('candidates')->where('candidate_id', $id)->update( $input );
                    $mess = "Data updated";
                }

                $slug = $q->create_slug($input['candidate_f_name'], "candidates", "candidate_slug", "candidate_id", $id);
                DB::table('candidates')->where('candidate_id', $id)->update( array('candidate_slug' => $slug) );

                if ($request->hasFile('candidate_image')) {
                    if(!empty($edit->candidate_image) && file_exists(public_path().'/imgs/candidate/'.$edit->candidate_image)) {
                        unlink(public_path().'/imgs/candidate/'.$edit->candidate_image);
                    }
                    $image           = $request->file('candidate_image');
                    $name            = 'img'.$id.'.'.$image->getClientOriginalExtension();
                    $destinationPath = 'public/imgs/candidate';
                    $image->move($destinationPath, $name);

                    if(!empty($edit->candidate_image)) {
                        $name .= "?v=".uniqid();
                    }

                    DB::table('candidates')->where('candidate_id', $id)->update( array('candidate_image' => $name) );
                }

                if ($request->hasFile('candidate_signature')) {
                    if(!empty($edit->candidate_signature) && file_exists(public_path().'/imgs/candidate/'.$edit->candidate_signature)) {
                        unlink(public_path().'/imgs/candidate/'.$edit->candidate_signature);
                    }
                    $image           = $request->file('candidate_signature');
                    $name            = 'sign'.$id.'.'.$image->getClientOriginalExtension();
                    $destinationPath = 'public/imgs/candidate';
                    $image->move($destinationPath, $name);

                    if(!empty($edit->candidate_signature)) {
                        $name .= "?v=".uniqid();
                    }

                    DB::table('candidates')->where('candidate_id', $id)->update( array('candidate_signature' => $name) );
                }
                return redirect()->back()->with('success', $mess);
            }

            $check = $request->input('check');

            if(!empty($check)) {
                $arr = array(
                    "candidate_is_deleted" => "Y"
                );
                DB::table('candidates')->whereIn('candidate_id', $check)->update( $arr );

                return redirect('ea-xpanel/candidate')->with('success', 'Selected record(s) deleted.');
            }


        }

        $page_no = $request->input('page');
        $offset  = !empty($page_no) ? $page_no - 1 : 0;
        $records = DB::table('candidates')->where('candidate_is_deleted','N')->paginate(10);

        $edit = array();
        if(!empty($id)) {
            $edit = DB::table('candidates AS c')
                        ->join('users AS u', 'c.candidate_user', 'u.user_id')
                        ->where('candidate_id',$id)
                        ->first();
        }

        $page   = "add_candidate";
        $data   = compact('page', 'records', 'offset', 'edit', 'candidates', 'id', 'setting', 'courses');
        return view('backend/layout', $data);
    }

    public function profile( Request $request ) {
        $q       = new Query();
        $id      = session('user_auth');
        $records = DB::table('candidates as c')
                     ->join('users as u', 'u.user_id', '=', 'c.candidate_user')
                     ->where('c.candidate_user', $id)
                     ->where('u.user_id', $id)
                     ->first();
        $courses = DB::table('courses')->where('course_is_deleted', 'N')->get();
        if ($request->isMethod('post')) {
            $input = $request->input('record');

            if(!empty($input)) {
                $arr = array(
                    'user_fname'        =>  $input['candidate_f_name'],
                    'user_lname'        =>  $input['candidate_l_name'],
                    'user_name'         =>  $input['candidate_f_name'].$input['candidate_l_name'],
                    'user_courses'      =>   implode(",", $input['user_courses'])

                );
                    DB::table('users')->where('user_id', $id)->update( $arr );
                    unset($input['user_courses']);
                    DB::table('candidates')->where('candidate_user', $id)->update( $input );
                    $mess = "profile updated.";

                $slug = $q->create_slug($input['candidate_f_name'], "candidates", "candidate_slug", "candidate_id", $id);
                DB::table('candidates')->where('candidate_id', $id)->update( array('candidate_slug' => $slug) );

                if ($request->hasFile('candidate_image')) {
                    if(!empty($edit->candidate_image) && file_exists(public_path().'/imgs/candidate/'.$edit->candidate_image)) {
                        unlink(public_path().'/imgs/candidate/'.$edit->candidate_image);
                    }
                    $image           = $request->file('candidate_image');
                    $name            = 'img'.$id.'.'.$image->getClientOriginalExtension();
                    $destinationPath = 'public/imgs/candidate';
                    $image->move($destinationPath, $name);

                    if(!empty($edit->candidate_image)) {
                        $name .= "?v=".uniqid();
                    }

                    DB::table('candidates')->where('candidate_id', $id)->update( array('candidate_image' => $name) );
                }

                if ($request->hasFile('candidate_signature')) {
                    if(!empty($edit->candidate_signature) && file_exists(public_path().'/imgs/candidate/'.$edit->candidate_signature)) {
                        unlink(public_path().'/imgs/candidate/'.$edit->candidate_signature);
                    }
                    $image           = $request->file('candidate_signature');
                    $name            = 'sign'.$id.'.'.$image->getClientOriginalExtension();
                    $destinationPath = 'public/imgs/candidate';
                    $image->move($destinationPath, $name);

                    if(!empty($edit->candidate_signature)) {
                        $name .= "?v=".uniqid();
                    }

                    DB::table('candidates')->where('candidate_id', $id)->update( array('candidate_signature' => $name) );
                }
                return redirect()->back()->with('success', $mess);
            }
        }

        $page   = "profile";
        $data   = compact('page', 'records', 'courses');
        return view('backend/layout', $data);
    }
}
