<?php
namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Query;
use DB;
use Hash;

class News extends BaseController {
    public function index( Request $request, $id = NULL ) {
    	if ($request->isMethod('post')) {
            $check = $request->input('check');

            if(!empty($check)) {
                $arr = array(
                    "news_is_deleted" => "Y"
                );
                DB::table('news')->whereIn('news_id', $check)->update( $arr );

                return redirect()->back()->with('success', 'Selected record(s) deleted.');
            }
	    }
        $page_no = $request->input('page');
        $offset  = !empty($page_no) ? $page_no - 1 : 0;

        $records = DB::table('news')
                    ->join('news_category', 'news.news_category', '=', 'news_category.category_id')
                    ->select('news.*','news_category.category_name')
                    ->where('news_is_deleted','N')
                    ->paginate(10);
                    // print_r($catname);

	    // $records = DB::table('news')->where('news_is_deleted','N')->paginate(10);
	    
    	$page 	= "news";
    	$data 	= compact('page', 'records', 'offset', 'catname');
    	return view('backend/layout', $data);
    }

    public function add( Request $request, $id = NULL ) {
        $q = new Query();

        if ($request->isMethod('post')) {
            $input  = $request->input('record');
            $ntitle = $input['news_title'];
            $ncate  = $input['news_category'];
            $ndesc  = $input['news_description'];

            $Is_exists = DB::table('news')
                        ->where('news_title', $ntitle)
                        ->where('news_category', $ncate)
                        ->where('news_description', $ndesc)
                        ->where('news_is_deleted', 'N')
                        ->first();

            if(!empty($input)) {
                if(empty($id)) {
                    if (empty($Is_exists)){
                        DB::table('news')->insert( $input );
                        $id = DB::getPdo()->lastInsertId();
                        $mess = "Data inserted.";
                    } else {
                        $mess = "News already exist.";
                    }
                } else {
                    DB::table('news')->where('news_id', $id)->update( $input );
                    $mess = "Data updated";
                }

                $slug = $q->create_slug($input['news_title'], "news", "news_slug", "news_id", $id);
                DB::table('news')->where('news_id', $id)->update( array('news_slug' => $slug) );
                return redirect()->back()->with('success', $mess);
            }
        }

        $page_no = $request->input('page');
        $offset  = !empty($page_no) ? $page_no - 1 : 0;
        $records = DB::table('news')->where('news_is_deleted','N')->paginate(10);

        $edit = $subjects = $topics = array();
        if(!empty($id)) {
            $edit = DB::table('news')->where('news_id',$id)->first();
        }

        $category = DB::table('news_category')->where('category_is_deleted', 'N')->get();
        
        $page   = "add_news";
        $data   = compact('page', 'records', 'offset', 'edit', 'category');
        return view('backend/layout', $data);
    }
}
