<?php
namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Query;
use DB;
use Hash;

class Blog extends BaseController {
    public function index( Request $request, $id = NULL ) {
    	if ($request->isMethod('post')) {
            $check = $request->input('check');

            if(!empty($check)) {
                $arr = array(
                    "blog_is_deleted" => "Y"
                );
                DB::table('blogs')->whereIn('blog_id', $check)->update( $arr );

                return redirect()->back()->with('success', 'Selected record(s) deleted.');
            }
	    }

        $page_no = $request->input('page');
        $offset  = !empty($page_no) ? $page_no - 1 : 0;
	    $records = DB::table('blogs')->where('blog_is_deleted','N')->paginate(10);
	    
    	$page 	= "blog";
    	$data 	= compact('page', 'records', 'offset');
    	return view('backend/layout', $data);
    }

    public function add( Request $request, $id = NULL ) {
        $q = new Query();

        if ($request->isMethod('post')) {
            $input = $request->input('record');
            $btitle = $input['blog_title'];
            $bdesc = $input['blog_description'];

            $Is_exists = DB::table('blogs')->where('blog_title', $btitle)->where('blog_description', $bdesc)->where('blog_is_deleted', 'N')->first();

            if(!empty($input)) {
                if(empty($id)) {
                    if (empty($Is_exists)) {
                        DB::table('blogs')->insert( $input );
                        $id = DB::getPdo()->lastInsertId();
                        $mess = "Data inserted.";
                    } else {
                        $mess = "Blog already exist.";
                    }
                } else {
                    DB::table('blogs')->where('blog_id', $id)->update( $input );
                    $mess = "Data updated";
                }

                $slug = $q->create_slug($input['blog_title'], "blogs", "blog_slug", "blog_id", $id);
                DB::table('blogs')->where('blog_id', $id)->update( array('blog_slug' => $slug) );

                if ($request->hasFile('blog_image')) {
                    if(!empty($edit->blog_image) && file_exists(public_path().'/imgs/blogs/'.$edit->blog_image)) {
                        unlink(public_path().'/imgs/blogs/'.$edit->blog_image);
                    }
                    $image           = $request->file('blog_image');
                    $name            = $id.'.'.$image->getClientOriginalExtension();
                    $destinationPath = 'public/imgs/blogs';
                    $image->move($destinationPath, $name);

                    if(!empty($edit->blog_image)) {
                        $name .= "?v=".uniqid();
                    }

                    DB::table('blogs')->where('blog_id', $id)->update( array('blog_image' => $name) );
                }

                return redirect()->back()->with('success', $mess);
            }
        }

        $page_no = $request->input('page');
        $offset  = !empty($page_no) ? $page_no - 1 : 0;
        $records = DB::table('blogs')->where('blog_is_deleted','N')->paginate(10);

        $edit = $subjects = $topics = array();
        if(!empty($id)) {
            $edit = DB::table('blogs')->where('blog_id',$id)->first();
        }
        
        $page   = "add_blog";
        $data   = compact('page', 'records', 'offset', 'edit');
        return view('backend/layout', $data);
    }
}
