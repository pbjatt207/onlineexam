<?php
namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use Hash;
use App\Query;

class Question extends BaseController {
    public function index( Request $request, $id = NULL ) {
    	if ($request->isMethod('post')) {
            $check = $request->input('check');

            if(!empty($check)) {
                $arr = array(
                    "que_is_deleted" => "Y"
                );
                DB::table('questions')->whereIn('que_id', $check)->update( $arr );

                return redirect()->back()->with('success', 'Selected record(s) deleted.');
            }
	    }

        $get_parmas = $request->input();

        $page_no = $request->input('page');
        $offset  = !empty($page_no) ? ($page_no - 1) * 10 : 0;

        $query   = DB::table('questions')
                    ->join('courses', 'questions.que_course', '=', 'courses.course_id')
                    ->join('subjects', 'questions.que_subject', '=', 'subjects.subject_id')
                    ->leftJoin('topics', 'questions.que_topic', '=', 'topics.topic_id')
                    ->select('questions.*','courses.course_name', 'subjects.subject_name', 'topics.topic_name')
                    ->where('que_is_deleted','N');

        $search = array();
        if(!empty($request->input('search'))) {
            $search = $request->input('search');
            if(!empty($search['keyword'])) {
                // $query->where('que_name', 'LIKE', '%'.$search['keyword'].'%');
                $query->where( function($q) use ($search) { 
                    $q->whereRaw('1=2')
                      ->orWhere('que_name', 'LIKE', '%'.$search['keyword'].'%')
                      ->orWhere('que_explaination', 'LIKE', '%'.$search['keyword'].'%');
                } );
            }

            if(!empty($search['que_course'])) {
                $query->where('que_course', $search['que_course']);
            }

            if(!empty($search['que_subject'])) {
                $query->where('que_subject', $search['que_subject']);
            }

            if(!empty($search['que_topic'])) {
                $query->where('que_topic', $search['que_topic']);
            }

            if(!empty($search['que_language'])) {
                $query->where('que_language', $search['que_language']);
            }
        }

	    $records = $query->paginate(10);
        $subjects = $topics = array();
        if(!empty($search)) {

            if(!empty($search['que_course'])) {
                $subjects = DB::table('subjects')->where('subject_course', $search['que_course'])->where('subject_is_deleted','N')->get();
            }

            if(!empty($search['que_subject'])) {
                $topics = DB::table('topics')->where('topic_subject', $search['que_subject'])->where('topic_is_deleted','N')->get();
            }
        }

        $courses = DB::table('courses')->where('course_is_deleted', 'N')->get();

    	$page 	= "question";
    	$data 	= compact('page', 'records', 'offset', 'search', 'courses', 'subjects', 'topics', 'get_parmas');
    	return view('backend/layout', $data);
    }

    public function add( Request $request, $id = NULL ) {

        if ($request->isMethod('post')) {
            $input      = $request->input('record');
            $qcourse      = $input['que_course'];
            $qsubject   = $input['que_subject'];
            $qtopic     = $input['que_topic'];
            // $qlanguage  = $input['que_language'];
            // $qdlevel    = $input['que_defficulty_level'];
            $qtype      = $input['que_type'];
            $qname      = $input['que_name'];
            $qexp       = $input['que_explaination'];
            $qanswer    = $input['que_answer'];
            $qopt1      = $input['que_opt1'];
            $qopt2      = $input['que_opt2'];
            $qopt3      = $input['que_opt3'];
            $qopt4      = $input['que_opt4'];

            $exist    = DB::table('questions')
                        ->where('que_course', $qcourse)
                        ->where('que_subject', $qsubject)
                        ->where('que_topic', $qtopic)
                        ->where('que_type', $qtype)
                        ->where('que_name', $qname)
                        ->where('que_explaination', $qexp)
                        ->where('que_answer', $qanswer)
                        ->where('que_opt1', $qopt1)
                        ->where('que_opt2', $qopt2)
                        ->where('que_opt3', $qopt3)
                        ->where('que_opt4', $qopt4)
                        ->where('que_is_deleted', 'N')
                        ->first();

            if(!empty($input)) {

                if(empty($id)) {
                    if (empty($exist)) {
                        DB::table('questions')->insert( $input );
                        $id = DB::getPdo()->lastInsertId();
                        $mess = "Data inserted.";
                    } else {
                        $mess = "Data already exist.";
                    }
                } else {
                    DB::table('questions')->where('que_id', $id)->update( $input );
                    $mess = "Data updated";
                }

                return redirect()->back()->with('success', $mess);
            }
        }

        $page_no = $request->input('page');
        $offset  = !empty($page_no) ? $page_no - 1 : 0;
        $records = DB::table('subjects')->join('courses', 'subjects.subject_course', '=', 'courses.course_id')->select('subjects.*','courses.course_name')->where('course_is_deleted','N')->paginate(10);

        $edit = $subjects = $topics = array();
        if(!empty($id)) {
            $edit = DB::table('questions')->where('que_id',$id)->first();

            if(!empty($edit->que_course)) {
                $subjects = DB::table('subjects')->where('subject_course', $edit->que_course)->where('subject_is_deleted','N')->get();
            }

            if(!empty($edit->que_subject)) {
                $topics = DB::table('topics')->where('topic_subject', $edit->que_subject)->where('topic_is_deleted','N')->get();
            }
        }

        $courses = DB::table('courses')->where('course_is_deleted', 'N')->get();

        $page   = "add_question";
        $data   = compact('page', 'records', 'offset', 'courses', 'subjects', 'topics', 'edit');
        return view('backend/layout', $data);
    }

    public function copy(Request $request, $que_id) {
        $page_no = $request->input('page');
        $offset  = !empty($page_no) ? $page_no - 1 : 0;
        $subject = array();
        $question = DB::table('questions')->where('que_id',$que_id)->first();
        $courses  = DB::table('courses')->where('course_is_deleted', 'N')->get();
        $topic    = DB::table('topics')
                    ->join('courses', 'topics.topic_course', '=', 'courses.course_id')
                    ->join('subjects', 'topics.topic_subject', '=', 'subjects.subject_id')
                    ->select('topics.*','courses.course_name', 'subjects.subject_name')
                    ->where('course_is_deleted','N');

        $get_parmas = $request->input();

        $search = array();
        if(!empty($request->input('search'))) {
            $search = $request->input('search');
            if(!empty($search['course'])) {
                $topic->where('topic_course',$search['course']);
            }

            if(!empty($search['subject'])) {
                $topic->where('topic_subject', $search['subject']);
            }
        }
        $topic =$topic->paginate(10);

        if ($request->isMethod('post')) {
            $check = $request->input('check');

            if(!empty($check)) {
                foreach($check as $tid) {
                    $topic = DB::table('topics')->where('topic_id', $tid)->first();

                    $is_exists = DB::table('questions')
                                ->where('que_course', $topic->topic_course)
                                ->where('que_subject', $topic->topic_subject)
                                ->where('que_topic', $topic->topic_id)
                                ->where('que_name', $question->que_name)
                                ->select(DB::raw('COUNT(*) AS total'))
                                ->first();

                    if($is_exists->total == 0) :
                        $arr = (array) $question;
                        unset($arr['que_id']);
                        $arr['que_course']  = $topic->topic_course;
                        $arr['que_subject'] = $topic->topic_subject;
                        $arr['que_topic']   = $topic->topic_id;
                        DB::table('questions')->insert($arr);
                    endif;

                }

                return redirect()->back()->with('success', 'Selected record(s) copied.');
            }
        }

        $page   = "question_copy";
        $data   = compact('page', 'question', 'topic','courses', 'subject', 'offset', 'get_parmas');
        return view('backend/layout', $data);
    }
}
