<?php
namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

class Dashboard extends BaseController {
    public function index() {
    	$title 	= "Dashboard | English Aliens";
    	$page 	= "dashboard";
    	$data 	= compact('page', 'title');

    	$user_id = session('user_auth');

    	if(!empty($user_id)) {
	    	$profile = DB::table('users as u')
						->join('roles as r', 'u.user_role', '=', 'r.role_id')
						->where('user_id', $user_id)->first();
	    	if(!empty($profile->role_name) && $profile->role_name == "Candidate") {
	    		$courses = DB::table('courses')->whereIn('course_id', explode(',', $profile->user_courses))->where('course_is_deleted', 'N')->get();

	    		$data['courses'] = $courses;
	    	}
	    }

    	return view('backend/layout', $data);
    }
}
