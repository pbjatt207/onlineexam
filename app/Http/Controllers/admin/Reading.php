<?php
namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Query;
use DB;

class Reading extends BaseController {
    public function index( Request $request, $id = NULL ) {
    	if ($request->isMethod('post')) {
            $check = $request->input('check');

            if(!empty($check)) {
                $arr = array(
                    "reading_is_deleted" => "Y"
                );
                DB::table('readings')->whereIn('reading_id', $check)->update( $arr );

                return redirect()->back()->with('success', 'Selected record(s) deleted.');
            }
	    }

        $page_no = $request->input('page');
        $offset  = !empty($page_no) ? $page_no - 1 : 0;
	    $records = DB::table('readings')->where('reading_is_deleted','N')->paginate(10);
	    
    	$page 	= "reading";
    	$data 	= compact('page', 'records', 'offset');
    	return view('backend/layout', $data);
    }

    public function add( Request $request, $id = NULL ) {
        $q = new Query();

        if ($request->isMethod('post')) {
            $input  = $request->input('record');
            $rtitle = $input['reading_title'];
            $rdesc  = $input['reading_description'];

            $Is_exists = DB::table('readings')
                         ->where('reading_title', $rtitle)
                         ->where('reading_description', $rdesc)
                         ->where('reading_is_deleted', 'N')
                         ->first();

            if(!empty($input)) {
                if(empty($id)) {
                    if (empty($Is_exists)) {
                        DB::table('readings')->insert( $input );
                        $id = DB::getPdo()->lastInsertId();
                        $mess = "Data inserted.";
                    } else {
                        $mess = "Reading already exist.";
                    }
                } else {
                    DB::table('readings')->where('reading_id', $id)->update( $input );
                    $mess = "Data updated";
                }

                $slug = $q->create_slug($input['reading_title'], "readings", "reading_slug", "reading_id", $id);
                DB::table('readings')->where('reading_id', $id)->update( array('reading_slug' => $slug) );

                return redirect()->back()->with('success', $mess);
            }
        }

        $page_no = $request->input('page');
        $offset  = !empty($page_no) ? $page_no - 1 : 0;
        $records = DB::table('readings')->where('reading_is_deleted','N')->paginate(10);

        $edit = array();
        if(!empty($id)) {
            $edit = DB::table('readings')->where('reading_id',$id)->first();
        }
        
        $page   = "add_reading";
        $data   = compact('page', 'records', 'offset', 'edit');
        return view('backend/layout', $data);
    }
}
