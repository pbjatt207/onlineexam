<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

class Exam extends BaseController {
    public function index( Request $request, $slug ) {
    	$title 		= "Online Exams | English Aliens";
      $test   = DB::table('tests')->where('test_slug', $slug)->first();
      $inst   = DB::table('instructions')->where('inst_id', $test->test_instruction)->where('inst_is_deleted', 'N')->first();

      $sections = DB::table('subjects')->whereIn('subject_id' , function($q) use($test){
        $q->select('que_subject')
        ->from('test_questions')
        ->join('questions', 'test_questions.test_questions_question_id', 'questions.que_id')
        ->where('test_questions_test_id', $test->test_id);
      })->groupBy('subject_name')->get();

      $questionsArr = [];

      foreach($sections as $key => $sec) {
        $questions = DB::table('test_questions')
                    ->join('questions', 'que_id', '=', 'test_questions.test_questions_question_id')
                    ->where('test_questions_test_id', $test->test_id)
                    ->where('que_subject', $sec->subject_id)
                    ->get()->toArray();

        $questionsArr[$sec->subject_id] = $questions;
      }

      // echo '<pre>';
      // print_r($questionsArr);
      // die;
      // echo '</pre>';


      // print_r($questionshi);

      $total_questions = DB::table('test_questions')
                  ->join('questions', 'que_id', '=', 'test_questions.test_questions_question_id')
                  ->where('test_questions_test_id', $test->test_id)
                  ->count();

    	$page 		= "online_exam";
    	$data 		= compact('page', 'title', 'inst', 'test', 'questions','sections','questionsArr', 'total_questions');
    	return view('backend/layout2', $data);
    }
    public function Bank() {

    	$title 	= "Online Exams | English Aliens";
    	$page 	= "bank";
    	$data 	= compact('page', 'title');
    	return view('frontend/layout2', $data);
    }

    public function feedback( Request $request, $id = NULL ) {
        $title  = "Feedback | English Aliens";

        if($_SERVER['REQUEST_METHOD'] == "POST") {

            $input = $request->input('feedback');

            $user = session('user_auth');
            $jsonfile = "tests/$id/{$user}.json";
            $json = file_get_contents($jsonfile);
            $arr  = json_decode($json, true);

            // print_r($arr);

            $data = array(
                "tres_tid"      =>  $id,
                "tres_uid"      =>  $user,
                "tres_details"  =>  $json,
                "tres_feedback" =>  $input
            );
            $isExist = DB::table('test_results')
                         ->where('tres_tid', $id)
                         ->where('tres_uid', $user)
                         ->first();
            if (!empty($isExist)) {
                 DB::table('test_results')
                    ->where('tres_id', $isExist->tres_id)
                    ->update($data);
            } else {
                 DB::table('test_results')
                    ->insert($data);
            }

            return redirect('ea-xpanel/result/'.$user.'/'.$id);
        }

        $page   = "feedback";
        $data   = compact('page', 'title');
        return view('backend/layout', $data);
    }

    public function result( Request $request, $uid, $tid ) {
        $title          = "Result | English Aliens";
        $user_id        = session('user_auth');
        $user_info      = DB::table('candidates')->where('candidate_user', $user_id)->first();
        $test_info      = DB::table('tests')->where('test_id', $tid)->first();
        $result         = DB::table('test_results')->where('tres_tid', $tid)->where('tres_uid', $uid)->first();
        if (empty($result)) {
            return redirect('ea-xpanel');
        }

        $testDetails    = json_decode( html_entity_decode($result->tres_details), true );

        $questions = DB::table('test_questions as tq')
                    ->join('questions as q', 'q.que_id', '=', 'tq.test_questions_question_id')
                    ->join('tests as t', 't.test_id', '=', 'tq.test_questions_test_id')
                    ->where('tq.test_questions_test_id', $test_info->test_id)
                    ->get();


         $correct = $incorrect = $notAnswered = $totalmarks = $gainmarks = 0;

         foreach($questions as $que) {
            if (!empty($testDetails['option'])) {
                    $queIds     = array_keys($testDetails['option']);

                $correctOpt = $que->que_answer;
                $totalmarks += $que->test_positive_marks;

                if(in_array($que->que_id, $queIds)) {
                    if($testDetails['option'][$que->que_id] == $correctOpt){
                        $correct++;
                        $gainmarks += $que->test_positive_marks;
                    } else {
                        $incorrect++;
                        $gainmarks -= $que->test_negative_marks;
                    }
                } else {
                    $notAnswered++;
                }
            }
        }
        $total_marks = $que->test_total_marks;

        $arr = array(
            "tres_gain_marks"   => $gainmarks,
            "tres_correct"      => $correct,
            "tres_incorrect"    => $incorrect
        );

        // print_r($testDetails);
        DB::table('test_results')->where('tres_uid', $uid)->where('tres_tid', $tid)->update($arr);

        if($test_info->test_is_free == "Y") {
            DB::table('test_results')->where('tres_uid', $uid)
            ->where('tres_tid', $tid)->delete();
            $jsonfile = "tests/".$tid."/".$uid.".json";
            unlink($jsonfile);
        }



        $page   = "result";
        $data   = compact('page', 'title', 'gainmarks', 'correct', 'incorrect', 'notAnswered', 'total_marks', 'questions');
        return view('backend/layout', $data);
    }

    public function tests( Request $request ) {
        $title   = "Result | English Aliens";
        $user_id = session('user_auth');
        $test    = DB::table('test_results')
                   ->join('tests', 'tests.test_id', '=', 'test_results.tres_tid')
                   ->where('tres_uid', $user_id)
                   ->paginate(10);
        $page    = "myexams";
        $data    = compact('page', 'title','test', 'user_id');
        return view('backend/layout', $data);
    }
}
