<?php
namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use Hash;
use App\Query;

class Instructions extends BaseController {
    public function index( Request $request, $id = NULL ) {

        $get_parmas = $request->input();
        $search = $request->input('search');
        $query = DB::table('instructions')
                    ->where('inst_is_deleted', 'N');

        if(!empty($request->input('search'))) {
            $search = $request->input('search');
            if(!empty($search['keyword'])) {
                $query->where('inst_name', 'LIKE', '%'.$search['keyword'].'%');
            }
        }

        $records = $query->paginate(10);

        if ($request->isMethod('post')) {
            $check = $request->input('check');

            if(!empty($check)) {
                $arr = array(
                    "inst_is_deleted" => "Y"
                );
                DB::table('instructions')->whereIn('inst_id', $check)->update( $arr );

                return redirect()->back()->with('success', 'Selected record(s) deleted.');
            }
        }


    	$page 	= "instruction";
    	$data 	= compact('page', 'records', 'search', 'get_parmas');
    	return view('backend/layout', $data);
    }

    public function add( Request $request, $id = NULL ) {
        $q = new Query();

        if ($request->isMethod('post')) {
            $input = $request->input('record');
            $iname = $input['inst_name'];
            $idesc = $input['inst_description'];

            $exist    = DB::table('instructions')
                        ->where('inst_name', $iname)
                        ->where('inst_description', $idesc)
                        ->where('inst_is_deleted', 'N')
                        ->first();

            if(!empty($input)) {
                if(empty($id)) {
                    if (empty($exist)) {
                        DB::table('instructions')->insert( $input );
                        $id = DB::getPdo()->lastInsertId();
                        $mess = "Data inserted.";
                    } else {
                        $mess = "Data already exist.";
                    }
                } else {
                    DB::table('instructions')->where('inst_id', $id)->update( $input );
                    $mess = "Data updated";
                }

                return redirect()->back()->with('success', $mess);
            }   
        }

        $page_no = $request->input('page');
        $offset  = !empty($page_no) ? $page_no - 1 : 0;
        $edit = array();
        if(!empty($id)) {
            $edit = DB::table('instructions')->where('inst_id',$id)->first();
        }

        
        $page   = "add_instruction";
        $data   = compact('page', 'edit');
        return view('backend/layout', $data);
    }
}
