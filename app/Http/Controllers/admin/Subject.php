<?php
namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use App\Query;

class Subject extends BaseController {
    public function index( Request $request, $id = NULL ) {
        $q = new Query();

    	if ($request->isMethod('post')) {
    		$input = $request->input('record');
            $sname = $input['subject_name'];
            $scourse = $input['subject_course'];
            $exist = DB::table('subjects')->where('subject_name', $sname)->where('subject_course', $scourse)->where('subject_is_deleted', 'N')->first();

            if(!empty($input)) {

                if(empty($id)) {
                    if (empty($exist)) {
            		    DB::table('subjects')->insert( $input );
                        $id = DB::getPdo()->lastInsertId();
                        $mess = "Data inserted.";
                    } else {
                        $mess = "Data already exist.";
                    }
                } else {
                    DB::table('subjects')->where('subject_id', $id)->update( $input );
                    $mess = "Data updated";
                }

                $slug = $q->create_slug($input['subject_name'], "subjects", "subject_slug", "subject_id", $id);
                DB::table('subjects')->where('subject_id', $id)->update( array('subject_slug' => $slug) );

        		return redirect()->back()->with('success', $mess);
            }

            $check = $request->input('check');

            if(!empty($check)) {
                $arr = array(
                    "subject_is_deleted" => "Y"
                );
                DB::table('subjects')->whereIn('subject_id', $check)->update( $arr );

                return redirect()->back()->with('success', 'Selected record(s) deleted.');
            }
	    }

        $page_no = $request->input('page');
        $offset  = !empty($page_no) ? $page_no - 1 : 0;
	    $records = DB::table('subjects')->join('courses', 'subjects.subject_course', '=', 'courses.course_id')->select('subjects.*','courses.course_name')->where('subject_is_deleted', 'N')->paginate(10);

        $edit = array();
        if(!empty($id)) {

            $edit = DB::table('subjects')->where('subject_id',$id)->first();

        }

        $courses = DB::table('courses')->where('course_is_deleted', 'N')->get();
	    
    	$page 	= "subject";
    	$data 	= compact('page', 'records', 'offset', 'courses', 'edit');
    	return view('backend/layout', $data);
    }

    public function copy(Request $request, $subject_id) {
        $page_no = $request->input('page');
        $offset  = !empty($page_no) ? $page_no - 1 : 0;

        $subject = DB::table('subjects')->where('subject_id',$subject_id)->first();

        if ($request->isMethod('post')) {
            $check = $request->input('check');

            if(!empty($check)) {
                foreach($check as $cid) {
                    $is_exists = DB::table('subjects')->where('subject_course', $cid)->where('subject_name', 'LIKE', $subject->subject_name)->select(DB::raw('COUNT(*) AS total'))->first();

                    $arr = array(
                        'subject_name'          => $subject->subject_name,
                        'subject_slug'          => $subject->subject_slug,
                        'subject_course'        => $cid,
                        'subject_is_deleted'    => 'N'
                    );

                    if($is_exists->total == 0) {
                        DB::table('subjects')->insert($arr);
                    } else {
                        DB::table('subjects')->where('subject_course', $cid)->where('subject_name', 'LIKE', $subject->subject_name)->update($arr);
                    }
                }


                return redirect()->back()->with('success', 'Selected record(s) copied.');
            }
        }


        $courses = DB::table('courses')->where('course_is_deleted', 'N')->paginate(10);

        $page   = "subject_copy";
        $data   = compact('page', 'subject', 'courses', 'offset');
        return view('backend/layout', $data);
    }
}
