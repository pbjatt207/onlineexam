<?php
namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

class User extends BaseController {
    public function change_password( Request $request, $id = NULL ) {
        $profile = DB::table('users')->where('user_id', session('user_auth'))->first();

    	if ($request->isMethod('post')) {
    		$input = $request->input('password');

            if(!password_verify($input['current'], $profile->user_password)) {
                return redirect()->back()->with('failed', "Current password not matched.");
            } elseif($input['new'] != $input['confirm']) {
                return redirect()->back()->with('failed', "New password and confirm password not matched.");
            } else {
                $password = password_hash($input['new'], PASSWORD_BCRYPT, ['cost' => 10]);

                DB::table('users')->where('user_id', $profile->user_id)->update( array('user_password' => $password) );
                return redirect()->back()->with('success', "Password has been changed.");
            }
	    }

    	$page 	= "change_password";
    	$data 	= compact('page');
    	return view('backend/layout', $data);
    }

    public function register( Request $request ) {

        $courses = DB::table('courses')->where('course_is_deleted', 'N')->get();
        $candidate = DB::table('candidates')->where('candidate_is_deleted', 'N')->get();
        $setting = DB::table('settings')->first();
        foreach($candidate as $roll){}
        $roll_number = @$roll->candidate_id + 1;

        if ($request->isMethod('post')) {
            $input = $request->input('record');

            if(!empty($input)) {
                if($input['user_password'] == $input['user_confirm_password']) :
                    $is_exists = DB::table('users')->where('user_mobile', $input['user_mobile'])->where('user_is_deleted', 'N')->first();

                    if(empty( $is_exists ) || $is_exists->isEmpty()) {
                        $input['user_login']    = $input['user_mobile'];
                        $input['user_role']     = 2;
                        $input['user_courses']  = implode(",", $input['user_courses']);
                        $input['user_password'] = password_hash($input['user_password'], PASSWORD_BCRYPT, ['cost' => 10]);
                        $roll = $input['candidate_roll_number'];
                        unset($input['user_confirm_password']);
                        unset($input['candidate_roll_number']);

                        DB::table('users')->insert( $input );
                        $id = DB::getPdo()->lastInsertId();


                        $arr = array(
                            'candidate_name'        => $input['user_name'],
                            'candidate_roll_number' => $roll,
                            'candidate_email'       => $input['user_email'],
                            'candidate_mobile_no'   => $input['user_mobile'],
                            'candidate_mobile_no'   => $input['user_mobile'],
                            'candidate_user'        => $id
                        );

                        DB::table('candidates')->insert( $arr );
                        $mess = "Registration successfully done.";

                        return redirect('ea-xpanel');
                    } else {
                        $mess = "Mobile no. already exists, please try with another.";
                    }
                else:
                    $mess = "Password and confirm password is not matched.";
                endif;
            }
        }

        $data   = compact('courses', 'roll_number', 'setting');
        return view('backend/register', $data);
    }

    public function logout(Request $request) {
        $request->session()->forget('user_auth');
        return redirect('ea-xpanel')->with('success', "You've successfully logged out.");
    }
}
