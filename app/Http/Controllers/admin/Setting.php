<?php
namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

class Setting extends BaseController {
    public function index( Request $request, $id = NULL ) {
        $edit       = DB::table('settings')->first();

    	if ($request->isMethod('post')) {
    		$input = $request->input('record');

            if(!empty($input)) {
                if ($request->hasFile('site_logo')) {
                    if(!empty($edit->site_logo) && file_exists(public_path().'/site/'.$edit->site_logo)) {
                        unlink(public_path().'/site/'.$edit->site_logo);
                    }
                    $image           = $request->file('site_logo');
                    $name            = "logo".'.'.$image->getClientOriginalExtension();
                    $destinationPath = 'public/site';
                    $image->move($destinationPath, $name);

                    $input['site_logo'] = $name;
                }

                if ($request->hasFile('site_logo2')) {
                    if(!empty($edit->site_logo2) && file_exists(public_path().'/site/'.$edit->site_logo2)) {
                        unlink(public_path().'/site/'.$edit->site_logo2);
                    }
                    $image           = $request->file('site_logo2');
                    $name            = "logo2".'.'.$image->getClientOriginalExtension();
                    $destinationPath = 'public/site';
                    $image->move($destinationPath, $name);

                    $input['site_logo2'] = $name;
                }

                if ($request->hasFile('site_favicon')) {
                    if(!empty($edit->site_favicon) && file_exists(public_path().'/site/'.$edit->site_favicon)) {
                        unlink(public_path().'/site/'.$edit->site_favicon);
                    }
                    $image           = $request->file('site_favicon');
                    $name            = "favicon".'.'.$image->getClientOriginalExtension();
                    $destinationPath = 'public/site';
                    $image->move($destinationPath, $name);

                    $input['site_favicon'] = $name;
                }

                DB::table('settings')->update( $input );
                $mess = "Data updated";

        		return redirect()->back()->with('success', $mess);
            }
	    }

    	$page 	= "wsettings";
    	$data 	= compact('page', 'edit');
    	return view('backend/layout', $data);
    }

     public function general( Request $request, $id = NULL ) {
        $edit       = DB::table('settings')->first();

        if ($request->isMethod('post')) {
            $input = $request->input('record');

            if(!empty($input)) {
                if ($request->hasFile('site_logo')) {
                    if(!empty($edit->site_logo) && file_exists(public_path().'/site/'.$edit->site_logo)) {
                        unlink(public_path().'/site/'.$edit->site_logo);
                    }
                    $image           = $request->file('site_logo');
                    $name            = "logo".'.'.$image->getClientOriginalExtension();
                    $destinationPath = 'public/site';
                    $image->move($destinationPath, $name);

                    $input['site_logo'] = $name;
                }

                if ($request->hasFile('site_logo2')) {
                    if(!empty($edit->site_logo2) && file_exists(public_path().'/site/'.$edit->site_logo2)) {
                        unlink(public_path().'/site/'.$edit->site_logo2);
                    }
                    $image           = $request->file('site_logo2');
                    $name            = "logo2".'.'.$image->getClientOriginalExtension();
                    $destinationPath = 'public/site';
                    $image->move($destinationPath, $name);

                    $input['site_logo2'] = $name;
                }

                if ($request->hasFile('site_favicon')) {
                    if(!empty($edit->site_favicon) && file_exists(public_path().'/site/'.$edit->site_favicon)) {
                        unlink(public_path().'/site/'.$edit->site_favicon);
                    }
                    $image           = $request->file('site_favicon');
                    $name            = "favicon".'.'.$image->getClientOriginalExtension();
                    $destinationPath = 'public/site';
                    $image->move($destinationPath, $name);

                    $input['site_favicon'] = $name;
                }

                DB::table('settings')->update( $input );
                $mess = "Data updated";

                return redirect()->back()->with('success', $mess);
            }
        }

        $page   = "gsettings";
        $data   = compact('page', 'edit');
        return view('backend/layout', $data);
    }
}
