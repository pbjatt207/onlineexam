<?php
namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use App\Query;

class Plan extends BaseController {
    public function index( Request $request, $id = NULL ) {
        $q = new Query();

        $courses = DB::table('courses')->where('course_is_deleted', 'N')->get();
        
    	if ($request->isMethod('post')) {
    		$input = $request->input('record');

            if(!empty($input)) {
                if(empty($id)) {
        		    DB::table('plans')->insert( $input );
                    $id = DB::getPdo()->lastInsertId();
                    $mess = "Data inserted.";
                } else {
                    DB::table('plans')->where('plan_id', $id)->update( $input );
                    $mess = "Data updated";
                }

                $slug = $q->create_slug($input['plan_title'], "plans", "plan_slug", "plan_id", $id);
                DB::table('plans')->where('plan_id', $id)->update( array('plan_slug' => $slug) );

        		return redirect()->back()->with('success', $mess);
            }

            $check = $request->input('check');

            if(!empty($check)) {
                $arr = array(
                    "plan_is_deleted" => "Y"
                );
                DB::table('plans')->whereIn('plan_id', $check)->update( $arr );

                return redirect('ea-xpanel/plan')->with('success', 'Selected record(s) deleted.');
            }
	    }

        $page_no = $request->input('page');
        $offset  = !empty($page_no) ? $page_no - 1 : 0;
	    $records = DB::table('plans')
                    ->Join('courses', 'courses.course_id', '=', 'plans.plan_course')
                    ->where('plan_is_deleted','N')->paginate(10);

        $edit = array();
        if(!empty($id)) {

            $edit = DB::table('plans')->where('plan_id',$id)->first();

        }
	    
    	$page 	= "plan";
    	$data 	= compact('page', 'records', 'offset', 'edit', 'courses', 'id');
    	return view('backend/layout', $data);
    }
}
