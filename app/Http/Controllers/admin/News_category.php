<?php
namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Query;
use DB;
use Hash;

class News_category extends BaseController {
    public function index( Request $request, $id = NULL ) {
        $q = new Query();
        if ($request->isMethod('post')) {
            $input = $request->input('record');
            $cate  = $input['category_name'];

            $Is_exists = DB::table('news_category')->where('category_name', $cate)->where('category_is_deleted', 'N')->first();

            if(!empty($input)) {
                if(empty($id)) {
                    if (empty($Is_exists)) {
                        DB::table('news_category')->insert( $input );
                        $id = DB::getPdo()->lastInsertId();
                        $mess = "Data inserted.";
                    } else {
                        $mess = "Category already exists.";
                    }
                } else {
                    DB::table('news_category')->where('category_id', $id)->update( $input );
                    $mess = "Data updated";
                }

               $slug = $q->create_slug($input['category_name'], "news_category", "category_slug", "category_id", $id);
                DB::table('news_category')->where('category_id', $id)->update( array('category_slug' => $slug) );
                return redirect('ea-xpanel/category')->with('success', $mess);
            }

            $check = $request->input('check');

            if(!empty($check)) {
                $arr = array(
                    "category_is_deleted" => "Y"
                );
                DB::table('news_category')->whereIn('category_id', $check)->update( $arr );

                return redirect()->back()->with('success', 'Selected record(s) deleted.');
            }

           
        }


        $page_no = $request->input('page');
        $offset  = !empty($page_no) ? $page_no - 1 : 0;
	    $records = DB::table('news_category')->where('category_is_deleted','N')->paginate(10);
	    
         $edit = array();
        if(!empty($id)) {
            $edit = DB::table('news_category')->where('category_id',$id)->first();
        }

    	$page 	= "news_category";
    	$data 	= compact('page', 'records', 'offset', 'edit');
    	return view('backend/layout', $data);
    }
}
