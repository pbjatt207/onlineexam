<?php
namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use App\Query;

class Course extends BaseController {
    public function index( Request $request, $id = NULL ) {
        $q = new Query();
        
    	if ($request->isMethod('post')) {
    		$input = $request->input('record');
            $cname = $input['course_name'];
            $exist = DB::table('courses')->where('course_name', $cname)->where('course_is_deleted', 'N')->first();

            if(!empty($input)) {
                if(empty($id)) {
                    if (empty($exist)) {
            		    DB::table('courses')->insert( $input );
                        $id = DB::getPdo()->lastInsertId();
                        $mess = "Data inserted.";
                    } else {
                        $mess = "Data already exist.";
                    }
                } else {
                    DB::table('courses')->where('course_id', $id)->update( $input );
                    $mess = "Data updated";
                }

                $slug = $q->create_slug($input['course_name'], "courses", "course_slug", "course_id", $id);
                DB::table('courses')->where('course_id', $id)->update( array('course_slug' => $slug) );

        		return redirect()->back()->with('success', $mess);
            }

            $check = $request->input('check');

            if(!empty($check)) {
                $arr = array(
                    "course_is_deleted" => "Y"
                );
                DB::table('courses')->whereIn('course_id', $check)->update( $arr );

                return redirect()->back()->with('success', 'Selected record(s) deleted.');
            }
	    }

        $page_no = $request->input('page');
        $offset  = !empty($page_no) ? $page_no - 1 : 0;
	    $records = DB::table('courses')->where('course_is_deleted','N')->paginate(10);

        $edit = array();
        if(!empty($id)) {

            $edit = DB::table('courses')->where('course_id',$id)->first();

        }
	    
    	$page 	= "course";
    	$data 	= compact('page', 'records', 'offset', 'edit');
    	return view('backend/layout', $data);
    }
}
