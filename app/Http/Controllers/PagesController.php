<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

class PagesController extends BaseController {
    public function index(Request $request, $slug) {
    	$title 		= "Page | English Aliens";
    	$page 		= "pages";

        $rec = DB::table('pages')->where('page_slug',$slug)->first();

    	$data 		= compact('page', 'title', 'rec');
    	return view('frontend/layout', $data);
    }
    // public function single(Request $request, $slug) {
    // 	$rec = DB::table('blogs')->where('blog_is_deleted','N')->where('blog_slug',$slug)->first();

    // 	$title 	= "Blog | English Aliens";
    // 	$page 	= "single-blog";


    // 	$data 	= compact('page', 'title', 'rec');
    // 	return view('frontend/layout', $data);
    // }
}
