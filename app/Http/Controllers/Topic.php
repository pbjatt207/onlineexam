<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

class Topic extends BaseController {
    public function index(Request $request, $course_slug = null, $subject_slug = null) {

        $course     = DB::table('courses')->where('course_slug', $course_slug)->first();
        $subject    = DB::table('subjects')->where('subject_slug', $subject_slug)->first();

    	$title 		= "English Aliens | Read, write, speak and listen to it";

    	$courses 	= DB::table('courses')->where('course_is_deleted','N')->where('course_is_visible','Y')->get();

    	$topics     = DB::table('topics')->where('topic_is_deleted', 'N')->where('topic_subject', $subject->subject_id)->get();

    	$page = "topic";
    	$data = compact('page', 'title', 'courses', 'topics', 'course', 'subject');
    	return view('frontend/layout', $data);
    }
}

