<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

class ExamController extends BaseController {
    public function index() {
    	$title 		= "News | English Aliens";
    	$page 		= "ssc";

    	$data 		= compact('page', 'title');
    	return view('frontend/layout2', $data);
    }
}
