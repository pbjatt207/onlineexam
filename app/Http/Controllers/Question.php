<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

class Question extends BaseController {
    public function index(Request $request, $course_slug = null, $subject_slug = null, $topic_slug = null) {

        $page_no = $request->input('page');
        $offset  = !empty($page_no) ? $page_no - 1 : 0;

        $course     = DB::table('courses')->where('course_slug', $course_slug)->first();
        $subject    = DB::table('subjects')->where('subject_slug', $subject_slug)->first();
        $topic      = DB::table('topics')->where('topic_slug', $topic_slug)->first();

    	$title 		= "Read Questions Of {$course->course_name}, {$subject->subject_name}, {$topic->topic_name} | EA";

    	$courses 	= DB::table('courses')->where('course_is_deleted','N')->where('course_is_visible','Y')->get();

        $paragraphs = $questions = array();
        if($topic->topic_is_para == "Y") {
            $paragraphs = DB::table('paragraphs')->where('para_is_deleted', 'N')->where('para_subject', $subject->subject_id)->where('para_topic', $topic->topic_id)->paginate(1);
            foreach($paragraphs as $key => $para) {
                $para_ques = DB::table('questions')->where('que_is_deleted', 'N')->where('que_paragraph', $para->para_id)->get();
                $paragraphs[$key]->questions = $para_ques;
            }
        } else {
    	    $questions  = DB::table('questions')->where('que_is_deleted', 'N')->where('que_subject', $subject->subject_id)->where('que_topic', $topic->topic_id)->paginate(10);
        }


    	$page       = "questions";
    	$data       = compact('page', 'title', 'courses', 'questions', 'paragraphs', 'course', 'subject', 'topic');
    	return view('frontend/layout', $data);
    }
}

