<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

class HomeController extends BaseController
{
   public function index() {
    	$title 		= "English Aliens | Read, write, speak and listen to it";

    	$courses 	= DB::table('courses')->where('course_is_deleted','N')->where('course_is_visible','Y')->get();

    	foreach($courses as $key => $course) {
    		$subjects = DB::table('subjects')->where('subject_is_deleted', 'N')->where('subject_is_visible','Y')->where('subject_course',$course->course_id)->get();
    		$courses[$key]->subjects = $subjects;
    	}

    	$page = "home";
    	$data = compact('page', 'title', 'courses');
    	return view('frontend/layout', $data);
    }
}

