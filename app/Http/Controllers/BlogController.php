<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

class BlogController extends BaseController {
    public function index() {
    	$title 		= "Blog | English Aliens";
    	$page 		= "blog";

    	$records 	= DB::table('blogs')->where('blog_is_deleted','N')->where('blog_is_visible','Y')->paginate(12);

    	$data 		= compact('page', 'title', 'records');
    	return view('frontend/layout', $data);
    }
    public function single(Request $request, $blog_slug) {
    	$rec = DB::table('blogs')->where('blog_is_deleted','N')->where('blog_slug',$blog_slug)->first();

    	$title 	= "Blog | English Aliens";
    	$page 	= "single-blog";


    	$data 	= compact('page', 'title', 'rec');
    	return view('frontend/layout', $data);
    }
}
