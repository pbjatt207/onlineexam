<?php
namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use Hash;
use App\Query;

class Test extends BaseController {
    public function index( Request $request, $id = NULL ) {

        $search = $request->input('search');
        $query = DB::table('tests as t')
                    ->Join('courses as c', 't.test_course', '=', 'c.course_id')
                    ->where('t.test_is_deleted', 'N')
                    ->select('*');

        if(!empty($request->input('search'))) {
            $search = $request->input('search');
            if(!empty($search['keyword'])) {
                $query->where('t.test_name', 'LIKE', '%'.$search['keyword'].'%');
            }

            if(!empty($search['que_course'])) {
                $query->where('c.course_name', 'LIKE', '%'.$search['que_course'].'%');
            }

            if(!empty($search['defficulty_level'])) {
                $query->where('t.test_defficulty_level', 'LIKE', '%'.$search['defficulty_level'].'%');
            }
        }

        $records = $query->paginate(10);
        $courses = DB::table('courses')->get();

        if ($request->isMethod('post')) {
            $check = $request->input('check');

            if(!empty($check)) {
                $arr = array(
                    "test_is_deleted" => "Y"
                );
                DB::table('tests')->whereIn('test_id', $check)->update( $arr );

                return redirect()->back()->with('success', 'Selected record(s) deleted.');
            }
        }


    	$page 	= "test";
    	$data 	= compact('page', 'records', 'courses', 'search');
    	return view('backend/layout', $data);
    }

    public function add( Request $request, $id = NULL ) {
        $q = new Query();
        $tests = DB::table('tests')->where('test_is_deleted', 'N')->get();
        $courses = DB::table('courses')->where('course_is_deleted', 'N')->get();
        $instruction = DB::table('instructions')->where('inst_is_deleted', 'N')->get();

        if ($request->isMethod('post')) {
            $input = $request->input('record');

            if(!empty($input)) {
                if(empty($id)) {
                    DB::table('tests')->insert( $input );
                    $id = DB::getPdo()->lastInsertId();
                    $mess = "Data inserted.";
                } else {
                    DB::table('tests')->where('test_id', $id)->update( $input );
                    $mess = "Data updated";
                }

                $slug = $q->create_slug($input['test_name'], "tests", "test_slug", "test_id", $id);
                DB::table('tests')->where('test_id', $id)->update( array('test_slug' => $slug) );


                return redirect('ea-xpanel/test/test_questions/'.$id);
            }
        }

        $page_no = $request->input('page');
        $offset  = !empty($page_no) ? $page_no - 1 : 0;
        $records = DB::table('tests')->where('test_is_deleted','N')->paginate(10);

        $edit = array();
        if(!empty($id)) {
            $edit = DB::table('tests')->where('test_id',$id)->first();
        }

        // print_r($courses);


        $page   = "add_test";
        $data   = compact('page', 'records', 'offset', 'edit', 'tests', 'id', 'courses', 'instruction');
        return view('backend/layout', $data);
    }

    public function test_questions( Request $request, $id ) {
        $que_limit = DB::table('tests')->where('test_id', $id)->select('test_no_of_question')->first();
        $limit = $que_limit->test_no_of_question;
        $count     = DB::table('test_questions')->where('test_questions_test_id', $id)->where('test_questions_is_deleted', 'N')->count();

        $mess = "Total remaining question limit is ".($limit - $count).", so please add maximum ".($limit - $count)." question(s). ";

        if ($request->isMethod('post')) {
            $check = $request->input('check');
            if(!empty($check) && $limit > $count && (($limit - $count) >= count($check))) {

                foreach($check as $que){
                    $is_exists = DB::table('test_questions')->where('test_questions_test_id', $id)->where('test_questions_question_id', $que)->first();
                    $arr = array(
                        'test_questions_test_id'    => $id,
                        'test_questions_question_id'    => $que
                    );
                    if (empty($is_exists)) {
                        DB::table('test_questions')->insert( $arr );
                        $mess = "Question(s) added into your test.";
                    } else {
                        $mess = "Question already exist.";
                    }
                }

                return redirect('ea-xpanel/test')->with('success', $mess);
            } else {
            return redirect()->back()->with('success', $mess);
        }
        }

        $get_parmas = $request->input();

        $page_no = $request->input('page');
        $offset  = !empty($page_no) ? ($page_no - 1) * 10 : 0;

        // $optsearch = DB::table('questions')->where();
        // print_r($optsearch['que_opt1']); die();

        $query   = DB::table('questions')
                    ->join('courses', 'questions.que_course', '=', 'courses.course_id')
                    ->join('subjects', 'questions.que_subject', '=', 'subjects.subject_id')
                    ->leftJoin('topics', 'questions.que_topic', '=', 'topics.topic_id')
                    ->select('questions.*','courses.course_name', 'subjects.subject_name', 'topics.topic_name')
                    ->where('que_is_deleted','N');


        $search = array();
        if(!empty($request->input('search'))) {
            $search = $request->input('search');
            if(!empty($search['keyword'])) {
                $query->orWhere('que_name', 'LIKE', '%'.$search['keyword'].'%');
            }

            if(!empty($search['keyword'])) {
                $query->orWhere('que_opt1', 'LIKE', '%'.$search['keyword'].'%');
            }

            if(!empty($search['keyword'])) {
                $query->orWhere('que_opt2', 'LIKE', '%'.$search['keyword'].'%');
            }

            if(!empty($search['keyword'])) {
                $query->orWhere('que_opt3', 'LIKE', '%'.$search['keyword'].'%');
            }

            if(!empty($search['keyword'])) {
                $query->orWhere('que_opt4', 'LIKE', '%'.$search['keyword'].'%');
            }

            // if(!empty($search['keyword'])) {
            //     $query->where('que_opt2', 'LIKE', '%'.$search['keyword'].'%');
            // }

            if(!empty($search['que_course'])) {
                $query->where('que_course', $search['que_course']);
            }

            if(!empty($search['que_defficulty_level'])) {
                $query->where('que_defficulty_level', $search['que_defficulty_level']);
            }

            if(!empty($search['que_subject'])) {
                $query->where('que_subject', $search['que_subject']);
            }

            if(!empty($search['que_topic'])) {
                $query->where('que_topic', $search['que_topic']);
            }

            if(!empty($search['que_language'])) {
                $query->where('que_language', $search['que_language']);
            }
        }

        $records = $query->paginate(10);
        $subjects = $topics = array();
        if(!empty($search)) {

            if(!empty($search['que_course'])) {
                $subjects = DB::table('subjects')->where('subject_course', $search['que_course'])->where('subject_is_deleted','N')->get();
            }

            if(!empty($search['que_subject'])) {
                $topics = DB::table('topics')->where('topic_subject', $search['que_subject'])->where('topic_is_deleted','N')->get();
            }
        }

        $courses = DB::table('courses')->where('course_is_deleted', 'N')->get();

        $page   = "test_questions";
        $data   = compact('page', 'records', 'offset', 'search', 'courses', 'subjects', 'topics', 'get_parmas', 'mess', 'id');
        return view('backend/layout', $data);
    }

    public function view_questions( Request $request, $id ) {
        if ($request->isMethod('post')) {
            $check = $request->input('check');

            if(!empty($check)) {

                DB::table('test_questions')->where('test_questions_test_id', $id)->whereIn('test_questions_question_id', $check)->delete();

                return redirect()->back()->with('success', 'Selected record(s) deleted.');
            }
        }

        $get_parmas = $request->input();

        $page_no = $request->input('page');
        $offset  = !empty($page_no) ? ($page_no - 1) * 10 : 0;

        $query = DB::table('test_questions as t')
                ->join('questions as q', 't.test_questions_question_id', '=', 'q.que_id')
                ->join('courses', 'q.que_course', '=', 'courses.course_id')
                ->join('subjects', 'q.que_subject', '=', 'subjects.subject_id')
                ->leftJoin('topics', 'q.que_topic', '=', 'topics.topic_id')
                ->where('t.test_questions_test_id', $id)
                ->where('t.test_questions_is_deleted', 'N')
                ->select('*');

        $search = array();
        if(!empty($request->input('search'))) {
            $search = $request->input('search');
            if(!empty($search['keyword'])) {
                $query->where('que_name', 'LIKE', '%'.$search['keyword'].'%');
            }

            if(!empty($search['que_course'])) {
                $query->where('que_course', $search['que_course']);
            }

            if(!empty($search['que_subject'])) {
                $query->where('que_subject', $search['que_subject']);
            }

            if(!empty($search['que_topic'])) {
                $query->where('que_topic', $search['que_topic']);
            }

            if(!empty($search['que_language'])) {
                $query->where('que_language', $search['que_language']);
            }
        }

        $records = $query->paginate(10);
        $subjects = $topics = array();
        if(!empty($search)) {

            if(!empty($search['que_course'])) {
                $subjects = DB::table('subjects')->where('subject_course', $search['que_course'])->where('subject_is_deleted','N')->get();
            }

            if(!empty($search['que_subject'])) {
                $topics = DB::table('topics')->where('topic_subject', $search['que_subject'])->where('topic_is_deleted','N')->get();
            }
        }

        $courses = DB::table('courses')->where('course_is_deleted', 'N')->get();
        $page   = "view_question";
        $data   = compact('page', 'records', 'offset', 'search', 'courses', 'subjects', 'topics', 'get_parmas', 'id');
        return view('backend/layout', $data);
    }

    public function ctest( Request $request, $slug) {
        $title  = "Tests | English Aliens";


        $user_id = session('user_auth');

        $course = DB::table('courses')->where('course_slug', $slug)->first();
        $test  = DB::table('tests')
                 ->where('test_is_deleted', 'N')
                 ->where('test_course', $course->course_id)
                 ->get();

        $page   = "test";
        $data   = compact('page', 'title', 'test', 'course');
        return view('backend/layout', $data);
    }

    public function instruction( Request $request, $id) {
        $title  = "Test Instructions | English Aliens";
        $inst   = DB::table('instructions')->where('inst_id', $id)->where('inst_is_deleted', 'N')->first();
        $page   = "instructions";
        $data   = compact('page', 'title', 'inst');
        return view('backend/layout', $data);
    }
}
