<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

class NewsController extends BaseController {
    public function index(Request $request, $slug) {
    	$title 		= "News | English Aliens";
    	$page 		= "news";

    	$records = DB::table('news')
                    ->join('news_category', 'news.news_category', '=', 'news_category.category_id')
                    ->select('news.*','news_category.*')
                    ->where('news_is_deleted','N')
                    ->where('news_is_visible','Y')
                    ->where('category_slug',$slug)
                    ->paginate(10);

    	$data 		= compact('page', 'title', 'records');
    	return view('frontend/layout', $data);
    }
    public function single(Request $request, $slug) {
    	$rec = DB::table('news')->where('news_slug',$slug)->where('news_is_deleted','N')->where('news_is_visible','Y')->first();

    	$title 	= "News | English Aliens";
    	$page 	= "single-news";


    	$data 	= compact('page', 'title', 'rec');
    	return view('frontend/layout', $data);
    }
}
