<?php

namespace App\Http\Controllers\api;


use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Model\ProfileModel as Profile;
use DB;

class ProfileController extends BaseController {
      public function index(Request $request){
        $profiles = Profile::where('candidate_is_deleted','N')->get();

        if (!$profiles->isEmpty()) {
            foreach($profiles as $key => $p) {
               if (!empty($s->candidate_image)) {
                   $profiles[$key]->candidate_image = url('imgs/candidates/'.$p->candidate_image);
               }
            }
           $re = [
            'status'  => TRUE,
            'data'    => $profiles,
            'message' => $profiles->count().'record(s) found.'
             ];
           } else{
             $re = [
             'status'  => FALSE,
             'message' => 'No record(s) found.'
           ];
           }

           return response()->json($re);
    } 
}