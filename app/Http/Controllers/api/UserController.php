<?php

namespace App\Http\Controllers\api;


use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Model\UserModel as User;
use DB;

class UserController extends BaseController {
    public function login(Request $request) {
        $post = $request->isMethod('post') ? $request->input() : [];

        if(!empty($post['user_mobile']) && !empty($post['user_password'])) {
            $isExists = User::where('user_is_deleted', 'N')
                ->where('user_login', $post['user_mobile'])
                ->first();

            if (!empty($isExists->user_id)) {
                if (password_verify($post['user_password'], $isExists->user_password)) {
                    $re = [
                        'status' => TRUE,
                        'data' => $isExists,
                        'message' => 'Logged in successfully'
                    ];
                } else {
                    $re = [
                        'status' => FALSE,
                        'message' => 'Password not matched.'
                    ];
                }
            } else {
                $re = [
                    'status' => FALSE,
                    'message' => 'Mobile no. not exists, please try with another.'
                ];
            }
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing.'
            ];
        }
        return response()->json($re);
    }
    public function register(Request $request) {
        $post = $request->isMethod('post') ? $request->input() : [];

        if(!empty($post['user_fname']) && !empty($post['user_password']) && !empty($post['user_mobile']) && !empty($post['user_courses']) )  {
            $isExists = User::where('user_is_deleted', 'N')
                ->where('user_login', $post['user_mobile'])
                ->first();

            if (empty($isExists->user_id)) {

            } else {
                $re = [
                    'status'    => FALSE,
                    'message'   => 'Mobile no. is already exists.'
                ];
            }
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing.'
            ];
        }
        return response()->json($re);
    }
}
