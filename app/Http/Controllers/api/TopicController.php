<?php

namespace App\Http\Controllers\api;


use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Model\TopicModel as Topic;
use DB;

class TopicController extends BaseController {
      public function index(Request $request){
        $subject_id = $request->input('subject_id');

        if(!empty($subject_id)) :
          $topics = Topic::where('topic_is_deleted','N')->where('topic_subject', $subject_id)->get();

          if (!$topics->isEmpty()) {
            $re = [
              'status'  => TRUE,
              'data'    => $topics,
              'message' => $topics->count().'record(s) found.'
            ];
          } else{
            $re = [
             'status'  => FALSE,
             'message' => 'No record(s) found.'
            ];
          }
        else:
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing.'
            ];
        endif;
        
        return response()->json($re);
    } 
}