<?php

namespace App\Http\Controllers\api;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Model\CourseModel as Course;
use DB;

class CourseController extends BaseController {
    public function index(Request $request) {
    	$courses = Course::where('course_is_deleted','N')->get();

    	if(!$courses->isEmpty()) {
    	    foreach($courses as $key => $c) {
                if(!empty($c->course_image)) {
                    $courses[$key]->course_image = url('imgs/courses/'.$c->course_image);
                }
            }
            $re = [
                'status'    => TRUE,
                'data'      => $courses,
                'message'   => $courses->count().' record(s) found.'
            ];
        } else {
    	    $re = [
    	        'status'    => FALSE,
                'message'   => 'No record(s) found.'
            ];
        }

        return response()->json($re);
    }
}
