<?php

namespace App\Http\Controllers\api;


use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Model\SubjectModel as Subject;
use DB;

class SubjectController extends BaseController {
      public function index(Request $request){
        $course_id = $request->input('course_id');

        if(!empty($course_id)) :
            $subjects = Subject::where('subject_is_deleted','N')->where('subject_course', $course_id)->get();

            if (!$subjects->isEmpty()) {
                foreach($subjects as $key => $s) {
                   if (!empty($s->subject_image)) {
                       $subjects[$key]->subject_image = url('imgs/subjects/'.$s->subject_image);
                   }
                }
                $re = [
                    'status'  => TRUE,
                    'data'    => $subjects,
                    'message' => $subjects->count().'record(s) found.'
                ];
            } else {
                $re = [
                    'status'  => FALSE,
                    'message' => 'No record(s) found.'
               ];
            }
        else:
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing.'
            ];
        endif;

        return response()->json($re);
    } 
}