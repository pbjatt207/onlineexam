<?php

namespace App\Http\Controllers\api;


use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Model\QuestionModel as Question;
use DB;

class QuestionController extends BaseController {
      public function index(Request $request){
         $topic_id = $request->input('topic_id');

      if(!empty($topic_id)) :
        $questions = Question::where('que_is_deleted','N')->where('que_topic', $topic_id)->limit(10)->get();

        if (!$questions->isEmpty()) {
          $re = [
            'status'  => TRUE,
            'data'    => $questions,
            'message' => $questions->count().'record(s) found.'
          ];
        } else {
               $re = [
               'status'  => FALSE,
               'message' => 'No record(s) found.'
             ];
        }

      else:
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing.'
            ];
      endif;

      return response()->json($re);
    } 
}