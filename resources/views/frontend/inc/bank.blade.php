<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<base href="{{ url('/').'/' }}">
		<title>Bank</title>
		{{ HTML::style('css/bootstrap.min.css') }}
		{{ HTML::style('icomoon/style.css') }}
		{{ HTML::style('css/style.css') }}
		{{ HTML::style('css/responsive.css') }}
		<link rel="icon" href="{{ url('site/favicon.ico') }}">
	</head>
	<body>
		<section class="bank">
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="bank_heading text-center">
							<strong>CWE Online Assessment Mock Test Assessment</strong>
						</div>
						<div class="t-blog">
							<a href="">
								<div class="topic">Reasoning Aptitude</div>
							</a>
							<a href="">
								<div class="topic">English Language</div>
							</a>
							<a href="">
								<div class="topic">Quantitative Aptitude</div>
							</a>
							<a href="">
								<div class="topic">General Awareness</div>
							</a>
							<a href="">
								<div class="topic">Computer Awareness</div>
							</a>
							<div class="Sections">Sections</div>
						</div>

						<div class="c-panel">
							<strong>Question No. 5</strong>
							<div class="view_in">
								<strong>View In: </strong>
								<select>
									<option>English/Hindi</option>
									<option>English</option>
									<option>Hindi</option>
								</select>
							</div>
						</div>
						<div> 
							<div class="question mt-4 mb-4">
								<div class="questions">
									Which one set of letters when sequentially placed at the gaps in the given letter series shall complete it?
									<div>(A) Which one set of letters when sequentially</div>
									<div>(B) Which one set of letters when sequentially</div>
									<div>(C) Which one set of letters when sequentially</div>
									<div>(D) Which one set of letters when sequentially</div>
								</div>
							</div>
							<form class="test-form">
								@php $sn = 0; @endphp
								@for($i = 1; $i <= 5; $i++)
								<label class="answers d-block">
									<input type="radio" name="option" class="form-control test_radio d-inline">
									{{ ++$sn }}) Answer
								</label>
								@endfor

								<div class="bank-footer">
									<div class="row">
										<div class="col-sm-6">
											<a href="" class="mnr">
												Mark for Review & Next
											</a>

											<a href="" class="mnr">
												Mark for Review & Next
											</a>
										</div>
										<div class="col-sm-3"></div>
										<div class="col-sm-3">
											<button class="bank-save">
												Save & Next
											</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="row mt-3">
							<div class="col-sm-6">
								<img class="bank_image" src="{{ url('imgs/dummy-user-old.png') }}">
							</div>
							<div class="col-sm-6 pt-4">
								<div class="timer-bank">
									<strong>Time Left:</strong>
									<span>0:59:54</span>
								</div>
								<div>
									Candidate
								</div>
							</div>
						</div>
						<div class="questin_p">
							You are Viewing <strong>Reasoning Aptitude</strong> Action
						</div>
						<div class="bank-que-pallete mt-5">
							Question Pallete:
						</div>
						<div class="bank-questions mt-3">
							<div id="queStatus" class="que_status1 clearfix text-center mb-2">
								@for($i = 1, $sn = 1; $i <= 100; $i++, $sn++)
								<div class=" @if($sn == 1) bank_que_box-na @elseif($sn == 3) bank_que_box-na @endif bank_que_box-nv">{{ sprintf('%02d', $sn) }}</div>
								@endfor
							</div>
						</div>
						<strong>Legend:</strong>
						<div class="row mt-3">
							<div class="col-sm-6 mb-2">
								<div class="legend">
									<img src="{{ url('imgs/review_answer.png') }}">Answered
								</div>
							</div>
							<div class="col-sm-6 mb-2">
								<div class="legend">
									<img src="{{ url('imgs/not_answered.png') }}">Not Answered
								</div>
							</div>
							<div class="col-sm-6">
								<div class="legend">
									<img src="{{ url('imgs/review.png') }}">Marked
								</div>
							</div>
							<div class="col-sm-6">
								<div class="legend">
									<img src="{{ url('imgs/not_visited.png') }}">Not Visisted
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</body>
</html>