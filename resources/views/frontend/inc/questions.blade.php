<section class="bottum">
	<div class="container">
		 <div class="row">
			<div class="col-xl-3">
				@include('frontend.common.side_courses')
			</div>

		 	 <div class="col-xl-9">
		 	 	  <h3 class="heading"> {!! $course->course_name.' &raquo; '.$subject->subject_name.' &raquo; '.$topic->topic_name !!} </h3>
		 	 	  @if(!empty($questions) && !$questions->isEmpty())
		 	 	  <div>
		 	 	  		@foreach($questions as $que)
		 	 	  	   	<article class="queans">
	                        <div class="questionTitle text-justify">{!! $que->que_name !!}</div>
	                        <div class="row answersList">
	                        	@if(!empty($que->que_opt1))
	                            <div class="col-md-6 mrgnTop10">
	                                <strong class="float-left">A. &nbsp;</strong> {!! $que->que_opt1 !!}
	                            </div>
	                            @endif
	                            @if(!empty($que->que_opt2))
	                            <div class="col-md-6 mrgnTop10">
	                                <strong class="float-left">B. &nbsp;</strong> {!! $que->que_opt2 !!}
	                            </div>
	                            @endif
		                        <div class="clearfix"></div>
	                            @if(!empty($que->que_opt3))
	                            <div class="col-md-6 mrgnTop10">
	                                <strong class="float-left">C. &nbsp;</strong> {!! $que->que_opt3 !!}
	                            </div>
	                            @endif
	                            @if(!empty($que->que_opt4))
	                            <div class="col-md-6 mrgnTop10">
	                                <strong class="float-left">D. &nbsp;</strong> {!! $que->que_opt4 !!}
	                            </div>
	                            @endif
	                            <div class="clearfix"></div>
	                            <div class="col-sm-12 explanationDiv">
	                                <p>
	                                    <strong>Answer:</strong> Option
	                                    <strong>{{ $que->que_answer }}</strong>
	                                </p>
	                                @if(!empty($que->que_explaination))
	                                <p class="text-justify">
	                                	<strong class="float-left">Explaination:&nbsp;</strong>
	                                	{!! $que->que_explaination !!}
	                                </p>
	                                @endif
	                            </div>
	                            <div class="clearfix"></div>
	                            <div class="col-sm-12" style="margin-top:10px;">
	                                <a href="#" class="viewAns"> View Answer </a>
	                            </div>
	                        </div>
	                    </article>
		 	 	  	   	@endforeach

		 	 	  	   	{{ $questions->links() }}
		 	 	  </div>
		 	 	  @elseif(!empty($paragraphs) && !$paragraphs->isEmpty())
		 	 	  <div>
		 	 	  	@foreach($paragraphs as $para)
		 	 	  		<div class="text-justify form-group">{!! $para->para_description !!}</div>
		 	 	  		@if(!empty($para->questions))
			 	 	  		@foreach($para->questions as $que)
			 	 	  			<article class="queans">
			                        <div class="questionTitle text-justify">{!! $que->que_name !!}</div>
			                        <div class="row answersList">
			                        	@if(!empty($que->que_opt1))
				                            <div class="col-md-6 mrgnTop10">
				                                <strong class="float-left">A. &nbsp;</strong> {!! $que->que_opt1 !!}
				                            </div>
			                            @endif
			                            @if(!empty($que->que_opt2))
				                            <div class="col-md-6 mrgnTop10">
				                                <strong class="float-left">B. &nbsp;</strong> {!! $que->que_opt2 !!}
				                            </div>
			                            @endif
				                        <div class="clearfix"></div>
			                            @if(!empty($que->que_opt3))
				                            <div class="col-md-6 mrgnTop10">
				                                <strong class="float-left">C. &nbsp;</strong> {!! $que->que_opt3 !!}
				                            </div>
			                            @endif
			                            @if(!empty($que->que_opt4))
				                            <div class="col-md-6 mrgnTop10">
				                                <strong class="float-left">D. &nbsp;</strong> {!! $que->que_opt4 !!}
				                            </div>
			                            @endif
			                            <div class="clearfix"></div>
			                            <div class="col-sm-12 explanationDiv">
			                                <p>
			                                    <strong>Answer:</strong> Option
			                                    <strong>{{ $que->que_answer }}</strong>
			                                </p>
			                                @if(!empty($que->que_explaination))
				                                <p class="text-justify">
				                                	<strong class="float-left">Explaination:&nbsp;</strong>
				                                	{!! $que->que_explaination !!}
				                                </p>
			                                @endif
			                            </div>
			                            <div class="clearfix"></div>
			                            <div class="col-sm-12" style="margin-top:10px;">
			                                <a href="#" class="viewAns"> View Answer </a>
			                            </div>
			                        </div>
			                    </article>
			 	 	  		@endforeach
			 	 	  	@endif
		 	 	  	@endforeach

		 	 	  	{{ $paragraphs->links() }}
		 	 	  </div>
		 	 	  @else
		 	 	  <div class="no_records_found">
		 	 	  	No questions(s) found in <strong>{{ $course->course_name }}</strong>, <strong>{{ $subject->subject_name }}</strong>, <strong>{{ $topic->topic_name }}</strong>
		 	 	  </div>
		 	 	  @endif
		 	 </div>
		 </div>
	</div>
</section>