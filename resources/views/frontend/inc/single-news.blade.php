<section class="b-block">
	<div class="container">
		<h1 class="heading"> {{ $rec->news_title }} </h1>

		<div class="blog-detail">

			<div class="text-justify">
				{!! $rec->news_description !!}
			</div>
		</div>
	</div>
</section>
