<section class="bottum">
	<div class="container">
		 <div class="row">
		 	<div class="col-xl-3">
		 	 	@include('frontend.common.side_courses')
		 	</div>

		 	<div class="col-xl-9">
		 	 	  <h3 class="heading"> {!! $course->course_name.' &raquo; '.$subject->subject_name !!} </h3>

		 	 	  <div class="sub-part">
		 	 	  	   On this page you will find all the important topics of <strong>{{ $course->course_name }}</strong> (English, math, reasoning, General Knowledge and General Science) and completely based on <strong>{{ $course->course_name }}</strong> pattern.  All the questions are very important and have already been asked by <strong>{{ $course->course_name }}</strong> in previous years question papers. The following section has questions from these sections (spotting common errors, fill in the blanks, synonyms, antonyms, direct/indirect narration (speech), spelling mistakes, idioms & phrases, active/passive voice of verbs, one word substitution, improvement of sentences, cloze passages & comprehension passages.). In this section all questions are validated by experts and we have made sure you get all the correct answers with explanation. There are practice and sample questions with answers from all the subjects (English, math, reasoning, General Knowledge and General Science) and from latest years as well 2016 & 2017. Let us tell you on the website you can give <strong>FREE SSC TEST SERIES</strong>  ONLINE TEST SERIES AS PER SSC CGL, SSC 10+2 & CPO PATTERN.
		 	 	  </div>

		 	 	  <div class="row">
		 	 	  		@foreach($topics as $topic)
		 	 	  	   	<div class="col-sm-3">
		 	 	  	   		<div class="mainTopic">
	                            <div class="topicList topicLists">
	                                <a href="{{ url('question/'.$course->course_slug.'/'.$subject->subject_slug.'/'.$topic->topic_slug) }}">
	                                    <strong>{{ $topic->topic_name }}</strong>
	                                </a>
								</div>
	                        </div>
		 	 	  	   	</div>
		 	 	  	   	@endforeach
		 	 	  </div>
		 	 </div>
		 </div>
	</div>
</section>