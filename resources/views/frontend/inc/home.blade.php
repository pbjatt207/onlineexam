<section class="bottum">
	<div class="container">
		<div class="row">
			<div class="col-xl-3">
				@include('frontend.common.side_courses')
			</div>

		 	<div class="col-xl-9">
		 	 	  <h3 class="heading">{{ $site->site_home_title1 }}</h3>
		 	 	  <div class="sub-part">{!! $site->site_home_para1 !!}</div>

		 	 	  <div class="row">
		 	 	  		@foreach($courses as $course)
		 	 	  	   	<div class="col-xl-3">
		 	 	  	   	    <div class="d-flex flex-column bd-highlight mb-3">
		 	 	  	   	    	<div class="p-3 bd-highlight">{{ $course->course_name }}</div>
		 	 	  	   	    	@foreach($course->subjects as $subject)
								<div class="p-2 bd-highlight">
									<a href="{{ url($course->course_slug.'/'.$subject->subject_slug) }}">{{ $subject->subject_name }}</a>
								</div>
								@endforeach
							</div>
		 	 	  	   	</div>
		 	 	  	   	@endforeach
		 	 	  </div>

                  <center>
			 	 	  <div class="course">
			 	 	  	   	<a href="{{ url('ea-xpanel') }}" target="_blank" class="course-sub">Test Series</a>
			 	 	  </div>
			 	  </center> 	  
		 	 
		 	      <!-- <div class="heading">
		 	 	  	    test your grammar knowledge
		 	 	  </div>

		 	 	  <div class="bottom"> </div>


		 	 	  <div class="sub-part">
		 	 	  	   In this section of the website you can check your grammar&nbsp;knowledge and level by giving online exams. You will find exams for all the users as per their levels (<strong>Beginner</strong>, <strong>Intermediate</strong>&nbsp;and <strong>Advanced</strong>). You will get your marks with the correct answers after submitting the exams.
		 	 	  </div>

		 	 	  <div class="d-flex flex-row bd-highlight mb-3">
						<a href="" class="p-4 bd-highlight">
							<div class="verb">
							    T
							</div>
                           <span class="chapter">
                           	    Tenses
                           </span>
						</a>
						<a href="" class="p-4 bd-highlight">
							<div class="verb1">
							   M
							</div> 
                           <span class="chapter">
                           	    Modal Verbs
                           </span>
						</a>
						<a href="" class="p-4 bd-highlight">
						   <div class="verb2">
						      P
						   </div>
                           <span class="chapter">
                           	    Preposition
                           </span>
						</a>
						<a href="" class="p-4 bd-highlight">
						   <div class="verb3">
						      D
						   </div>	
                           <span class="chapter">
                           	    Direct & Indirect Speech
                           </span>
						</a>

						<a href="" class="p-4 bd-highlight">
							<div class="verb4">
								S
							</div> 
                            <span class="chapter">
                           	    SSC (CGL - CPO) QUICK SKILL ONLINE TEST     
                           </span>
						</a>
				  </div>

				   <div class="d-flex flex-row bd-highlight mb-3">
						<a href="" class="p-4 bd-highlight">
						  <div class="verb5">
						     V
						   </div>	
                           <span class="chapter">
                           	    Verbs
                           </span>
						</a>

						<a href="" class="p-4 bd-highlight">
						   <div class="verb6">
						   	   I
						   </div>	
                           <span class="chapter">
                           	    If Conditions
                           </span>
						</a>

						<a href="" class="p-4 bd-highlight">
						   <div class="verb7">
						   	  A
						   </div>	
                           <span class="chapter">
                           	    Articles
                           </span>
						</a>

						<a href="" class="p-4 bd-highlight">
						   <div class="verb8">
						   	   A
						   </div>	
                           <span class="chapter">
                           	    Active & Passive Verbs
                           </span>
						</a>
						<a href="" class="p-4 bd-highlight">
							<div class="verb9">
							  C
							</div> 
                            <span class="chapter">
                           	    Comming Soon 
                           </span>
						</a>
				  </div> -->
				  



		 	 	  <h3 class="heading">{{ $site->site_home_title2 }}</h3>
		 	 	  <div class="sub-part">{!! $site->site_home_para2 !!}</div>
		 	 </div>
		 </div>
	</div>
</section>