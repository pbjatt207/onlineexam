<section class="bottum">
		<div class="container">
			 <div class="row">
				 	 <div class="col-xl-3">
				 	 	<ul id="navone">
				 	 		<li class="cs">complete solution for all competition exam</li>
		                    <li class="ssc"><a href="{{ url('english') }}">SSC CGL TIER-I & II</a></li>
		                    <li><a href="">NDA</a></li>
		                    <li><a href="">BANK P.O</a></li>
		                    <li><a href="">BANK ALERT</a></li>
		                    <li><a href="">CDS</a></li>
		                    <li><a href="">SSC CHSL(10+2)</a></li>
		                    <li><a href="">X & Y GROUPS</a></li>
		                    <li><a href="">LDC CLERK</a></li>
		                    <li class="cs">test your grammar knowledge</li>
		                    <li><a href="">SSC CGL TIER-I & II</a></li>
		                    <li><a href="">NDA</a></li>
		                    <li><a href="">BANK P.O</a></li>
		                    <li><a href="">BANK ALERT</a></li>
		                    <li><a href="">CDS</a></li>
		                    <li><a href="">SSC CHSL(10+2)</a></li>
		                    <li><a href="">X & Y GROUPS</a></li>
		                    <li><a href="">LDC CLERK</a></li>
				 	 	</ul>
				 	 </div>

			 	 <div class="col-xl-9">
			 	 	  <div class="heading">
			 	 	  	    complete solution for all competition exam
			 	 	  </div>

			 	 	  <div class="bottom">
			 	 	  	    
			 	 	  </div>

			 	 	  <div class="sub-part">
			 	 	  	  On this page you will find all the important topics of <strong>SSC CGL TIER - I &amp; II</strong> (English, math, reasoning, General Knowledge and General Science) and completely based on <strong>SSC CGL</strong> and <strong>SSC</strong> <strong>CPO</strong> pattern.&nbsp; All the questions are very important and have already been asked by <strong>SSC</strong> in previous years question papers. The following section has questions from these sections (spotting common errors, fill in the blanks, synonyms, antonyms, direct/indirect narration (speech),&nbsp;spelling mistakes, idioms &amp; phrases, active/passive voice of verbs, one word substitution, improvement of sentences, cloze passages &amp; comprehension passages.). In this section all questions are validated by experts and we have made sure you get all the correct answers with explanation. There are practice and sample questions with answers from all the subjects (English, math, reasoning, General Knowledge and General Science) and from latest years as well 2016 &amp; 2017. Let us tell you on the website you can give <a href=""><strong><u>FREE ONLINE TEST SERIES AS PER SSC CGL, SSC 10+2 &amp; CPO PATTERN</u>.</strong></a>
			 	 	  </div>

			 	 	 <div class="row f-r">
	                           <a href="" class="finding"><div class="p-11">Finding / Spotting Errors</div></a>
	                           <a href="" class="spelling"><div class="p-12">Spellings</div></a>
	                           <a href=""><div class="p-11">Direct & Indirect Speech</div></a>
	                           	<a href=""> <div class="p-11">Active and Passive Voice</div></a>
                      </div>

                      <div class="row f-s">
	                           <a href="" class="finding"><div class="p-11">Antonyms / Opposite Words</div></a>
	                           <a href="" class="spelling"><div class="p-12">Passages</div></a>
	                           <a href=""><div class="p-11">Sentence Improvement</div></a>
	                           	<a href=""> <div class="p-11">Idioms and Phrases</div></a>
                      </div>

                      <div class="row f-t">
	                           <a href="" class="finding"><div class="p-11">One Word Substitution</div></a>
	                           <a href="" class="spelling"><div class="p-12">Fill in the Blanks</div></a>
	                           <a href=""><div class="p-11">Synonyms / Same Words</div></a>
	                           	<a href=""> <div class="p-11">Sentence Rearrangement</div></a>
                      </div>

			 	 </div>
			 </div>
		</div>
</section>




