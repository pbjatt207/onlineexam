<section class="b-block">
	<div class="container">
		@foreach($records as $rec)
		<h1 class="heading">{{ $rec->reading_title }}</h1>
		<div class="text-success">
			<i class="icon-calendar"></i> {{ date('F d, Y', strtotime($rec->reading_created_on)) }}
		</div>
		<div class="blog-detail text-justify">
			{!! $rec->reading_description !!}
		</div>
		@endforeach

		{{ $records->links() }}
	</div>
</section>
