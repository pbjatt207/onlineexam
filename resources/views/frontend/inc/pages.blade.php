<section class="b-block">
	<div class="container">
		<h1 class="heading"> {{ $rec->page_title }} </h1>

		<div class="blog-detail">
			<div>
				<img src="{{ !empty($rec->page_image) ? url('imgs/pages/'.$rec->page_image) : url('imgs/no-image.png') }}" alt="{{ $rec->page_title }}" title="{{ $rec->page_title }}" class="blog-img">
			</div>

			<div class="text-justify">
				{!! $rec->page_description !!}
			</div>
		</div>
	</div>
</section>
