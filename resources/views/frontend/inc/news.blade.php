<section class="b-block">
	<div class="container">
		<h1 class="heading"> NEWS </h1>

		<div class="row">
			@foreach($records as $rec)
			<div class="col-sm-6 col-lg-4">
				<div class="blog-block">

					<div class="fblog-head">{{ $rec->news_title}}</div>

					<p class="blog-desc">
						 {{ substr( strip_tags($rec->news_description), 0, 120) }} &hellip;
					</p>

					<div class="read">
					 	<a href="{{ url('news/single/'.$rec->news_slug) }}">Read More<i class="icon-long-arrow-right"></i></a>
			        </div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</section>
