<section class="contact-block">
	<div class="container">
		<h1 class="heading">contact us</h1>

		   <div class="row form-group">
			   <div class="col-xl-8">
			   	    <section class="info">
			   	    	<div class="info-head">
			   	    		<i class="icon-envelope"></i>Feel free to contact us
			   	    	</div>
                        
                        <div class="row">
                            <div class="col-sm-6">
                            	<div class="form-group">
                            		<input type="text" name="" class="form-control" placeholder="Name" required>
                            	</div>
                            </div>

                            <div class="col-xl-6">	
                            	<div class="form-group">
                            		<input type="email" name="" class="form-control" placeholder="Email" required>
                            	</div>
                            </div>
                         </div>  

                         <div class="form-group">
                         	 <input type="text" name="" class="form-control" placeholder="Subject">
                         </div> 

                         <div class="form-group">
                         	 <textarea class="form-control" placeholder="Message" rows="8" cols="10"></textarea>
                         </div>

                          <div class="text-right">
                          	 <button class="btn btn-success"> Send <i class="icon-send-o"></i></button>
                          </div>
			   	    </section>
			   </div>

			  <div class="col-xl-4">
			  	   <section class="info">
			  	   	    <div class="info-head">
			   	    		<i class="icon-office"></i>Our Office
			   	    	</div>

			   	    	<div class="tag">{{ $site->site_title }}</div>

			   	    	<div class="location">
			   	    		<div class="clearfix">
				   	    		<div class="float-left">&nbsp;<i class="icon-location2"></i> &nbsp;</div>
				   	    		<div class="float-left">{!! nl2br($site->site_address) !!}</div>
				   	    	</div>
			   	    	</div>

			   	    	<div class="mail">
			   	    		 <i class="icon-mail1"></i><a href="mailto: {{ $site->site_email }}"> {{ $site->site_email }}</a>
			   	    	</div>
			  	   </section>
			  </div>
		   </div>
	</div>

	<div class="container">
	     <section class="map"> 
			 <div class="map-head">
			 	  <i class="icon-map-marker"></i> <span class="ol">Our Location</span>
			 </div>

			 <div class="google-map">
			 	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7155.240413006396!2d73.030554!3d26.273987!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x62e993805ae7b981!2sEnglish+Aliens!5e0!3m2!1sen!2sin!4v1556220440838!5m2!1sen!2sin" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
			 </div>
	      </section>
	</div>



</section>

