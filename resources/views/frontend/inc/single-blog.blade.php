<section class="b-block">
	<div class="container">
		<h1 class="heading"> {{ $rec->blog_title }} </h1>

		<div class="blog-detail">
			<div>
				<div class="blog-date">
					<div class="bdate">{{ date('d', strtotime($rec->blog_created_on)) }}</div>
					<div class="bmon-yr">{{ date('M y', strtotime($rec->blog_created_on)) }}</div>
				</div>
				<img src="{{ !empty($rec->blog_image) ? url('imgs/blogs/'.$rec->blog_image) : url('imgs/no-image.png') }}" alt="{{ $rec->blog_title }}" title="{{ $rec->blog_title }}" class="blog-img">
			</div>

			<div class="text-justify">
				{!! $rec->blog_description !!}
			</div>
		</div>
	</div>
</section>
