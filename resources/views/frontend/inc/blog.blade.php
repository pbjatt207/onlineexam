<section class="b-block">
	<div class="container">
		<h1 class="heading"> BLOG </h1>

		<div class="row">
			@foreach($records as $rec)
			<div class="col-sm-6 col-lg-4">
				<div class="blog-block">
					<div>
						<div class="blog-date">
							<div class="bdate">{{ date('d', strtotime($rec->blog_created_on)) }}</div>
							<div class="bmon-yr">{{ date('M y', strtotime($rec->blog_created_on)) }}</div>
						</div>
						<img src="{{ !empty($rec->blog_image) ? url('imgs/blogs/'.$rec->blog_image) : url('imgs/no-image.png') }}" alt="{{ $rec->blog_title }}" title="{{ $rec->blog_title }}" class="blog-img">
					</div>

					<div class="fblog-head">{{ $rec->blog_title }}</div>

					<p class="blog-desc">
						 {{ substr( strip_tags($rec->blog_description), 0, 120) }} &hellip;
					</p>

					<div class="read">
					 	<a href="{{ url('blog/'.$rec->blog_slug) }}">Read More<i class="icon-long-arrow-right"></i></a>
			        </div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</section>
