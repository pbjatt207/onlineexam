@php
  $category = DB::table('news_category')->where('category_is_deleted', 'N')->get();
@endphp
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<base href="{{ url('/').'/' }}">
	<title>{{ $title }}</title>
	{{ HTML::style('css/bootstrap.min.css') }}
	{{ HTML::style('icomoon/style.css') }}
	{{ HTML::style('css/style.css') }}
	{{ HTML::style('css/responsive.css') }}
	<link rel="icon" href="{{ url('site/'.$site->site_favicon) }}">
</head>
<body>

<header>
	  <div class="container">
        	<nav class="navbar navbar-expand-lg" id="navbar">
			  	<a class="navbar-brand" href="{{ url('/')  }}">
			  		<img src="{{ url('site/'.$site->site_logo) }}" class="logo" title="{{ $site->site_title }}" alt="{{ $site->site_title }}">
			  		<img src="{{ url('site/'.$site->site_logo2) }}" class="logo2" title="{{ $site->site_title }}" alt="{{ $site->site_title }}">
			  	</a>

				    <ul class="navbar-nav ml-auto">
                       <li>
                       	   <a href="{{ url('/')  }}" class="nav-link">HOME</a>
                       </li>

				       <li class="nav-item">
				         	<a href="#" class="nav-link">NEWS UPDATE <i class="icon-keyboard_arrow_down"></i></a>
				          	<ul class="drop">
				          		@foreach($category as $rec)
				          	 	<li><a href="{{ url('news/'.$rec->category_slug) }}">{{ $rec->category_name }}</a></li>
				          	 	@endforeach
				          	</ul>
				        </li>

	                    <li class="nav-item ">
					        <a href="{{ url('blog') }}" class="nav-link">BLOG</a>
					    </li>

					    <li class="nav-item ">
					        <a href="{{ url('reading') }}" class="nav-link">READING</a>
					    </li>


					    <li class="nav-item ">
					        <a href="{{ url('contact') }}" class="nav-link">CONTACT</a>
					    </li>

                             <li class="nav-item ">
					        <a href="{{ url('ea-xpanel') }}" target="_blank" class="nav-link"><i class="icon-person"></i> LOGIN</a>
					    </li>

				    </ul>
			</nav>
        </div>
</header>
<div class="nav-icon">
	<span></span>
	<span></span>
	<span></span>
</div>
