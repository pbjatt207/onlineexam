@php
	$courses = DB::table('courses')->where('course_is_deleted', 'N')->get();

	foreach($courses as $key => $crs) {
		$subjects = DB::table('subjects')->where('subject_is_deleted', 'N')->where('subject_course', $crs->course_id)->get();
		$courses[$key]->subjects = $subjects;
	}
@endphp
<ul id="navone">
	<li class="cs">{{ $site->site_home_title1 }}</li>
	@foreach($courses as $course)
		<li>
			<a href="#" title="{{ $course->course_name }}">
				{{ $course->course_name }}
				<i class="acc-icon icon-plus"></i>
			</a>
			<ul>
				@foreach($course->subjects as $subject)
					<li @if($crs->course_id == $course->course_id) {{ 'class=active' }} @endif><a href="{{ url($course->course_slug.'/'.$subject->subject_slug) }}" title="{{ $subject->subject_name }}">{{ $subject->subject_name }}</a></li>
				@endforeach
			</ul>
		</li>
	@endforeach
</ul>