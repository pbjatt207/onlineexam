@php
  $category = DB::table('news_category')->where('category_is_deleted', 'N')->get();
  $setting = DB::table('settings')->get();
@endphp
<footer>
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<h3>Important Links</h3>
				<ul>
					<li><a href="" title="">Home</a></li>
					<li><a href="{{ url('page/about-us')  }}" title="">About Us</a></li>
					<li><a href="{{ url('blog') }}" title="">Blog</a></li>
					<li><a href="{{ url('reading') }}" title="">Reading</a></li>
					<li><a href="{{ url('contact') }}" title="">Contact Us</a></li>
				</ul>
			</div>
			<div class="col-sm-3">
				<h3>News Update</h3>
				<marquee direction='up' onmouseover="stop()" onmouseout="start()">
					<ul>
						@foreach($category as $rec)
		          	 	<li><a href="{{ url('news/'.$rec->category_slug) }}">{{ $rec->category_name }}</a></li>
		          	 	@endforeach
					</ul>
				</marquee>
			</div>
			<div class="col-sm-3">
				<h3>Terms &amp; Privacy</h3>
				<ul>
					<li><a href="{{ url('page/terms-amp-conditions')  }}" title="">Terms &amp; Conditions</a></li>
					<li><a href="{{ url('page/privacy-policy') }}" title="">Privacy Policy</a></li>
					<li><a href="{{ url('page/terms-of-use') }}" title="">Terms Of Use</a></li>
				</ul>
			</div>
			<div class="col-sm-3">
				<h3>Connect Us</h3>
				<ul>
					@foreach($setting as $set)
					@endforeach
					<li><a href="" title=""><i class="icon-whatsapp"></i> +91 {{ $set->site_phone }}</a></li>
					<li><a href="" title=""><i class="icon-phone"></i> +91 {{ $set->site_phone }}</a></li>
					<li><a href="" title=""><i class="icon-envelope-o"></i> {{ $set->site_email }}</a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>
<a href="" id="return">
	<i class="icon-keyboard_arrow_up">
		
	</i>
</a>

	{{ HTML::script('js/jquery.min.js') }}
	{{ HTML::script('js/popper.min.js') }}
    {{ HTML::script('js/bootstrap.min.js') }}
	{{ HTML::script('js/custom.js') }}
	
	<!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-148578216-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-148578216-1');
    </script>
</body>
</html>