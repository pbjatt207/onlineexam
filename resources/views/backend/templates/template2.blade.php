<div id="onlineTest" class="bank d-none">
	<div class="container-fluid">
		<form class="" id="onlineExam" method="post">
		<input type="hidden" name="test_id" value="<?php echo $test->test_id ?>">
			<input type="hidden" name="total_questions" id="total_questions" value="{{ count($questions) }}">
		@csrf
			<div class="row main-bank">
				<div class="col-sm-9">
					<div class="bank_heading text-center">
						<strong>CWE Online Assessment Mock Test Assessment</strong>
					</div>
					<div class="t-blog">
						<a href="">
							<div class="topic">Reasoning Aptitude</div>
						</a>
						<a href="">
							<div class="topic">English Language</div>
						</a>
						<a href="">
							<div class="topic">Quantitative Aptitude</div>
						</a>
						<a href="">
							<div class="topic">General Awareness</div>
						</a>
						<a href="">
							<div class="topic">Computer Awareness</div>
						</a>
						<div class="Sections">Sections</div>
					</div>

					<div class="c-panel">
						<strong>Question No. 5</strong>
						<div class="view_in">
							<strong>View In: </strong>
							<select>
								<option>English/Hindi</option>
								<option>English</option>
								<option>Hindi</option>
							</select>
						</div>
					</div>
					<div> 
						@php $sn = 0; @endphp
							@foreach($questions as $que)
								@php $sn++ @endphp
							<div class="que-ans @if($sn == $curQue) active @else d-none @endif" id="question-block-{{ $que->que_id }}">
								<input type="hidden" name="is_marked[{{ $que->que_id }}]" id="is_marked_{{ $sn }}" value="{{ intval(@$arr['is_marked'][$que->que_id]) }}">
								<div class="question mt-4 mb-4">
									<div class="float-left mr-3">Q{{ $sn }}.</div>
									<div class="questions">{!! $que->que_name !!}</div>
								</div>
								@for($i = 1; $i <= 4; $i++)
								@php
									if($i == 1){
										$val = 'A';
									} elseif($i == 2){
										$val = 'B';
									} elseif($i == 3){
										$val = 'C';
									} else{
										$val = 'D';
									}

								@endphp

								<label class="answers d-block">
									<div class="float-left mr-2">
										<input type="radio" name="option[{{ $que->que_id }}]" value="{{ $val }}" class="form-control test_radio d-inline" @if(@$arr['option'][$que->que_id] == $i) checked @endif>
									{{ $i }})
									</div> {!! $que->{"que_opt".$i} !!}
								</label>
								@endfor
							</div>
							@endforeach
						<form class="test-form">
							<div class="bank-footer">
								<div class="row">
									<div class="col-sm-6">
										<a class="mnr" id="next-save-bank">
											Mark for Review & Next
										</a>

										<a id="clear-bank" class="mnr">
											Clear Responce
										</a>
									</div>
									<div class="col-sm-3"></div>
									<div class="col-sm-3">
										<button class="bank-save">
											<a style="color: #fff;" href="{{ url('ea-xpanel/feedback/'.$test->test_id) }}" >Save & Next </a>
										</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="row mt-3">
						<div class="col-sm-6">
							<img class="pro_image" src="{{ url('imgs/candidate/'.$user_info->candidate_signature) }}">
						</div>
						<div class="col-sm-6 pt-4">
							<div class="timer-bank">
								<strong>Time Left:</strong>
								<strong id="demo" class="timer_box">
									00:59:54 hrs
								</strong>
								<input type="hidden" name="totalRemains" id="totalRemains">
							</div>
							<div>
								Candidate
							</div>
						</div>
					</div>
					<div class="questin_p">
						You are Viewing <strong>Reasoning Aptitude</strong> Action
					</div>
					<div class="bank-que-pallete mt-2">
						Question Pallete:
					</div>
					<div class="bank-questions mt-3">
						<div id="queStatus" class="que_status1 clearfix text-center mb-2">
							@php $i = 0; @endphp
							@foreach($questions as $que) 
							@php $class = ""; @endphp
							@if(!empty($arr['option'][$que->que_id]))
								@php
								$class = $arr['is_marked'][$que->que_id] ? "bank_que_box-a" : "bank_que_box-a";
								@endphp
							@elseif(!empty($arr['is_marked'][$que->que_id]))
								@php
									$class = "";
								@endphp
							@endif
							<div class="{{ $class }} bank_que_box-nv" id="num-btn-{{ ++$i }}" data-id="{{ $que->que_id }}">Q{{ $i }}</div>
							@endforeach
						</div>
					</div>
					<strong>Legend:</strong>
					<div class="row mt-3">
						<div class="col-sm-6 mb-2">
							<div class="legend">
								<img src="{{ url('imgs/review_answer.png') }}">Answered
							</div>
						</div>
						<div class="col-sm-6 mb-2">
							<div class="legend">
								<img src="{{ url('imgs/not_answered.png') }}">Not Answered
							</div>
						</div>
						<div class="col-sm-6">
							<div class="legend">
								<img src="{{ url('imgs/review.png') }}">Marked
							</div>
						</div>
						<div class="col-sm-6">
							<div class="legend">
								<img src="{{ url('imgs/not_visited.png') }}">Not Visisted
							</div>
						</div>
					</div>
				</div>
			</div>
			<input type="hidden" name="current_question" id="current_question" value="{{ @$curQue }}">
		</form>
	</div>
</div>