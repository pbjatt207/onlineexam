<style>
.questions *, .answers *, .que-ans * {
    font-size: 14px !important;
}
.hindi, .hindi * {
    font-size: 16px !important;
}
</style>
<div id="onlineTest" class="bank d-none">
	<form  id="onlineExam" method="post">
        @csrf
		<input type="hidden" name="test_id" value="{{ $test->test_id }}">
		<input type="hidden" name="total_questions" id="total_questions" value="{{ $total_questions }}">
		<div class="test_heading">
			<img src="{{ url('imgs/sscbanner.jpg') }}">
		</div>
		<div class="test_line"></div>
		<div class="test_time_control">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="inner_timer">
							<strong>{{ $test->test_name }}</strong>
						</div>
					</div>
					<div class="col-sm-6">
                        <div class="subject-list">
                          <ul>
                            @php $sn = 0; @endphp
                            @foreach($sections as $sec)
                                @php $sn++; @endphp
                                <li class="nav-item @if($sn == 1) active @endif" data-section="{{ $sec->subject_slug }}">
                                    {{ $sec->subject_name }}
                                </li>
                            @endforeach
                          </ul>
                        </div>
                      </div>

					<div class="col-sm-4 text-right">
						<div class="inner_timer">
							<strong>Time Left:</strong>
							<!-- <div> -->
								<strong id="demo" class="timer_box">00:59:54 hrs</strong>
								<input type="hidden" name="totalRemains" id="totalRemains">
							<!-- </div> -->
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-sm-8 mt-2 mb-3">
					<div class="que_nav_box">
						<div class="fit">
							<div class="width">
								@php $i = 0; @endphp
								@foreach($questions as $que)
  								@php
  								    $class = "";
  								@endphp

  								@if(!empty($arr['option'][$que->que_id]))
    								@php
    								$class = $arr['is_marked'][$que->que_id] ? "btn-info check" : "btn-success";
    								@endphp
  								@elseif(!empty($arr['is_marked'][$que->que_id]))
					          @php
					            $class = "btn-info";
			              @endphp
			            @endif
								<div class="que_box {{ $class }} p" id="num-btn-{{ ++$i }}" data-id="{{ $que->que_id }}">Q{{ $i }}</div>
								@endforeach
							</div>
						</div>
					</div>
					<div class="que_heading mt-2">
						<span>Online Test</span>
                        <form class="" action="" method="post">
                          @csrf
                          <select class="" id="test" name="language">
                            <option value="english">English</option>
                            <option value="hindi">Hindi</option>
                          </select>
                        </form>
						<!-- <span>Text Size</span>
						<div class="circle">
							<div>A<sup>-</sup></div>
							<div>A<sup>+</sup></div>
						</div> -->
					</div>
					<div class="que_body" id="english">

						<div class="mark_info">
							<strong>
								(Mark:{{ $test->test_positive_marks }}) (Negative Mark/s-{{ $test->test_positive_marks }})
							</strong>
						</div><br>

                        @php $i = 0; @endphp
    					@php
                              $sn = 0;
                        @endphp

                        @foreach($sections as $sec)
                        @php
                            $i++;
                        @endphp
                        <div class="{{ $sec->subject_slug }} section-questions @if($i == 1) active @endif">

    						@foreach($questionsArr[$sec->subject_id] as $que)
    							@php
                                    $sn++;
    								$direction = DB::table('topics')
    										   ->where('topic_id', $que->que_topic)
    										   ->first();

    							    $para = "";
    							    if(!empty($que->que_paragraph)) {
    							        $parag = DB::table('paragraphs')->where('para_id', $que->que_paragraph)->first();
    							        $para = $parag->para_description;
    							    }
    							@endphp
    							<div class="que-ans @if($sn == $curQue) active @else d-none @endif" id="question-block-{{ $que->que_id }}" style="width: 100%; max-height: 60vh; overflow: auto; overflow-x: hidden;">
    							    <div>
        								<div class="c_details">
        									<strong>
        										@if(!empty($direction->topic_direction))Section: {{ $direction->topic_direction }} @endif
        									</strong>
        								</div>
        								@if(!empty($para))
        								    <p>{!! $para !!}</p>
        								@endif
        								<input type="hidden" name="is_marked[{{ $que->que_id }}]" id="is_marked_{{ $sn }}" value="{{ intval(@$arr['is_marked'][$que->que_id]) }}">
    								</div>
    								<div class="question mt-4 mb-4">
    									<div class="float-left mr-3">Q{{ $sn }}.</div>
    									<div class="questions english">{!! $que->que_name !!}</div>
       									<div class="questions hindi">{!! $que->que_name_hi !!}</div>
    								</div>
    								<div class="row">
        								@for($i = 1; $i <= 4; $i++)
        								@php
        									if($i == 1){
        										$val = 'A';
        									} elseif($i == 2){
        										$val = 'B';
        									} elseif($i == 3){
        										$val = 'C';
        									} else{
        										$val = 'D';
        									}

        								@endphp

        								<label class="answers d-block col-md-6">
        									<div class="float-left mr-2">
        										<input type="radio" name="option[{{ $que->que_id }}]" value="{{ $val }}" class="form-control test_radio d-inline" @if(@$arr['option'][$que->que_id] == $val) checked @endif>
        									    {{ $i }})
                                            </div>
                                            <span class="english">{!! $que->{"que_opt".$i} !!}</span>
                                            <span class="hindi">{!! $que->{"que_opt".$i."_hi"} !!}</span>
        								</label>
        								@endfor
    								</div>
    							</div>
    							@endforeach
                            </div>
                            @endforeach
					</div>
				</div>
				<div class="col-sm-4 mt-3 mb-3">
					<div class="profile max">
						<div class="row">
							<div class="col-sm-6">
								<div>
									<div>
										<img class="pro_image" src="{{ url('imgs/candidate/'.$user_info->candidate_image) }}">
									</div>
									<div class="mt-3">
										<img class="pro_image" src="{{ url('imgs/candidate/'.$user_info->candidate_signature) }}">
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="candi-details mt-2">
									<strong>Roll no.</strong>
									<span>{{ $user_info->candidate_roll_number }}</span>
								</div>

								<div class="candi-details mt-3">
									<strong>Candidate Name</strong>
									<span>{{ $user_info->candidate_name }}</span>
								</div>
							</div>
						</div>
					</div>

					<div class="que-pallete mt-2 max">
						<strong class="qp_heading">{{ $sec->subject_name }}</strong>
						<div class="que_status clearfix text-center mb-2">
                          <div class="tab-content" id="pills-tabContent">
                            @php $sn=0; @endphp
                            @php $i = 0; @endphp
                            @foreach($sections as $sec)
                              @php $sn++; @endphp
                              <div class="{{ $sec->subject_slug }} section-questions @if($sn == 1) active @endif">
                              @foreach($questionsArr[$sec->subject_id] as $question)
                              @php $i++ @endphp
                                @php $class = ""; @endphp
                                @if(!empty($arr['option'][$question->que_id]))
                                  @php
                                    $class = $arr['is_marked'][$question->que_id] ? "btn-info check" : "btn-success";
                                  @endphp
                                @elseif(!empty($arr['is_marked'][$question->que_id]))
                                  @php
                                    $class = "btn-info";
                                  @endphp
                                @endif
                                <div class="que_box {{ $class }} mt-2" id="num-btn-{{ $i }}" data-id="{{ $question->que_id }}">Q{{ $i }}</div>
                              @endforeach
                            </div>
                            @endforeach
                          </div>
						</div>
						<div class="qp_footer max">
							<div class="row text-center">
								<div class="col-sm-3">
									<div class="attempt">0</div>
									<span class="qp-lable">Attempted</span>
								</div>
								<div class="col-sm-3">
									<div class="tagged">0</div>
									<span class="qp-lable">Tagged</span>
								</div>
								<div class="col-sm-3">
									<div class="aandt">0</div>
									<span class="qp-lable">Attempted & Tagged</span>
								</div>
								<div class="col-sm-3">
									<div class="unattempt">0</div>
									<span class="qp-lable">unattempted</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="test_footer">
			<div class="footer_test">
				<div class="container">
					<div class="row">
						<div class=" col-4 col-sm-4 col-lg-5">
							<button type="button" id="review-prev" class="btn-left">
								<div class="footer_arrow_image">
									<img src="{{ url('imgs/left-arrow.png') }}">
								</div>
								<span class="disnon">Previous Question</span>
							</button>

							<button type="button" id="next-save" class="btn-right">
								<span class="disnon">Next Question</span>
								<div class="footer_arrow_image">
									<img src="{{ url('imgs/right-arrow.png') }}">
								</div>
							</button>
						</div>
						<div class="col-2 col-sm-2">
							<div id="review-next" class="f_tagged">Tag</div>
						</div>
						<div class="col-3 col-sm-2">
							<div id="clear" class="f_erase">Erase</div>
						</div>

						<div class=" col-3 col-sm-4 col-lg-3">
							<button class="btn-footer_right">
								<div class="btn-foote-right-icon">
									<i class="icon-check"></i>
									<a class="alt float-left ml-2" href="{{ url('ea-xpanel/feedback/'.$test->test_id) }}" ><i class="icon-check1"></i></a>
								</div>
								<a class="max float-left mt-2 ml-2" href="{{ url('ea-xpanel/feedback/'.$test->test_id) }}" >Preview Submit </a>
								<a class="min" href="{{ url('ea-xpanel/feedback/'.$test->test_id) }}" >Submit </a>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Hidden Fields -->
		<input type="hidden" name="current_question" id="current_question" value="{{ @$curQue }}">
	</form>
</div>
