@php
	$site = DB::table('settings')->first();
@endphp

@if(session()->has('user_auth'))

	@php
		$profile = DB::table('users as u')
					->join('roles as r', 'u.user_role', '=', 'r.role_id')
					->where('user_id', session('user_auth'))->first();
	@endphp

	@include('backend.common.header')
	@if($profile->role_name == 'Admin')
		@include('backend.inc.'.$page)
	@else
		@include('backend.user.'.$page)
	@endif

	@include('backend.common.footer')

@else

	@include('backend.login')

@endif