<div class="page-content">
	<div class="container-fluid">
		<h1>
			<a href="{{ url('ea-xpanel/blog/add') }}" class="btn btn-success btn-sm float-right"><i class="icon-plus"></i> Add More</a>
			View Blog
		</h1>
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<form method="post">
						@csrf
						<h3 class="card-title">
							@if(!$records->isEmpty())
							<a href="#delete_all" class="text-white float-right" title="Remove"><i class="icon-trash-o"></i></a>
							@endif
							View Blog
						</h3>
						<div class="card-body">
							@if(!$records->isEmpty())
							<div class="table-responsive">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th width="100">
												<label class="animated-check">
													<input type="checkbox" class="check_all">
													<span class="label-text">All</span>
												</label>
											</th>
											<th>SN.</th>
											<th>Title</th>
											<th>Image</th>
											<th>Description</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										@php
											$sn = $offset;
										@endphp

										@foreach($records as $rec)
										<tr>
											<td>
												<label class="animated-check">
													<input type="checkbox" name="check[]" value="{{ $rec->blog_id }}" class="check">
													<span class="label-text"></span>
												</label>
											</td>
											<td>{{ ++$sn }}</td>
											<td>{!! $rec->blog_title !!}</td>
											<td><img src="{{ !empty($rec->blog_image) ? url('imgs/blogs/'.$rec->blog_image) : url('imgs/no-image.png') }}" style="max-width: 100px;" /></td>
											<td class="text-justify">{!! substr(strip_tags($rec->blog_description), 0, 400).' &hellip; ' !!}</td>
											<td>
												<a href="{{ url('ea-xpanel/blog/add/'.$rec->blog_id) }}"><i class="icon-pencil"></i></a>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							{{ $records->links() }}
							@else
							<div class="no_records_found">
								No record(s) found.
							</div>
							@endif
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>