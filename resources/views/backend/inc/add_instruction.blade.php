<div class="page-content">
	<div class="container-fluid">
		<h1>Add Instruction</h1>
		<form method="post" enctype="multipart/form-data">
			@csrf
			<div class="row">
				<div class="col-sm-8">
					<div class="card">
						<h3 class="card-title">
							{{ empty($edit->que_id) ? "Add" : "Edit" }} Instruction
						</h3>
						<div class="card-body">
							@if (\Session::has('success'))
							    <div class="alert alert-success">
								    {!! \Session::get('success') !!}</li>
								</div>
							@endif
								
							<div class="form-group">
								<label>Instruction Name</label>
								<input type="text" name="record[inst_name]" class="form-control" placeholder="Instruction Name" value="{{ @$edit->inst_name }}" required>
							</div>

							<div class="form-group">
								<label>Instruction Description</label>
								<textarea rows="10" name="record[inst_description]" class="form-control editor">{{ @$edit->inst_description }}</textarea>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-4">
					<h3 class="card-title">
						<a href="#save" class="text-white float-right"><i class="icon-save"></i> Save</a>
						Important Instruction
					</h3>
					<div class="inst-content">
						<ul>
							<li>
								<div class="row">
									<div class="col-md-1">
										<span class="check-icon icon-check_box"></span>
									</div>
									<div class="col-md-11">
										<span class="text">The user can add Candidate to the panel</span>
									</div>
								</div>
							</li>
							<li>
								<div class="row">
									<div class="col-md-1">
										<span class="check-icon icon-check_box"></span>
									</div>
									<div class="col-md-11">
										<span class="text">This will include few entities for initial registration of a Candidate.</span>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>