<div class="page-content">
	<div class="container-fluid">
		<h1>
			<a href="{{ url('ea-xpanel/candidate') }}" class="btn btn-success btn-sm float-right">View</a>
			Candidates
		</h1>
		<div class="row">
			<div class="col-sm-8">
				<div class="card">
					<form method="post" enctype="multipart/form-data">
						@csrf
						<h3 class="card-title">Add Candidate</h3>
						<div class="card-body">
							@if (\Session::has('success'))
							    <div class="alert alert-success">
								    {!! \Session::get('success') !!}</li>
								</div>
							@endif
								<h6>Login Detail</h6>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label>Mobile No. *</label>
											<input type="text" name="record[candidate_mobile_no]" class="form-control" maxlength="10" placeholder="Mobile Number" value="{{ @$edit->candidate_mobile_no }}" required>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>Password</label>
											<input type="password" name="record[candidate_password]" class="form-control" placeholder="Password" value="" @if(empty($edit->user_password)) required @endif autocomplete="new-password">
										</div>
									</div>
									
								</div>
								<h6 class="mt-3">Login Detail</h6>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label>Courses</label>
											@php
											$cids = !empty($edit->user_courses) ? explode(",", $edit->user_courses) : array();
											@endphp
											<select name="record[user_courses][]" multiple="multiple" class="3col active">
												@foreach($courses as $course)
										        <option value="{{ $course->course_id }}" @if(in_array($course->course_id, $cids)) selected @endif>{{ $course->course_name }}</option>
										        @endforeach
										    </select>
										</div>
									</div>
								</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label>F-Name *</label>
										<input type="text" name="record[candidate_f_name]" class="form-control" placeholder="F-Name" value="{{ @$edit->candidate_f_name }}" required>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>L-Name *</label>
										<input type="text" name="record[candidate_l_name]" class="form-control" placeholder="L-Name" value="{{ @$edit->candidate_l_name }}" required>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Gender (Required)</label>
										<div class="gender form-control clearfix">
											<span class="radio">
												<input type="radio" name="record[candidate_gender]" id="male" value="M" checked>
												<label for="male">Male</label>
												<div class="check"></div>
											</span>
											<span class="radio">
												<input type="radio" name="record[candidate_gender]" id="female" value="F">
												<label for="female">Female</label>
												<div class="check"></div>
											</span>
										</div>
									</div>
								</div>

								<div class="col-sm-6">
									<div class="form-group">
										<label>Email *</label>
										<input type="text" name="record[candidate_email]" class="form-control" placeholder="Email" value="{{ @$edit->candidate_email }}" required autocomplete="off">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label>DATE OF BIRTH</label>
										<input type="text" name="record[candidate_dob]" class="datepicker_no_future form-control" value="{{ @$edit->candidate_dob }}" readonly="" required>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>DATE OF REGISTRATION (REQUIRED)</label>
										<input type="text" name="record[candidate_created_on]" class=" datepicker form-control" value="{{ @$edit->candidate_created_on }}" readonly="" required>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label>ADDRESS (REQUIRED)</label>
										<input type="text" name="record[candidate_address]" class="form-control" placeholder="Address" value="{{ @$edit->candidate_address }}" required>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<label>STATE (REQUIRED)</label>
										<input type="text" name="record[candidate_state]" class="form-control" placeholder="State" value="{{ @$edit->candidate_state }}" required>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label>CITY (REQUIRED)</label>
										<input type="text" name="record[candidate_city]" class="form-control" placeholder="City" value="{{ @$edit->candidate_city }}" required>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label>POST CODE (REQUIRED)</label>
										<input type="text" name="record[candidate_post_code]" class="form-control" placeholder="Post Code" value="{{ @$edit->candidate_post_code }}" required>
									</div>
								</div>
							</div>
						</div>
				</div>
			</div>
			<div class="col-4">
				<div class="card">
					<h3 style="padding:0;" class="card-title"><button style=" color:#fff; font-weight: bold;" class="btn btn-block text-right"><i class="icon-save"></i> &nbsp;{{ empty($edit->candidate_id) ? "Save" : "Update" }}</button> </h3>
					<div class="candidate">
						<div style="display:flex;"><h5>Upload Image</h5>&nbsp;( in 100kb )</div>
							<div class="divider"></div>
						<div class="file-upload">

						  <div class="col image-upload-wrap">
				           <label class="file-upload form-group" style="padding: 0px; border: 1px solid #ccc;">
				            <img class="file-upload-image"  src="{{ !empty($edit->candidate_image) ? url('imgs/candidate/'.$edit->candidate_image) : url('imgs/no-image.png') }}">
				            <input type="file" class="file-upload-input" name="candidate_image" accept="image/*" id="candidateImage" >
				            </label>
			            </div>
						  <label for="candidateImage" class="file-upload-btn btn btn-info btn-block">Select Image</label>
						</div>
					</div>

					<div class="candidate">
						<div style="display:flex;"><h5>Signature Image</h5>&nbsp;( in 50kb )</div>
							<div class="divider"></div>
						<div class="file-upload">

						  <div class="col image-upload-wrap">
				           <label class="file-upload form-group" style="padding: 0px; border: 1px solid #ccc;">
				            <img style="max-height: 50px; width: 100%;" class="file-upload-image"  src="{{ !empty($edit->candidate_signature) ? url('imgs/candidate/'.$edit->candidate_signature) : url('imgs/no-image.png') }}">
				            <input type="file" class="file-upload-input" name="candidate_signature" accept="image/*" id="signatUre" >
				            </label>
			            </div>
						  <label for="signatUre" class="file-upload-btn btn btn-info btn-block">Select Signature</label>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>