<div class="page-content">
	<div class="container-fluid">
		<h1>Copy Subject To Courses</h1>
		<div class="row">
			<div class="col-sm-4">
				<div class="card">
					<form method="post">
						@csrf
						<h3 class="card-title">
							Subject
						</h3>
						<div class="card-body">
							{{ $subject->subject_name }}
						</div>
					</form>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="card">
					<form method="post">
						@csrf
						<h3 class="card-title">
							@if(!$courses->isEmpty())
							<a href="#copy_all" class="text-white float-right" title="Copy" data-toggle="tooltip"><i class="icon-copy"></i></a>
							@endif
							Copy To Courses
						</h3>
						<div class="card-body">
							@if(!$courses->isEmpty())
							<div class="table-responsive">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th width="100">
												<label class="animated-check">
													<input type="checkbox" class="check_all">
													<span class="label-text">All</span>
												</label>
											</th>
											<th>SN.</th>
											<th>Name</th>
										</tr>
									</thead>
									<tbody>
										@php
											$sn = $offset;
										@endphp

										@foreach($courses as $rec)
										<tr>
											<td>
												<label class="animated-check">
													<input type="checkbox" name="check[]" value="{{ $rec->course_id }}" class="check">
													<span class="label-text"></span>
												</label>
											</td>
											<td>{{ ++$sn }}</td>
											<td>{{ $rec->course_name }}</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							{{ $courses->links() }}
							@else
							<div class="no_records_found">
								No record(s) found.
							</div>
							@endif
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>