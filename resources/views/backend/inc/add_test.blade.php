<div class="page-content">
	<div class="container-fluid">
		<h1>Add Test</h1>
		<div class="row">
			<div class="col-sm-8">
				<div class="card">
					<form method="post" enctype="multipart/form-data">
						@csrf
						<h3 class="card-title">
							<a href="#save" class="text-white float-right"> Next <i class="icon-arrow_forward"></i></a>
							{{ empty($edit->que_id) ? "Add" : "Edit" }} Test
						</h3>
						<div class="card-body">
							@if (\Session::has('success'))
							    <div class="alert alert-success">
								    {!! \Session::get('success') !!}</li>
								</div>
							@endif
								
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<label>Test Name</label>
										<input type="text" name="record[test_name]" class="form-control" placeholder="Test Name" value="{{ @$edit->test_name }}" required>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label>Duration ( in minutes )</label>
										<input type="text" name="record[test_duration]" class="form-control" placeholder="Duration" value="{{ @$edit->test_duration }}" required>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label>No. of Questions</label>
										<input type="text" name="record[test_no_of_question]" class="form-control" placeholder="No. of Questions" value="{{ @$edit->test_no_of_question }}" required>
									</div>
								</div>
							</div>
							<div class="row">
									<div class="col-sm-4">
										<div class="form-group">
											<label>Course</label>
											<select name="record[test_course]" class="form-control course" data-target="#que_subject">
												<option value="">Select Course</option>
												@foreach($courses as $crs)
												<option value="{{ $crs->course_id }}" @if(@$edit->test_course == $crs->course_id) {{"selected"}} @endif >{{ $crs->course_name }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
											<label>Instruction</label>
											<select name="record[test_instruction]" class="form-control course">
												<option value="">Select Course</option>
												@foreach($instruction as $ins)
												<option value="{{ $ins->inst_id }}" @if(@$edit->test_instruction == $ins->inst_id) {{"selected"}} @endif >{{ $ins->inst_name }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
											<label>Difficulty Level</label>
											<select name="record[test_defficulty_level]" class="form-control" required>
												<option value="">Select Level</option>
												<option value="Beginner" @if(@$edit->test_defficulty_level == "Beginner") {{"selected"}} @endif>Beginner</option>
												<option value="Intermediate" @if(@$edit->test_defficulty_level == "Intermediate") {{"selected"}} @endif>Intermediate</option>
												<option value="Expert" @if(@$edit->test_defficulty_level == "Expert") {{"selected"}} @endif>Expert</option>
											</select>
										</div>
									</div>
								</div>
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<label>Total Marks</label>
										<input type="text" name="record[test_total_marks]" class="form-control" placeholder="Total Marks" value="{{ @$edit->test_total_marks }}" required>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label>Positive Marks</label>
										<input type="text" name="record[test_positive_marks]" class="form-control" placeholder="Positive Marks" value="{{ @$edit->test_positive_marks }}" required>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label>Negative Marks</label>
										<input type="text" name="record[test_negative_marks]" class="form-control" placeholder="Negative Marks" value="{{ @$edit->test_negative_marks }}" required>
									</div>
								</div>
							</div>
							<!-- <div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<label>Template</label>
										<select name="record[test_template]" class="form-control">
											<option>Choose Template</option>
											<option value="1">Template 1</option>
											<option value="2">Template 2</option>
										</select>
									</div>
								</div>
							</div> -->
						</div>
					</form>
				</div>
			</div>
			<div class="col-sm-4">
					<h3 class="card-title">
						Important Instruction
					</h3>
					<div class="inst-content">
						<ul>
							<li>
								<div class="row">
									<div class="col-md-1">
										<span class="check-icon icon-check_box"></span>
									</div>
									<div class="col-md-11">
										<span class="text">The user can add Candidate to the panel</span>
									</div>
								</div>
							</li>
							<li>
								<div class="row">
									<div class="col-md-1">
										<span class="check-icon icon-check_box"></span>
									</div>
									<div class="col-md-11">
										<span class="text">This will include few entities for initial registration of a Candidate.</span>
									</div>
								</div>
							</li>
						</ul>
					</div><!-- 
					<div class="row text-center mt-4">
						<div class="col">
							<div>
								<img class="img-fluid" src="{{ url('imgs/ssc_temp.jpg') }}">
							</div>
							<div>
								Template 1
							</div>
						</div>
						<div class="col">
							<div>
								<img class="img-fluid" src="{{ url('imgs/bank_temp.jpg') }}">
							</div>
							<div>
								Template 2
							</div>
						</div>
					</div> -->
				</div>
		</div>
	</div>
</div>