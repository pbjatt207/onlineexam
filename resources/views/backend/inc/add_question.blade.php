<div class="page-content">
	<div class="container-fluid">
		<h1>{{ empty($edit->que_id) ? "Add" : "Edit" }} Question</h1>
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<form method="post">
						@csrf
						<h3 class="card-title">
							<a href="#save" class="text-white float-right"><i class="icon-save"></i> Save</a>
							{{ empty($edit->que_id) ? "Add" : "Edit" }} Question
						</h3>
						<div class="card-body">
							@if (\Session::has('success'))
							    <div class="alert alert-success">
								    {!! \Session::get('success') !!}</li>
								</div>
							@endif
							<div class="row">
								<div class="col-sm-3">
									<div class="form-group">
										<label>Course</label>
										<select name="record[que_course]" class="form-control course" data-target="#que_subject">
											<option value="">Select Course</option>
											@foreach($courses as $crs)
											<option value="{{ $crs->course_id }}" @if($crs->course_id == @$edit->que_course) {{"selected"}} @endif>{{ $crs->course_name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<label>Subject</label>
										<select name="record[que_subject]" id="que_subject" data-target="#que_topic" class="form-control subject">
											<option value="">Select Subject</option>
											@foreach($subjects as $subj)
											<option value="{{ $subj->subject_id }}" @if($subj->subject_id == @$edit->que_subject) {{"selected"}} @endif>{{ $subj->subject_name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<label>Topic</label>
										<select name="record[que_topic]" id="que_topic" data-target="#que_para" class="form-control topic">
											<option value="">Select Topic</option>
											@foreach($topics as $top)
											<option value="{{ $top->topic_id }}" @if($top->topic_id == @$edit->que_topic) {{"selected"}} @endif>{{ $top->topic_name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<label>Question Type *</label>
										<select name="record[que_type]" class="form-control qtype" required>
											<option value="MCQ" @if(@$edit->que_type == "MCQ") selected @endif>MCQ</option>
											<option value="Paragraph" @if(@$edit->que_type == "Paragraph") selected @endif>Essay / Paragraph</option>
										</select>
									</div>
								</div>
							</div>

							<div class="form-group para d-none">
								<label>
									Essay / Paragraph
								</label>
								<select name="record[que_paragraph]" class="form-control" id="que_para">
									<option value="">Select Essay / Paragraph</option>
								</select>
							</div>
							<hr>
							<div class="row">
								<div class="col-sm-6">
									<h3>English</h3>
								</div>
								<div class="col-sm-6">
									<h3>Hindi</h3>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label>Question</label>
										<textarea rows="10" name="record[que_name]" class="form-control editor">{{ @$edit->que_name }}</textarea>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>Question</label>
										<textarea rows="10" name="record[que_name_hi]" class="form-control editor">{{ @$edit->que_name_hi }}</textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label>Explaination</label>
										<textarea rows="5" name="record[que_explaination]" class="form-control editor">{{ @$edit->que_explaination }}</textarea>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>Explaination</label>
										<textarea rows="5" name="record[que_explaination_hi]" class="form-control editor">{{ @$edit->que_explaination_hi }}</textarea>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label class="animated-radio float-right">
											<input type="radio" name="record[que_answer]" value="A"@php if(@$edit->que_answer == 'A') echo ' checked'; @endphp>
											<span class="label-text">Correct</span>
										</label>
										<label> Option 1 </label>
										<textarea rows="4" name="record[que_opt1]" class="form-control editor">{{ @$edit->que_opt1 }}</textarea>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="animated-radio float-right">
											<input type="radio" name="record[que_answer_hi]" value="A"@php if(@$edit->que_answer_hi == 'A') echo ' checked'; @endphp>
											<span class="label-text">Correct</span>
										</label>
										<label> Option 1 </label>
										<textarea rows="4" name="record[que_opt1_hi]" class="form-control editor">{{ @$edit->que_opt1_hi }}</textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label class="animated-radio float-right">
											<input type="radio" name="record[que_answer]" value="B"@php if(@$edit->que_answer == 'B') echo ' checked'; @endphp>
											<span class="label-text">Correct</span>
										</label>
										<label> Option 2 </label>
										<textarea rows="4" name="record[que_opt2]" class="form-control editor">{{ @$edit->que_opt2 }}</textarea>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="animated-radio float-right">
											<input type="radio" name="record[que_answer_hi]" value="B"@php if(@$edit->que_answer_hi == 'B') echo ' checked'; @endphp>
											<span class="label-text">Correct</span>
										</label>
										<label> Option 2 </label>
										<textarea rows="4" name="record[que_opt2_hi]" class="form-control editor">{{ @$edit->que_opt2_hi }}</textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label class="animated-radio float-right">
											<input type="radio" name="record[que_answer]" value="C"@php if(@$edit->que_answer == 'C') echo ' checked'; @endphp>
											<span class="label-text">Correct</span>
										</label>
										<label> Option 3 </label>
										<textarea rows="4" name="record[que_opt3]" class="form-control editor">{{ @$edit->que_opt3 }}</textarea>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="animated-radio float-right">
											<input type="radio" name="record[que_answer_hi]" value="C"@php if(@$edit->que_answer_hi == 'C') echo ' checked'; @endphp>
											<span class="label-text">Correct</span>
										</label>
										<label> Option 3 </label>
										<textarea rows="4" name="record[que_opt3_hi]" class="form-control editor">{{ @$edit->que_opt3_hi }}</textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label class="animated-radio float-right">
											<input type="radio" name="record[que_answer]" value="D"@php if(@$edit->que_answer == 'D') echo ' checked'; @endphp>
											<span class="label-text">Correct</span>
										</label>
										<label> Option 4 </label>
										<textarea rows="4" name="record[que_opt4]" class="form-control editor">{{ @$edit->que_opt4 }}</textarea>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="animated-radio float-right">
											<input type="radio" name="record[que_answer_hi]" value="D"@php if(@$edit->que_answer_hi == 'D') echo ' checked'; @endphp>
											<span class="label-text">Correct</span>
										</label>
										<label> Option 4 </label>
										<textarea rows="4" name="record[que_opt4_hi]" class="form-control editor">{{ @$edit->que_opt4_hi }}</textarea>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@if(@$edit->que_type == "Paragraph")
<script>
$(function() {
    $(window).on('load', function() {
        $('.qtype').trigger('change');
    });
});
</script>
@endif
