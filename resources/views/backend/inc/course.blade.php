<div class="page-content">
	<div class="container-fluid">
		<h1>Courses</h1>
		<div class="row">
			<div class="col-sm-4">
				<div class="card">
					<form method="post">
						@csrf
						<h3 class="card-title">{{ empty($edit->course_id) ? "Add" : "Edit" }} Course</h3>
						<div class="card-body">
							@if (\Session::has('success'))
							    <div class="alert alert-success">
								    {!! \Session::get('success') !!}</li>
								</div>
							@endif
							<div class="form-group">
								<label>Course Name *</label>
								<input type="text" name="record[course_name]" class="form-control" placeholder="Course Name" value="{{ @$edit->course_name }}" required>
							</div>
							<div>
								<button class="btn btn-block btn-success">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="card">
					<form method="post">
						@csrf
						<h3 class="card-title">
							@if(!$records->isEmpty())
							<a href="#delete_all" class="text-white float-right" title="Remove"><i class="icon-trash-o"></i></a>
							@endif
							View Course
						</h3>
						<div class="card-body">
							@if(!$records->isEmpty())
							<div class="table-responsive">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th width="100">
												<label class="animated-check">
													<input type="checkbox" class="check_all">
													<span class="label-text">All</span>
												</label>
											</th>
											<th>SN.</th>
											<th>Name</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										@php
											$sn = $offset;
										@endphp

										@foreach($records as $rec)
										@php
											$exist = DB::table('questions')->where('que_course', $rec->course_id)->first();
										@endphp
										<tr>
											<td>
												<label class="animated-check @if(!empty($exist)) disabled @endif">
													<input type="checkbox" name="check[]" value="{{ $rec->course_id }}" class="check" @if(!empty($exist)) disabled @endif>
													<span class="label-text"></span>
												</label>
											</td>
											<td>{{ ++$sn }}</td>
											<td>{{ $rec->course_name }}</td>
											<td>
												<a href="{{ url('ea-xpanel/courses/'.$rec->course_id) }}"><i class="icon-pencil"></i></a>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							{{ $records->links() }}
							@else
							<div class="no_records_found">
								No record(s) found.
							</div>
							@endif
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>