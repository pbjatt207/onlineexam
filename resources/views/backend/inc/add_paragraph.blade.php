<div class="page-content">
	<div class="container-fluid">
		<h1>{{ empty($edit->para_id) ? "Add" : "Edit" }} Paragraph</h1>
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<form method="post">
						@csrf
						<h3 class="card-title">
							<a href="#save" class="text-white float-right"><i class="icon-save"></i> Save</a>
							{{ empty($edit->para_id) ? "Add" : "Edit" }} Paragraph
						</h3>
						<div class="card-body">
							@if (\Session::has('success'))
							    <div class="alert alert-success">
								    {!! \Session::get('success') !!}</li>
								</div>
							@endif
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<label>Course</label>
										<select name="record[para_course]" class="form-control course" data-target="#para_subject">
											<option value="">Select Course</option>
											@foreach($courses as $crs)
											<option value="{{ $crs->course_id }}" @if($crs->course_id == @$edit->para_course) {{"selected"}} @endif>{{ $crs->course_name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label>Subject</label>
										<select name="record[para_subject]" id="para_subject" data-target="#para_topic" class="form-control subject">
											<option value="">Select Subject</option>
											@foreach($subjects as $subj)
											<option value="{{ $subj->subject_id }}" @if($subj->subject_id == @$edit->para_subject) {{"selected"}} @endif>{{ $subj->subject_name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label>Topic</label>
										<select name="record[para_topic]" id="para_topic" class="form-control">
											<option value="">Select Topic</option>
											@foreach($topics as $top)
											<option value="{{ $top->topic_id }}" @if($top->topic_id == @$edit->para_subject) {{"selected"}} @endif>{{ $top->topic_name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label>Language *</label>
										<select name="record[para_language]" class="form-control" required>
											<option value="">Select Language</option>
											<option value="HI" @if(@$edit->para_language == "HI") {{"selected"}} @endif>Hindi</option>
											<option value="EN" @if(@$edit->para_language == "EN") {{"selected"}} @endif>English</option>
										</select>
									</div>
								</div>
								<div class="col-sm-8">
									<div class="form-group">
										<label>Paragraph Name *</label>
										<input type="text" name="record[para_name]" value="{{ @$edit->para_name }}" class="form-control" required>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label>Paragraph</label>
								<textarea rows="10" name="record[para_description]" class="form-control editor">{{ @$edit->para_description }}</textarea>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>