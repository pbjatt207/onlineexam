<div class="page-content">
	<div class="container-fluid">
		<h1>{{ empty($edit->reading_id) ? "Add" : "Edit" }} Reading</h1>
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<form method="post" enctype="multipart/form-data">
						@csrf
						<h3 class="card-title">
							<a href="#save" class="text-white float-right"><i class="icon-save"></i> Save</a>
							{{ empty($edit->reading_id) ? "Add" : "Edit" }} Reading
						</h3>
						<div class="card-body">
							@if (\Session::has('success'))
							    <div class="alert alert-success">
								    {!! \Session::get('success') !!}</li>
								</div>
							@endif
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label>Title</label>
										<input type="text" name="record[reading_title]" class="form-control" placeholder="Title" value="{{ @$edit->reading_title }}" required>
									</div>
									<div class="form-group">
										<label>Description</label>
										<textarea rows="15" name="record[reading_description]" class="form-control editor">{{ @$edit->reading_description }}</textarea>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>