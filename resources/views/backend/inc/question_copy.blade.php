<div class="page-content">
	<div class="container-fluid">
		<h1>Copy Question To Topic</h1>
		@if (\Session::has('success'))
		    <div class="alert alert-success">
			    {!! \Session::get('success') !!}</li>
			</div>
		@endif
		<div class="row mb-2">
			<div class="col-sm-12">
				<div class="card">
					<form>
						@csrf
						<div class="row">
							<div class="col-sm-4">
								<div class="form-group mt-2 mb-2 ml-2">
									<select name="search[course]" class="form-control course" data-target="#searchSubject">
										<option value="">Select Course</option>
										@foreach($courses as $course)
										<option value="{{ $course->course_id }}" {{ (@$search['que_course'] == $course->course_id) ? ' selected' : '' }}>{{ $course->course_name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group mt-2 mb-2">
									<select name="search[subject]" class="form-control subject" id="searchSubject" data-target="#searchTopic">
										<option value="">Select Subject</option>
										@foreach($subject as $subject)
										<option value="{{ $subject->subject_id }}" {{ (@$search['que_subject'] == $subject->subject_id) ? ' selected' : '' }}>{{ $subject->subject_name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-sm-2">
								<div class="form-group mt-2 mb-2">
									<input type="submit" value="Search" class=" form-control btn btn-success">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="card">
					<form method="post">
						@csrf
						<h3 class="card-title">
							Question
						</h3>
						<div class="card-body">
							{!! (strip_tags($question->que_name)) !!}
						</div>
					</form>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="card">
					<form method="post">
						@csrf
						<h3 class="card-title">
							
							<a href="#copy_all" class="text-white float-right" title="Copy" data-toggle="tooltip"><i class="icon-copy"></i></a>
							
							Copy To Topics
						</h3>
						<div class="card-body">
							@if(!$topic->isEmpty())
							<div class="table-responsive">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th width="100">
												<label class="animated-check">
													<input type="checkbox" class="check_all">
													<span class="label-text">All</span>
												</label>
											</th>
											<th>SN.</th>
											<th>Name</th>
											<th>Course Name</th>
											<th>Subject Name</th>
											<th>Use Paragraph</th>
										</tr>
									</thead>
									<tbody>
										@php
											$sn = $offset;
										@endphp

										@foreach($topic as $rec)
										<tr>
											<td>
												<label class="animated-check">
													<input type="checkbox" name="check[]" value="{{ $rec->topic_id }}" class="check">
													<span class="label-text"></span>
												</label>
											</td>
											<td>{{ ++$sn }}</td>
											<td>{{ $rec->topic_name }}</td>
											<td>{{ $rec->course_name }}</td>
											<td>{{ $rec->subject_name }}</td>
											<td>{{ $rec->topic_is_para }}</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							{{ $topic->appends($get_parmas)->links() }}
							@else
							<div class="no_records_found">
								No record(s) found.
							</div>
							@endif
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>