<div class="page-content">
	<div class="container-fluid">
		<h1>
			<a href="{{ url('ea-xpanel/test/test_questions/'.$id) }}" class="btn btn-success btn-sm float-right"><i class="icon-plus"></i> Add More</a>
			View Test Question
		</h1>
		<div class="row">
			<div class="col-sm-12">
				<div class="card form-group">
					<h3 class="card-title"><i class="icon-filter"></i> Filter By</h3>
					<div class="card-body">
						<form>
							@csrf
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<label>By Keyword</label>
										<input type="text" name="search[keyword]" placeholder="By Keyword..." class="form-control" value="{{ @$search['keyword'] }}">
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
										<label>By Course</label>
										<select name="search[que_course]" class="form-control course" data-target="#searchSubject">
											<option value="">Select Course</option>
											@foreach($courses as $course)
											<option value="{{ $course->course_id }}" {{ (@$search['que_course'] == $course->course_id) ? ' selected' : '' }}>{{ $course->course_name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
										<label>By Subject</label>
										<select name="search[que_subject]" class="form-control subject" id="searchSubject" data-target="#searchTopic">
											<option value="">Select Subject</option>
											@foreach($subjects as $subject)
											<option value="{{ $subject->subject_id }}" {{ (@$search['que_subject'] == $subject->subject_id) ? ' selected' : '' }}>{{ $subject->subject_name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
										<label>By Topic</label>
										<select name="search[que_topic]" class="form-control" id="searchTopic">
											<option value="">Select Topic</option>
											@foreach($topics as $topic)
											<option value="{{ $topic->topic_id }}" {{ (@$search['que_topic'] == $topic->topic_id) ? ' selected' : '' }}>{{ $topic->topic_name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-sm-2">
									<label>&nbsp;</label>
									<button class="btn btn-block btn-success">Search</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="card">
					<form method="post">
						@csrf
						<h3 class="card-title">
							@if(!$records->isEmpty())
							<a href="#delete_all" class="text-white float-right" title="Remove"><i class="icon-trash-o"></i></a>
							@endif
							View Question
						</h3>
						<div class="card-body">
							@if (\Session::has('success'))
							    <div class="alert alert-success">
								    {!! \Session::get('success') !!}</li>
								</div>
							@endif

							@if(!$records->isEmpty())
							<div class="table-responsive">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th width="100">
												<label class="animated-check">
													<input type="checkbox" class="check_all">
													<span class="label-text">All</span>
												</label>
											</th>
											<th>SN.</th>
											<th width="500">Question</th>
											<th>Options</th>
											<th>Correct Answer</th>
											<th>Course Name</th>
											<th>Subject Name</th>
											<th>Topic Name</th>
										</tr>
									</thead>
									<tbody>
										@php
											$sn = $offset;
										@endphp

										@foreach($records as $rec)
										<tr>
											<td>
												<label class="animated-check">
													<input type="checkbox" name="check[]" value="{{ $rec->que_id }}" class="check">
													<span class="label-text"></span>
												</label>
											</td>
											<td>{{ ++$sn }}</td>
											<td>{!! substr(strip_tags($rec->que_name),0,600) !!}</td>
											<td>
												<ol style="padding: 0; margin: 0; list-style: inside decimal;">
													<li>{!! substr(strip_tags($rec->que_opt1),0,100) !!}</li>
													<li>{!! substr(strip_tags($rec->que_opt2),0,100) !!}</li>
													<li>{!! substr(strip_tags($rec->que_opt3),0,100) !!}</li>
													<li>{!! substr(strip_tags($rec->que_opt4),0,100) !!}</li>
												</ol>
											</td>
											<td>{{ $rec->que_answer }}</td>
											<td>{{ $rec->course_name }}</td>
											<td>{{ $rec->subject_name }}</td>
											<td>{{ $rec->topic_name }}</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							{{ $records->appends($get_parmas)->links() }}
							@else
							<div class="no_records_found">
								No record(s) found.
							</div>
							@endif
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
