<div class="page-content">
	<div class="container-fluid">
		<h1>
			<a href="{{ url('ea-xpanel/candidate/add') }}" class="btn btn-success btn-sm float-right"><i class="icon-plus"></i> Add More</a>
			View Candidates
		</h1>
		<div class="row">
			<div class="col-sm-12">
				<div class="card form-group">
					<h3 class="card-title"><i class="icon-filter"></i> Filter By</h3>
					<div class="card-body">
						<form>
							@csrf
							<div class="row">
								<div class="col-sm-2">
									<div class="form-group">
										<label>By Keyword</label>
										<input type="text" name="search[keyword]" placeholder="By Keyword..." class="form-control" value="{{ @$search['keyword'] }}">
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
										<label>By Roll Number</label>
										<input type="text" name="search[roll]" placeholder="By Roll Number..." class="form-control" value="{{ @$search['keyword'] }}">
									</div>
								</div>
								<div class="col-sm-2">
									<label>&nbsp;</label>
									<button class="btn btn-block btn-success">Search</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="card">
					<form method="post">
						@csrf
						<h3 class="card-title">
							<a href="#delete_all" class="text-white float-right" title="Remove"><i class="icon-trash-o"></i></a>
							View Candidates
						</h3>
						<div class="card-body">
							@if (\Session::has('success'))
							    <div class="alert alert-success">
								    {!! \Session::get('success') !!}</li>
								</div>
							@endif

							@if(!$records->isEmpty())

							<div class="table-responsive">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th width="100">
												<label class="animated-check">
													<input type="checkbox" class="check_all">
													<span class="label-text">All</span>
												</label>
											</th>
											<th>SN.</th>
											<th>Image</th>
											<th>Signature</th>
											<th>Roll No.</th>
											<th>Candidate Name</th>
											<th>Mobile No.</th>
											<th>Email</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										@php
											$sn = 0;
										@endphp
										@foreach($records as $rec)
										<tr>
											<td>
												<label class="animated-check">
													<input type="checkbox" name="check[]" value="{{ $rec->candidate_user }}" class="check">
													<span class="label-text"></span>
												</label>
											</td>
											<td>{{ ++$sn }}</td>
											<td class="text-center">
												<img style="width: 100px;" src="{{ url('imgs/candidate/'.$rec->candidate_image) }}">
											</td>
											<td class="text-center">
												<img style="width: 100px;" src="{{ url('imgs/candidate/'.$rec->candidate_signature) }}">
											</td>
											<td>{{ $rec->candidate_roll_number }}</td>
											<td>{{ $rec->candidate_name }}</td>
											<td>{{ $rec->candidate_mobile_no }}</td>
											<td>{{ $rec->candidate_email }}</td>
											<td>
												<a href="{{ url('ea-xpanel/candidate/add/'.$rec->candidate_id) }}"><i class="icon-pencil"></i></a>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							{{ $records->appends($get_parmas)->links() }}
							@else
							<div class="no_records_found">
								No record(s) found.
							</div>
							@endif
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
