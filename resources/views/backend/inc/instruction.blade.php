<div class="page-content">
	<div class="container-fluid">
		<h1>
			<a href="{{ url('ea-xpanel/instruction/add') }}" class="btn btn-success btn-sm float-right"><i class="icon-plus"></i> Add More</a>
			View Instructions
		</h1>
		<div class="row">
			<div class="col-sm-12">
				<div class="card form-group">
					<h3 class="card-title"><i class="icon-filter"></i> Filter By</h3>
					<div class="card-body">
						<form>
							@csrf
							<div class="row">
								<div class="col-sm-2">
									<div class="form-group">
										<label>By Keyword</label>
										<input type="text" name="search[keyword]" placeholder="By Keyword..." class="form-control" value="{{ @$search['keyword'] }}">
									</div>
								</div>
								<div class="col-sm-2">
									<label>&nbsp;</label>
									<button class="btn btn-block btn-success">Search</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="card">
					<form method="post">
						@csrf
						<h3 class="card-title">
							<a href="#delete_all" class="text-white float-right" title="Remove"><i class="icon-trash-o"></i></a>
							View Instruction
						</h3>
						<div class="card-body">
							@if (\Session::has('success'))
							    <div class="alert alert-success">
								    {!! \Session::get('success') !!}</li>
								</div>
							@endif

							@if(!$records->isEmpty())
							
							<div class="table-responsive">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th width="100">
												<label class="animated-check">
													<input type="checkbox" class="check_all">
													<span class="label-text">All</span>
												</label>
											</th>
											<th>SN.</th>
											<th>Name</th>
											<th>Description</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										@php
											$sn = $records->firstItem();
										@endphp
										@foreach($records as $rec)
										<tr>
											<td>
												<label class="animated-check">
													<input type="checkbox" name="check[]" value="{{ $rec->inst_id }}" class="check">
													<span class="label-text"></span>
												</label>
											</td>
											<td>{{ $sn++ }}</td>
											<td>{{ $rec->inst_name }}</td>
											<td>{!! substr(strip_tags($rec->inst_description),0,100) !!}</td>
											<td>
												<a href="{{ url('ea-xpanel/instruction/add/'.$rec->inst_id) }}"><i class="icon-pencil"></i></a>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							{{ $records->appends($get_parmas)->links() }}
							@else
							<div class="no_records_found">
								No record(s) found.
							</div>
							@endif
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>