<div class="page-content">
	<div class="container-fluid">
		<h1>Categories</h1>
		@if (\Session::has('success'))
		    <div class="alert alert-success">
			    {!! \Session::get('success') !!}</li>
			</div>
		@endif
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<form method="post" enctype="multipart/form-data">
						@csrf
						<h3 class="card-title">
							<a href="#save" class="text-white float-right"><i class="icon-save"></i> Save</a>
							{{ empty($edit->news_id) ? "Add" : "Edit" }} Category
						</h3>
						<div class="card-body">
							
							<div class="row">
								<div class="col-sm-12 col-lg-12">
									<div class="form-group">
										<label>Add Category</label>
										<input type="text" name="record[category_name]" class="form-control" 
										value="{{ @$edit->category_name }}" placeholder="Category">
									</div>
								</form>
								</div>
							</div>
						</div>
				</div>
			</div>
		</div>
		<form method="post">
			@csrf
		<div class="row mt-4">
			<div class="col-sm-12">
				<div class="card">
						<h3 class="card-title">
							<a href="#delete_all" class="text-white float-right" title="Remove"><i class="icon-trash-o"></i></a>View Category
						</h3>
						<div class="card-body">
							<div class="row">
								<div class="col-sm-12 col-lg-12">
									<div class="form-group">
										<table class="table table-bordered">
									<thead>
										<tr>
											<th width="100">
												<label class="animated-check">
													<input type="checkbox" class="check_all">
													<span class="label-text">All</span>
												</label>
											</th>
											<th>SN.</th>
											<th>Title</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										@php
											$sn = $offset;
										@endphp

										@foreach($records as $rec)
										@php
											$id = $rec->category_id;
											$exist = DB::table('news')->where('news_category', $id)->where('news_is_deleted', 'N')->first();
										@endphp
										<tr>
											<td>
												<label class="animated-check @if(!empty($exist)) disabled @endif">
													<input type="checkbox" name="check[]" value="{{ $rec->category_id }}" class="check" @if(!empty($exist)) disabled @endif>
													<span class="label-text"></span>
												</label>
											</td>
											<td>{{ ++$sn }}</td>
											<td>{!! $rec->category_name !!}</td>
											<td>
												<a href="{{ url('ea-xpanel/category/'.$rec->category_id) }}"><i class="icon-pencil"></i></a>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
									</div>
								</div>
							</div>
						</div>
				</div>
			</div>
		</div>
		</form>
	</div>
</div>