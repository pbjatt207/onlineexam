<div class="page-content">
	<div class="container-fluid">
		<h1>Plans</h1>
		<div class="row">
			<div class="col-sm-4">
				<div class="card">
					<form method="post">
						@csrf
						<h3 class="card-title">{{ empty($edit->course_id) ? "Add" : "Edit" }} Plan</h3>
						<div class="card-body">
							@if (\Session::has('success'))
							    <div class="alert alert-success">
								    {!! \Session::get('success') !!}</li>
								</div>
							@endif
							<div class="form-group">
								<label>Course *</label>
								<select type="text" name="record[plan_course]" class="form-control" value="{{ @$edit->plan_course }}" required>
									<option>Select Course</option>
									@foreach($courses as $course)
										<option @if(@$edit->plan_course == $course->course_id) selected @endif value="{{ $course->course_id }}">{{ $course->course_name }}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label>Plan Name *</label>
								<input type="text" name="record[plan_title]" class="form-control" placeholder="Plan Name" value="{{ @$edit->plan_title }}" required>
							</div>
							<div class="form-group">
								<label>No. of Test *</label>
								<input type="text" name="record[plan_no_of_test]" class="form-control" placeholder="No. of Test" value="{{ @$edit->plan_no_of_test }}" required>
							</div>
							<div class="form-group">
								<label>Regular Price *</label>
								<input type="text" name="record[plan_regular_price]" class="form-control" placeholder="Regular Price" value="{{ @$edit->plan_regular_price }}" required>
							</div>
							<div class="form-group">
								<label>Sell Price *</label>
								<input type="text" name="record[plan_sell_price]" class="form-control" placeholder="Sell Price" value="{{ @$edit->plan_sell_price }}" required>
							</div>
							<div>
								<button class="btn btn-block btn-success">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="card">
					<form method="post">
						@csrf
						<h3 class="card-title">
							@if(!$records->isEmpty())
							<a href="#delete_all" class="text-white float-right" title="Remove"><i class="icon-trash-o"></i></a>
							@endif
							View Course
						</h3>
						<div class="card-body">
							@if(!$records->isEmpty())
							<div class="table-responsive">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th width="100">
												<label class="animated-check">
													<input type="checkbox" class="check_all">
													<span class="label-text">All</span>
												</label>
											</th>
											<th>SN.</th>
											<th>Course</th>
											<th>Name</th>
											<th>No. of test</th>
											<th>Regular Price</th>
											<th>Sell Price</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										@php
											$sn = $offset;
										@endphp

										@foreach($records as $rec)
										<tr>
											<td>
												<label class="animated-check">
													<input type="checkbox" name="check[]" value="{{ $rec->plan_id }}" class="check">
													<span class="label-text"></span>
												</label>
											</td>
											<td>{{ ++$sn }}</td>
											<td>{{ $rec->course_name }}</td>
											<td>{{ $rec->plan_title }}</td>
											<td>{{ $rec->plan_no_of_test }}</td>
											<td>{{ $rec->plan_regular_price }}</td>
											<td>{{ $rec->plan_sell_price }}</td>
											<td>
												<a href="{{ url('ea-xpanel/plan/'.$rec->plan_id) }}"><i class="icon-pencil"></i></a>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							{{ $records->links() }}
							@else
							<div class="no_records_found">
								No record(s) found.
							</div>
							@endif
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>