<div class="page-content">
	<div class="container-fluid">
		<h1>{{ empty($edit->page_id) ? "Add" : "Edit" }} page</h1>
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<form method="post" enctype="multipart/form-data">
						@csrf
						<h3 class="card-title">
							<a href="#save" class="text-white float-right"><i class="icon-save"></i> Save</a>
							{{ empty($edit->page_id) ? "Add" : "Edit" }} Page
						</h3>
						<div class="card-body">
							@if (\Session::has('success'))
							    <div class="alert alert-success">
								    {!! \Session::get('success') !!}</li>
								</div>
							@endif
							<div class="row">
								<div class="col-sm-8 col-lg-9">
									<div class="form-group">
										<label>Title</label>
										<input type="text" name="record[page_title]" class="form-control" placeholder="Title" value="{{ @$edit->page_title }}" required>
									</div>
									<div class="form-group">
										<label>Description</label>
										<textarea rows="10" name="record[page_description]" class="form-control editor">{{ @$edit->page_description }}</textarea>
									</div>
								</div>
								<div class="col-sm-4 col-lg-3">
									<div class="form-group">
										<label>Upload Image</label>
										<div>
											<label class="file-upload form-group">
												<img src="{{ !empty($edit->page_image) ? url('imgs/pages/'.$edit->page_image) : url('imgs/no-image.png') }}">
												<input type="file" name="page_image" id="page_image" accept="image/*">
											</label>
											<label for="page_image" class="btn btn-block btn-success">Choose Image</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>