<div class="page-content">
	<div class="container-fluid">
		<h1>Subject</h1>
		<div class="row">
			<div class="col-sm-4">
				<div class="card">
					<form method="post">
						@csrf
						<h3 class="card-title">{{ empty($edit->subject_id) ? "Add" : "Edit" }} Subject</h3>
						<div class="card-body">
							@if (\Session::has('success'))
							    <div class="alert alert-success">
								    {!! \Session::get('success') !!}</li>
								</div>
							@endif
							<div class="form-group">
								<label>Select Course</label>
								<select name="record[subject_course]" class="form-control" required>
									<option value="">Select Course</option>
									@foreach($courses as $crs)
									<option value="{{ $crs->course_id }}" @if($crs->course_id == @$edit->subject_course) {{"selected"}} @endif>{{ $crs->course_name }}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label>Subject Name *</label>
								<input type="text" name="record[subject_name]" class="form-control" placeholder="Subject Name" value="{{ @$edit->subject_name }}" required>
							</div>
							<div>
								<button class="btn btn-block btn-success">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="card">
					<form method="post">
						@csrf
						<h3 class="card-title">
							@if(!$records->isEmpty())
							<a href="#delete_all" class="text-white float-right" title="Remove" data-toggle="tooltip"><i class="icon-trash-o"></i></a>
							@endif

							View Subject
						</h3>
						<div class="card-body">
							@if(!$records->isEmpty())
							<div class="table-responsive">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th width="100">
												<label class="animated-check">
													<input type="checkbox" class="check_all">
													<span class="label-text">All</span>
												</label>
											</th>
											<th>SN.</th>
											<th>Name</th>
											<th>Course Name</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										@php
											$sn = $offset;
										@endphp

										@foreach($records as $rec)
										@php
											$exist = DB::table('questions')->where('que_subject', $rec->subject_id)->first();
										@endphp
										<tr>
											<td>
												<label class="animated-check @if(!empty($exist)) disabled @endif">
													<input type="checkbox" name="check[]" value="{{ $rec->subject_id }}" class="check"  @if(!empty($exist)) disabled @endif>
													<span class="label-text"></span>
												</label>
											</td>
											<td>{{ ++$sn }}</td>
											<td>{{ $rec->subject_name }}</td>
											<td>{{ $rec->course_name }}</td>
											<td>
												<a href="{{ url('ea-xpanel/subject/'.$rec->subject_id) }}"><i class="icon-pencil"></i></a>
												<a href="{{ url('ea-xpanel/subject/copy/'.$rec->subject_id) }}" title="Copy" data-toggle="tooltip" style="margin-left: 15px;"><i class="icon-copy"></i>&nbsp;</a>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							{{ $records->links() }}
							@else
							<div class="no_records_found">
								No record(s) found.
							</div>
							@endif
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>