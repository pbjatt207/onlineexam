<div class="page-content">
	<div class="container-fluid">
		<h1>Topic</h1>
		<div class="row">
			<div class="col-sm-12">
				<div class="card form-group">
					<h3 class="card-title"><i class="icon-filter"></i> Filter By</h3>
					<div class="card-body">
						<form>
							@csrf
							<div class="row">
								<div class="col-sm-2">
									<div class="form-group">
										<label>By Keyword</label>
										<input type="text" name="search[keyword]" placeholder="By Keyword..." class="form-control" value="{{ @$search['keyword'] }}">
									</div>
								</div>
								<!-- <div class="col-sm-2">
									<div class="form-group">
										<label>By Langauage</label>
										<select name="search[que_language]" class="form-control">
											<option value="">Both</option>
											<option value="HI" {{ (@$search['que_language'] == 'HI') ? ' selected' : '' }}>Hindi</option>
											<option value="EN" {{ (@$search['que_language'] == 'EN') ? ' selected' : '' }}>English</option>
										</select>
									</div>
								</div> -->
								<div class="col-sm-2">
									<div class="form-group">
										<label>By Course</label>
										<select name="search[topic_course]" class="form-control course" data-target="#searchSubject">
											<option value="">Select Course</option>
											@foreach($courses as $course)
											<option value="{{ $course->course_id }}" {{ (@$search['topic_course'] == $course->course_id) ? ' selected' : '' }}>{{ $course->course_name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
										<label>By Subject</label>
										<select name="search[topic_subject]" class="form-control subject" id="searchSubject" data-target="#searchTopic">
											<option value="">Select Subject</option>
											@foreach($subjects as $subject)
											<option value="{{ $subject->subject_id }}" {{ (@$search['topic_subject'] == $subject->subject_id) ? ' selected' : '' }}>{{ $subject->subject_name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<!-- <div class="col-sm-2">
									<div class="form-group">
										<label>By Topic</label>
										<select name="search[topic_topic]" class="form-control" id="searchTopic">
											<option value="">Select Topic</option>
											@foreach($topics as $topic)
											<option value="{{ $topic->topic_id }}" {{ (@$search['que_topic'] == $topic->topic_id) ? ' selected' : '' }}>{{ $topic->topic_name }}</option>
											@endforeach
										</select>
									</div>
								</div> -->
								<div class="col-sm-2">
									<label>&nbsp;</label>
									<button class="btn btn-block btn-success">Search</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="card">
					<form method="post">
						@csrf
						<h3 class="card-title">{{ empty($edit->topic_id) ? "Add" : "Edit" }} Topic</h3>
						<div class="card-body">
							@if (\Session::has('success'))
							    <div class="alert alert-success">
								    {!! \Session::get('success') !!}</li>
								</div>
							@endif
							<div class="form-group">
								<label>Select Course</label>
								<select name="record[topic_course]" class="form-control course" data-target="#topic_subject" required>
									<option value="">Select Course</option>
									@foreach($courses as $crs)
									<option value="{{ $crs->course_id }}" @if($crs->course_id == @$edit->topic_course) selected @endif>{{ $crs->course_name }}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label>Select Subject</label>
								<select name="record[topic_subject]" id="topic_subject" class="form-control" required>
									<option value="">Select Subject</option>
									@foreach($subjects as $subj)
									<option value="{{ $subj->subject_id }}" @if($subj->subject_id == @$edit->topic_subject) selected @endif>{{ $subj->subject_name }}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label>Topic Name *</label>
								<input type="text" name="record[topic_name]" class="form-control" placeholder="Topic Name" value="{{ @$edit->topic_name }}" required>
							</div>

							<div class="form-group">
								<label>Topic Direction *</label>
								<input type="text" name="record[topic_direction]" class="form-control" placeholder="Topic Direction" value="{{ @$edit->topic_direction }}" required>
							</div>
							<div class="form-group">
								<label>Use Paragraph *</label>
								<div>
									<label class="animated-check">
										<input type="checkbox" name="record[topic_is_para]" value="Y" @if(@$edit->topic_is_para == "Y") {{ 'checked' }} @endif>
										<span class="label-text">Check if it is a paragraph topic.</span>
									</label>
								</div>
							</div>
							<div>
								<button class="btn btn-block btn-success">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="card">
					<form method="post">
						@csrf
						<h3 class="card-title">
							@if(!$records->isEmpty())
							<a href="#delete_all" class="text-white float-right" title="Remove"><i class="icon-trash-o"></i></a>
							@endif
							View Topic
						</h3>
						<div class="card-body">
							@if(!$records->isEmpty())
							<div class="table-responsive">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th width="100">
												<label class="animated-check">
													<input type="checkbox" class="check_all">
													<span class="label-text">All</span>
												</label>
											</th>
											<th>SN.</th>
											<th>Name</th>
											<th>Course Name</th>
											<th>Subject Name</th>
											<th>Use Paragraph</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										@php
											$sn = $offset;
										@endphp

										@foreach($records as $rec)
										@php
											$exist = DB::table('questions')->where('que_topic', $rec->topic_id)->first();
										@endphp
										<tr>
											<td>
												<label class="animated-check @if(!empty($exist)) disabled @endif">
													<input type="checkbox" name="check[]" value="{{ $rec->topic_id }}" class="check" @if(!empty($exist)) disabled @endif>
													<span class="label-text"></span>
												</label>
											</td>
											<td>{{ ++$sn }}</td>
											<td>{{ $rec->topic_name }}</td>
											<td>{{ $rec->course_name }}</td>
											<td>{{ $rec->subject_name }}</td>
											<td>{{ $rec->topic_is_para }}</td>
											<td>
												<a href="{{ url('ea-xpanel/topic/'.$rec->topic_id) }}"><i class="icon-pencil"></i></a>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							{{ $records->links() }}
							@else
							<div class="no_records_found">
								No record(s) found.
							</div>
							@endif
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
