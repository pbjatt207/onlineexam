<div class="page-content">
	<div class="container-fluid">
		<h1>{{ empty($edit->news_id) ? "Add" : "Edit" }} News</h1>
		
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<form method="post" enctype="multipart/form-data">
						@csrf
						<h3 class="card-title">
							<a href="#save" class="text-white float-right"><i class="icon-save"></i> Save</a>
							{{ empty($edit->news_id) ? "Add" : "Edit" }} News
						</h3>
						<div class="card-body">
							@if (\Session::has('success'))
							    <div class="alert alert-success">
								    {!! \Session::get('success') !!}</li>
								</div>
							@endif
							<div class="row">
								<div class="col-sm-12 col-lg-12">
									<div class="form-group">
										<label>Title</label>
										<input type="text" name="record[news_title]" class="form-control" placeholder="Title" value="{{ @$edit->news_title }}" required>
									</div>
									<div class="form-group">
										<label>News Category</label>
										<select name="record[news_category]" class="form-control course">
											<option value="">Select Category</option>
											@foreach($category as $rec)
											<option value="{{ $rec->category_id }}">{{ $rec->category_name }} </option>
											@endforeach
										</select>
									</div>
									<div class="form-group">
										<label>Description</label>
										<textarea rows="10" name="record[news_description]" class="form-control editor">{{ @$edit->news_description }}</textarea>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>