<div class="page-content">
	<div class="container-fluid">
		<form method="post" enctype="multipart/form-data">
			@csrf
			<h1>
				<button class="btn btn-success float-right btn-sm">&nbsp; &nbsp; <i class="icon-save"></i> Save Details &nbsp; &nbsp;</button>
				Settings
			</h1>

			@if (\Session::has('success'))
			    <div class="alert alert-success">
				    {!! \Session::get('success') !!}</li>
				</div>
			@endif
			<div class="row">
				<div class="col-sm-4">
					<div class="card form-group">
						<h3 class="card-title">General Information</h3>
						<div class="card-body">
							<div class="form-group">
								<label>Candidate roll no. prefix</label>
								<input type="text" name="record[site_roll_pre]" value="{{ @$edit->site_roll_pre }}" class="form-control">
							</div>
						</div>
					</div>
					<!-- <div class="card">
						<h3 class="card-title">Site Information</h3>
						<div class="card-body">
							<div class="form-group">
								<label>Site Title</label>
								<input type="text" name="record[site_title]" value="{{ @$edit->site_title }}" class="form-control" required="">
							</div>
							<div class="form-group">
								<label>Site Tagline</label>
								<input type="text" name="record[site_tagline]" value="{{ @$edit->site_tagline }}" class="form-control" required="">
							</div>
							<div class="form-group">
								<label>Upload Logo (193 x 44 px) - White</label>
								<div>
									<input type="file" name="site_logo" accept="image/png" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label>Upload Logo (193 x 44 px) - Color</label>
								<div>
									<input type="file" name="site_logo2" accept="image/png" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label>Upload Favicon</label>
								<div>
									<input type="file" name="site_favicon" accept=".ico" class="form-control">
								</div>
							</div>
							<div class="row text-center">
								@if(!empty($edit->site_logo))
									<div class="col-5">
										<label>Logo</label>
										<div style="background: #ccc; padding: 5px;">
											<img src="{{ url('site/'.$edit->site_logo) }}" style="max-width: 100%;">
										</div>
									</div>
								@endif
								@if(!empty($edit->site_logo2))
									<div class="col-5">
										<label>Logo</label>
										<div style="background: #ccc; padding: 5px;">
											<img src="{{ url('site/'.$edit->site_logo2) }}" style="max-width: 100%;">
										</div>
									</div>
								@endif
								@if(!empty($edit->site_favicon))
								<div class="col-2">
									<label>Favicon</label>
									<div style="background: #ccc; padding: 5px;">
										<img src="{{ url('site/'.$edit->site_favicon) }}" style="max-width: 100%;">
									</div>
								</div>
								@endif
							</div>
						</div>
					</div> -->
				</div>
				<!-- <div class="col-sm-8">
					<div class="card">
						<h3 class="card-title">
							Homepage
						</h3>
						<div class="card-body">
							<div class="form-group">
								<label>Title 1</label>
								<input type="text" name="record[site_home_title1]" class="form-control" value="{{ @$edit->site_home_title1 }}">
							</div>
							<div class="form-group">
								<label>Paragraph 1</label>
								<textarea rows="10" name="record[site_home_para1]" class="form-control editor">{!! @$edit->site_home_para1 !!}</textarea>
							</div>
							<div class="form-group">
								<label>Title 2</label>
								<input type="text" name="record[site_home_title2]" class="form-control" value="{{ @$edit->site_home_title2 }}">
							</div>
							<div class="form-group">
								<label>Paragraph 2</label>
								<textarea rows="10" name="record[site_home_para2]" class="form-control editor">{!! @$edit->site_home_para2 !!}</textarea>
							</div>
						</div>
					</div>
				</div> -->
			</div>
		</form>
	</div>
</div>