<div class="page-content">
	<div class="container-fluid">
		<h1>
			<a href="{{ url('ea-xpanel/paragraph/add') }}" class="btn btn-success btn-sm float-right"><i class="icon-plus"></i> Add More</a>
			View Paragraph
		</h1>
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<form method="post">
						@csrf
						<h3 class="card-title">
							@if(!$records->isEmpty())
							<a href="#delete_all" class="text-white float-right" title="Remove"><i class="icon-trash-o"></i></a>
							@endif
							View Paragraph
						</h3>
						<div class="card-body">
							@if(!$records->isEmpty())
							<div class="table-responsive">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th width="100">
												<label class="animated-check">
													<input type="checkbox" class="check_all">
													<span class="label-text">All</span>
												</label>
											</th>
											<th>SN.</th>
											<th width="500">Name</th>
											<th>Paragraph</th>
											<th>Course Name</th>
											<th>Subject Name</th>
											<th>Topic Name</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										@php
											$sn = $offset;
										@endphp

										@foreach($records as $rec)
										@php
											$exist = DB::table('questions')->where('que_paragraph', $rec->para_id)->first();
										@endphp
										<tr>
											<td>
												<label class="animated-check @if(!empty($exist)) disabled @endif">
													<input type="checkbox" name="check[]" value="{{ $rec->para_id }}" class="check" @if(!empty($exist)) disabled @endif>
													<span class="label-text"></span>
												</label>
											</td>
											<td>{{ ++$sn }}</td>
											<td>{!! $rec->para_name !!}</td>
											<td class="text-justify">{!! substr(strip_tags($rec->para_description), 0, 400).' &hellip; ' !!}</td>
											<td>{{ $rec->course_name }}</td>
											<td>{{ $rec->subject_name }}</td>
											<td>{{ $rec->topic_name }}</td>
											<td>
												<a href="{{ url('ea-xpanel/paragraph/add/'.$rec->para_id) }}"><i class="icon-pencil"></i></a>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							{{ $records->links() }}
							@else
							<div class="no_records_found">
								No record(s) found.
							</div>
							@endif
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>