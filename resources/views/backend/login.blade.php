<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="csrf-token" content="{{ csrf_token() }}" />
		<title>Login | English Aliens</title>

		{{ HTML::style('css/bootstrap.min.css') }}
		{{ HTML::style('icomoon/style.css') }}
		{{ HTML::style('admin/css/style.css') }}

		<link rel="icon" href="{{ url('imgs/ea_logo.ico') }}">
	</head>
	<body>
		<section class="full-section">
			<div class="half-section"></div>
			<div class="login-form">
				<div class="text-center form-group">
					<img src="{{ url('imgs/logo-2.png') }}">
				</div>
				<form id="loginForm" action="{{ url('ea-xpanel/ajax/user_login') }}" method="post">
					@csrf
					<div class="form-msg"></div>
					<div class="form-group">
						<label>Username</label>
						<input type="text" name="record[user_login]" class="form-control" placeholder="Username" required autofocus autocomplete="off">
					</div>
					<div class="form-group">
						<label>Password</label>
						<input type="password" name="record[user_password]" class="form-control" placeholder="Password" required autocomplete="new-password">
					</div>
					<div class="form-group">
						<button class="btn btn-success btn-block">Log Me In</button>
					</div>
					<div>
						If you don't have account, then <a href="{{ url('ea-xpanel/register') }}">Register here</a>
					</div>
				</form>
			</div>
		</section>

		{{ HTML::script('js/jquery.min.js') }}
		{{ HTML::script('js/popper.min.js') }}
	    {{ HTML::script('js/bootstrap.min.js') }}
	    {{ HTML::script('js/sweetalert.min.js') }}
	    {{ HTML::script('js/validation.js') }}
	    {{ HTML::script('admin/tinymce/js/tinymce/tinymce.min.js') }}
	    {{ HTML::script('admin/js/main.js') }}
	</body>
</html>