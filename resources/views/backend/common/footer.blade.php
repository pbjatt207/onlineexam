	    {{ HTML::script('js/popper.min.js') }}
	    {{ HTML::script('js/bootstrap.min.js') }}
	    {{ HTML::script('js/sweetalert.min.js') }}
	    {{ HTML::script('js/validation.js') }}
	    {{ HTML::script('admin/tinymce/js/tinymce/tinymce.min.js') }}
	    {{ HTML::script('admin/js/jquery-ui.js') }}
	    {{ HTML::script('admin/js/jquery.multiselect.js') }}
	    {{ HTML::script('https://code.jquery.com/ui/1.12.1/jquery-ui.js') }}
	    {{ HTML::script('admin/js/main.js') }}
	    <script>
		    $(function () {
		        $('select[multiple].active.3col').multiselect({
		            columns: 1,
		            placeholder: 'Select Courses',
		            search: true,
		            searchOptions: {
		                'default': 'Search Courses'
		            },
		            selectAll: true
		        });

			    $( ".datepicker" ).datepicker({
			    	dateFormat: 'yy-mm-dd',
			    	changeYear: true,
			    	changeMonth: true,
			    	yearRange: '-100:+10'
			    });
			    $( ".datepicker_no_future" ).datepicker({
			    	dateFormat: 'yy-mm-dd',
			    	maxDate: -3650,
			    	changeYear: true,
			    	changeMonth: true,
			    	yearRange: '-100:-10'
			    });
			  });
		</script>
	</body>
</html>