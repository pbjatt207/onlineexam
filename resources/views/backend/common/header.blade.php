<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Admin Login | English Aliens</title>
	{{ HTML::style('css/bootstrap.min.css') }}
	{{ HTML::style('icomoon/style.css') }}
	{{ HTML::style('admin/css/jquery-ui.css') }}
	{{ HTML::style('admin/css/jquery.multiselect.css') }}
	{{ HTML::style('https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css') }}
	{{ HTML::style('admin/css/style.css') }}
	{{ HTML::style('admin/css/responsive.css') }}

	<link rel="icon" href="{{ url('site/'.$site->site_favicon) }}">
	
	{{ HTML::script('js/jquery.min.js') }}
</head>
<body>
	<input type="hidden" id="base_url" value="{{ url('ea-xpanel/') }}">
	<input type="hidden" id="wbase_url" value="{{ url('/') }}">
	<header>
		<div class="container-fluid">
			<div class="clearfix">
				<div class="float-left">
					<a href="{{ url('ea-xpanel') }}" title="Dashboard"><img src="{{ url('imgs/logo.png') }}" alt="English Aliens Logo" title="English Aliens Logo"></a>
				</div>
				<div class="nav-icon nav-item float-right mt-1">
					<span></span>
					<span></span>
					<span></span>
				</div>
				<div class="float-right">
					<nav>
						<ul class="main-navbar">
							<li><a href="{{ url('ea-xpanel') }}">Dashboard</a></li>
							@if($profile->role_name == "Candidate")
							<li><a href="{{ url('ea-xpanel/exams') }}">My Exams</a></li>

							<li>
								<a><i class="icon-user2"></i> {{ $profile->user_name }} <i class="icon-angle-down"></i></a>
								<ul>
									<li><a href="{{ url('ea-xpanel/user/profile') }}">Edit Profile</a></li>
									<li><a href="{{ url('ea-xpanel/change-password') }}">Change Password</a></li>
									<li><a href="{{ url('ea-xpanel/logout') }}">Logout</a></li>
								</ul>
							</li>
							@endif
							@if($profile->role_name == "Admin")
							<li><a href="#">Questions Bank <i class="icon-angle-down"></i></a>
								<ul>
									<li><a href="{{ url('ea-xpanel/courses') }}">Courses</a></li>
									<li><a href="{{ url('ea-xpanel/subject') }}">Subject</a></li>
									<li><a href="{{ url('ea-xpanel/topic') }}">Topic</a></li>
									<li><a href="{{ url('ea-xpanel/paragraph') }}">Paragraph</a></li>
									<li><a href="{{ url('ea-xpanel/question') }}">Questions</a></li>
									<!-- <li><a href="{{ url('ea-xpanel/plan') }}">Plan</a></li> -->
								</ul>
							</li>

							<li><a href="#">Test Series <i class="icon-angle-down"></i></a>
								<ul>
									<li><a href="{{ url('ea-xpanel/instruction') }}">Instructions</a></li>
									<li><a href="{{ url('ea-xpanel/candidate') }}">Candidates</a></li>
									<li><a href="{{ url('ea-xpanel/test') }}">Test</a></li>
								</ul>
							</li>

							<li><a href="#">Website Master <i class="icon-angle-down"></i></a>
								<ul>
									<li><a href="{{ url('ea-xpanel/blog') }}">Blogs</a></li>
									<li><a href="{{ url('ea-xpanel/category') }}">Category</a></li>
									<li><a href="{{ url('ea-xpanel/news') }}">News</a></li>
									<li><a href="{{ url('ea-xpanel/page') }}">Pages</a></li>
									<li><a href="{{ url('ea-xpanel/reading') }}">Reading</a></li>
								</ul>
							</li>
							<li>
								<a href=""><i class="icon-user2"></i> {{ $profile->user_name }} <i class="icon-angle-down"></i></a>
								<ul>
									<li id="sub_nav"><i class="icon-angle-left"></i> Settings
										<ul id="sub_nav_show">
											<li><a href="{{ url('ea-xpanel/general-settings') }}">General Settings</a></li>
											<li><a href="{{ url('ea-xpanel/website-settings') }}">Website Settings</a></li>
										</ul>
									</li>
									<li><a href="{{ url('ea-xpanel/change-password') }}">Change Password</a></li>
									<li><a href="{{ url('ea-xpanel/logout') }}">Logout</a></li>
								</ul>
							</li>
							@endif
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</header>
