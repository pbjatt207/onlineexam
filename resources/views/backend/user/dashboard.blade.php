<div class="page-content">
	<div class="container-fluid">
		<h1>Welcome to Candidate Panel</h1>
		<div class="row">
			@foreach($courses as $course)
			<div class="col-md-6 col-lg-3">
				<div class="dashboard-box form-group">
					<div class="inrcntntdash">
						<div class="row">
							<div class="col-md-4">
								<div class="lytcntnt text-center">
									<img class="img-fluid" src="{{ url('imgs/courses.png') }}">
								</div>
							</div>
							<div class="col-md-8">
								<div class="rytcntnt">
									<h3>{{ $course->course_name }}</h3>
									<!-- <p>Admit card of Test 1</p> -->
								</div>
							</div>
						</div>
					</div>
					<div class="greenbtn">
						<a  href="{{ url('ea-xpanel/test_details/'.$course->course_slug ) }}" class="btn btn-green"><i class="icon-cloud_download"></i> Take a Test</a>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</div>
