<div class="page-content">
	<div class="container-fluid">
		<h1>Change Password</h1>
		<div class="row">
			<div class="col-sm-4 mx-auto">
				<div class="card">
					<h3 class="card-title"> <i class="icon-lock"></i> Change Password </h3>
					<div class="card-body">
						<form method="post">
							@csrf
							@if (\Session::has('success'))
							    <div class="alert alert-success">
								    {!! \Session::get('success') !!}</li>
								</div>
							@endif
							@if (\Session::has('failed'))
							    <div class="alert alert-danger">
								    {!! \Session::get('failed') !!}</li>
								</div>
							@endif
							<div class="form-group">
								<label>Current Password</label>
								<input type="password" name="password[current]" class="form-control" placeholder="Current Password" required>
							</div>
							<div class="form-group">
								<label>New Password</label>
								<input type="password" name="password[new]" class="form-control" placeholder="New Password" required>
							</div>
							<div class="form-group">
								<label>Re-type Password</label>
								<input type="password" name="password[confirm]" class="form-control" placeholder="Re-type Password" required>
							</div>
							<div>
								<button class="btn btn-success btn-block">Submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>