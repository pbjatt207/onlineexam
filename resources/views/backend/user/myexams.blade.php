<div class="page-content exams">
	<div class="container-fluid">
		<div class="card">
			<h3 class="card-title">My Attemped Exams</h3>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-stripped table-bordered">
						<thead>
							<tr>
								<th>SN.</th>
								<th>Test Name</th>
								<th>Level</th>
								<th>Total Questions</th>
								<th>Gain Score</th>
								<th>Rank</th>
								<th>Details</th>
							</tr>
						</thead>
						<tbody>
							@php $sn = 1; @endphp
							@foreach($test as $rec)
							@php
							
								$rank = DB::table('test_results')
										->where('tres_uid', $user_id)
										->where('tres_tid', $rec->tres_tid)
										->select(DB::raw("FIND_IN_SET( tres_gain_marks, (    
											SELECT GROUP_CONCAT( tres_gain_marks ORDER BY tres_gain_marks DESC ) FROM `ea_test_results` WHERE `tres_tid` = {$rec->tres_tid} )
										) AS rank"))
										->first();
							@endphp
							<tr>
								<td>{{ $sn++ }}</td>
								<td>{{ $rec->test_name }}</td>
								<td>{{ $rec->test_defficulty_level }}</td>
								<td>{{ $rec->test_no_of_question }}</td>
								<td>{{ $rec->tres_gain_marks }} out of {{ $rec->test_total_marks }}</td>
								<td>{{ $rank->rank }}</td>
								<td>
									<a style="color: #555;" href="{{ url('ea-xpanel/result/'.$rec->tres_uid.'/'.$rec->tres_tid) }}">
										<strong>View Details</strong>
									</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>