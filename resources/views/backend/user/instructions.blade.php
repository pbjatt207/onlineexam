<div id="testInstruction" class="d-none">
	<div class="container-fluid">
		<div class="row">
			<div class="card">
				<div class="card_inst">
					{!! $inst->inst_description !!}
				</div>
				<div class="resume_test mb-3">
					<a href="" onclick="toggleFullScreen(document.body)" class="btn btn-success btn-sm ml-3 mt-3">START TEST <i class="icon-keyboard_arrow_right"></i></a>
				</div>
			</div>
		</div>
	</div>
</div>


<section id="onlineExam" class="test">
	<div class="test_heading">
		<img src="{{ url('imgs/sscbanner.jpg') }}">
	</div>
	<div class="test_line"></div>
	<div class="test_time_control">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="inner_timer">
						<strong>SSC CGL Tier I Free Test</strong>
					</div>
				</div>
				<div class="col-sm-4 text-center">
					<div class="inner_timer">
						<strong>Language:</strong>
						<select>
							<option>English/Hindi</option>
							<option>English</option>
							<option>Hindi</option>
						</select>
					</div>
				</div>
				<div class="col-sm-4 text-right">
					<div class="inner_timer">
						<strong>Time Left:</strong>
						<!-- <div> -->
							<strong class="timer_box">
								00:59:54 hrs
							</strong>
						<!-- </div> -->
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-sm-8 mt-3 mb-3">
				<div class="que_heading">
					<span>Text Size</span>
					<div class="circle">
						<div>A<sup>-</sup></div>
						<div>A<sup>+</sup></div>
					</div>
				</div>

				<div class="que_body">

					<div class="mark_info">
						<strong>
							(Mark:2)(Negative Mark/s-0)
						</strong>
					</div><br>

					<div class="c_details">
						<strong>
							Section: General Intelligence and Reasoning
						</strong>
					</div>

					<div class="question mt-4 mb-4">
						<div>Q1. </div>
						<div class="questions">
							Which one set of letters when sequentially placed at the gaps in the given letter series shall complete it?
						</div>
					</div>
					<form class="test-form">
						@php $sn = 0; @endphp
						@for($i = 1; $i <= 4; $i++)
						<label class="answers d-block">
							<input type="radio" name="option" class="form-control test_radio d-inline">
							{{ ++$sn }}) Answer
						</label>
						@endfor
					</form>
				</div>

			</div>
			<div class="col-sm-4 mt-3 mb-3">
				<div class="profile">
					<div class="row">
						<div class="col-sm-6">
							<div>
								<div>
									<img class="pro_image" src="{{ url('imgs/dummy-user-old.png') }}">
								</div>
								<div class="mt-3">
									<img class="pro_image" src="{{ url('imgs/dummy-sign-old.png') }}">
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="candi-details mt-2">
								<strong>Roll no.</strong>
								<span>Test 1</span>
							</div>

							<div class="candi-details mt-3">
								<strong>Candidate Name</strong>
								<span>Practice Test</span>
							</div>
						</div>
					</div>
				</div>

				<div class="que-pallete mt-2">
					<strong class="qp_heading">Number of Questions</strong>
					<div class="que_status clearfix text-center mb-2">
						@for($i = 1, $sn = 1; $i <= 100; $i++, $sn++)
						<div class="que_box">Q{{ sprintf('%02d', $sn) }}</div>
						@endfor
					</div>
					<div class="qp_footer">
						<div class="row text-center">
							<div class="col-sm-3">
								<div class="attempt">0</div>
								<span class="qp-lable">Attempted</span>
							</div>
							<div class="col-sm-3">
								<div class="tagged">0</div>
								<span class="qp-lable">Tagged</span>
							</div>
							<div class="col-sm-3">
								<div class="aandt">0</div>
								<span class="qp-lable">Attempted & Tagged</span>
							</div>
							<div class="col-sm-3">
								<div class="unattempt">0</div>
								<span class="qp-lable">unattempted</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="test_footer">
		<div class="footer_test">
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<button class="btn-left">
							<div class="footer_arrow_image">
								<img src="{{ url('imgs/left-arrow.png') }}">
							</div>
							Previous Question
						</button>

						<button class="btn-right">
							Next Question
							<div class="footer_arrow_image">
								<img src="{{ url('imgs/right-arrow.png') }}">
							</div>
						</button>
					</div>
					<div class="col-sm-2">
						<div class="f_tagged">Tag</div>
					</div>
					<div class="col-sm-2">
						<div class="f_erase">Erase</div>
					</div>

					<div class="col-sm-4">
						<button class="btn-footer_right">
							<div class="btn-foote-right-icon">
								<i class="icon-check"></i>
							</div>
							Preview Submit
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</section>