
<section class="greybg">
	<div class="container-fluid">
		<div class="m30">
			<form method="post">
				@csrf
				<div class="row">
					<div class="col-md-8 offset-md-2">
						<div class="card p-5 mt-5">
							<div class="card-heading">
								<h3>Feedback</h3>
							</div>
							<div class="card-content">
								<div class="form-group">
									<label>Write Your Feedback (Required)</label>
									<textarea name="feedback" rows="10" class="form-control" required></textarea>
								</div>
								<div class="form-group text-right">
									<button type="submit" class="btn btn-primary"><i class="icon-check"></i> Done</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>