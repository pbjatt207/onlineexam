<div class="page-content">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-8">
				<div class="card">
					<form method="post" enctype="multipart/form-data">
						@csrf
						<h3 class="card-title">Profile</h3>
						<div class="card-body">
							@if (\Session::has('success'))
							    <div class="alert alert-success">
								    {!! \Session::get('success') !!}</li>
								</div>
							@endif
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label>Courses</label>
											@php
											$cids = !empty($records->user_courses) ? explode(",", $records->user_courses) : array();
											@endphp
											<select name="record[user_courses][]" multiple="multiple" class="3col active">
												@foreach($courses as $course)
										        <option value="{{ $course->course_id }}" @if(in_array($course->course_id, $cids)) selected @endif>{{ $course->course_name }}</option>
										        @endforeach
										    </select>
										</div>
									</div>
								</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label>F-Name *</label>
										<input type="text" name="record[candidate_f_name]" class="form-control" placeholder="F-Name" value="{{ $records->candidate_f_name }}" required>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>L-Name *</label>
										<input type="text" name="record[candidate_l_name]" class="form-control" placeholder="L-Name" value="{{ $records->candidate_l_name }}" required>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label>ADDRESS (REQUIRED)</label>
										<input type="text" name="record[candidate_address]" class="form-control" placeholder="Address" value="{{ $records->candidate_address }}" required>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<label>STATE (REQUIRED)</label>
										<input type="text" name="record[candidate_state]" class="form-control" placeholder="State" value="{{ $records->candidate_state }}" required>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label>CITY (REQUIRED)</label>
										<input type="text" name="record[candidate_city]" class="form-control" placeholder="City" value="{{ $records->candidate_city }}" required>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label>POST CODE (REQUIRED)</label>
										<input type="text" name="record[candidate_post_code]" class="form-control" placeholder="Post Code" value="{{ $records->candidate_post_code }}" required>
									</div>
								</div>
							</div>
						</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card">
					<h3 style="padding:0;" class="card-title desk"><button style=" color:#fff; font-weight: bold;" class="btn btn-block text-right"><i class="icon-save"></i> &nbsp;{{ empty($edit->candidate_id) ? "Save" : "Update" }}</button> </h3>
					<div class="candidate">
						<div style="display:flex;"><h5>Upload Image</h5>&nbsp;( in 100kb )</div>
							<div class="divider"></div>
						<div class="file-upload">

						  <div class="col image-upload-wrap">
				           <label class="file-upload form-group" style="padding: 0px; border: 1px solid #ccc;">
				            <img class="file-upload-image"  src="{{ !empty($records->candidate_image) ? url('imgs/candidate/'.$records->candidate_image) : url('imgs/no-image.png') }}">
				            <input type="file" class="file-upload-input" name="candidate_image" accept="image/*" id="candidateImage" >
				            </label>
			            </div>
						  <label for="candidateImage" class="file-upload-btn btn btn-info btn-block">Select Image</label>
						</div>
					</div>

					<div class="candidate">
						<div style="display:flex;"><h5>Signature Image</h5>&nbsp;( in 50kb )</div>
							<div class="divider"></div>
						<div class="file-upload">

						  <div class="col image-upload-wrap">
				           <label class="file-upload form-group" style="padding: 0px; border: 1px solid #ccc;">
				            <img style="max-height: 50px; width: 100%;" class="file-upload-image"  src="{{ !empty($records->candidate_signature) ? url('imgs/candidate/'.$records->candidate_signature) : url('imgs/no-image.png') }}">
				            <input type="file" class="file-upload-input" name="candidate_signature" accept="image/*" id="signatUre" >
				            </label>
			            </div>
						  <label for="signatUre" class="file-upload-btn btn btn-info btn-block">Select Signature</label>
						</div>
					</div>
					<h3 style="padding:0;" class="card-title mob"><button style=" color:#fff; font-weight: bold;" class="btn btn-block"><i class="icon-save"></i> &nbsp;{{ empty($edit->candidate_id) ? "Save" : "Update" }}</button> </h3>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
