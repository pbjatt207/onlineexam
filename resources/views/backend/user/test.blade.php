<div class="page-content">
	<div class="container-fluid">
		<div class="row">
			@if(!$test->isEmpty())
			@foreach($test as $rec)
			@php
				$isAttempted = DB::table('test_results')
						   ->where('tres_tid', $rec->test_id)
						   ->where('tres_uid', $profile->user_id)
						   ->first();
			@endphp
			<div class="col-md-6 col-lg-3">
				<div class="dashboard-box form-group">
					<div class="inrcntntdash">
						<div class="row">
							<div class="col-md-4">
								<div class="lytcntnt text-center">
									<img class="img-fluid" src="{{ url('imgs/courses.png') }}">
								</div>
							</div>
							<div class="col-md-8">
								<div class="rytcntnt">
									<h3>{{ $rec->test_name }}</h3>
									<!-- <p>Admit card of Test 1</p> -->
								</div>
							</div>
						</div>
					</div>
					<div class="greenbtn">
						@if(!empty($isAttempted) && $profile->user_mobile != "9571342990")
						<a  href="{{ url('ea-xpanel/result/'.$isAttempted->tres_uid.'/'.$isAttempted->tres_tid) }}" class="btn btn-green"><i class="icon-cloud_download"></i> Test attempted</a>
						@else
						    @php
						        if($profile->user_mobile != "9571342990") {
    						        DB::table('test_results')
            						   ->where('tres_tid', $rec->test_id)
            						   ->where('tres_uid', $profile->user_id)
            						   ->delete();
            				    }
						    @endphp
						<a  href="{{ url('ea-xpanel/online-exam/'.$rec->test_slug) }}" class="btn btn-green"><i class="icon-cloud_download"></i> Start test</a>
						@endif

					</div>
				</div>
			</div>
			@endforeach
			@else
			<div class="container-fluid">
				<div class="no_records_found">
					No test(s) found in course <strong>{{ $course->course_name }}</strong>.
				</div>
			</div>
			@endif
		</div>
	</div>
</div>
