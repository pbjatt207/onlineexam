<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="csrf-token" content="{{ csrf_token() }}" />
		<title>Register | English Aliens</title>

		{{ HTML::style('css/bootstrap.min.css') }}
		{{ HTML::style('icomoon/style.css') }}
		{{ HTML::style('admin/css/jquery-ui.css') }}
		{{ HTML::style('admin/css/jquery.multiselect.css') }}
		{{ HTML::style('admin/css/style.css') }}

		<link rel="icon" href="{{ url('imgs/ea_logo.ico') }}">
	</head>
	<body>
		<section class="full-section">
			<div class="half-section"></div>
			<div class="login-form">
				<div class="text-center form-group">
					<img src="{{ url('imgs/logo-2.png') }}">
				</div>
				<form method="post">
					@csrf
					<div class="form-msg"></div>
					<input type="hidden" value="{{ $setting->site_roll_pre }}{{ $roll_number }}" name="record[candidate_roll_number]">
					<div class="form-group">
						<label>Name</label>
						<input type="text" name="record[user_name]" class="form-control" placeholder="Name" required autofocus autocomplete="off">
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="text" name="record[user_email]" class="form-control" placeholder="E-mail" required autofocus autocomplete="off">
					</div>
					<div class="form-group">
						<label>Mobile No.</label>
						<input type="text" name="record[user_mobile]" class="form-control" placeholder="Mobile No." required autofocus autocomplete="off">
					</div>

					<div class="form-group">
						<label>Password</label>
						<input type="password" name="record[user_password]" class="form-control password" placeholder="Password" required autocomplete="new-password">
					</div>
					<div class="form-group">
						<label>Confirm Password</label>
						<input type="password" name="record[user_confirm_password]" class="form-control confirm-password" placeholder="Confirm Password" required autocomplete="new-password">
					</div>
					<div class="form-group">
						<label>Courses</label>
						<select name="record[user_courses][]" multiple="multiple" class="3col active">
							@foreach($courses as $course)
					        <option value="{{ $course->course_id }}">{{ $course->course_name }}</option>
					        @endforeach
					    </select>
					</div>
					<div class="form-group">
						<button class="btn btn-success btn-block">Register</button>
					</div>
					<div>
						If already have an account, then <a href="{{ url('ea-xpanel') }}">Login here</a>
					</div>
				</form>
			</div>
		</section>

		{{ HTML::script('js/jquery.min.js') }}
		{{ HTML::script('js/popper.min.js') }}
	    {{ HTML::script('js/bootstrap.min.js') }}
	    {{ HTML::script('js/sweetalert.min.js') }}
	    {{ HTML::script('js/validation.js') }}
	    {{ HTML::script('admin/tinymce/js/tinymce/tinymce.min.js') }}
	    {{ HTML::script('admin/js/jquery-ui.js') }}
	    {{ HTML::script('admin/js/jquery.multiselect.js') }}
	    {{ HTML::script('admin/js/main.js') }}

	    <script>
		    $(function () {
		        $('select[multiple].active.3col').multiselect({
		            columns: 1,
		            placeholder: 'Select Courses',
		            search: true,
		            searchOptions: {
		                'default': 'Search Courses'
		            },
		            selectAll: true
		        });

		    });
		</script>
	</body>
</html>