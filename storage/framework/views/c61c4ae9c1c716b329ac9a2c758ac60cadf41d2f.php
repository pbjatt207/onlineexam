	    <?php echo e(HTML::script('js/popper.min.js')); ?>

	    <?php echo e(HTML::script('js/bootstrap.min.js')); ?>

	    <?php echo e(HTML::script('js/sweetalert.min.js')); ?>

	    <?php echo e(HTML::script('js/validation.js')); ?>

	    <?php echo e(HTML::script('admin/tinymce/js/tinymce/tinymce.min.js')); ?>

	    <?php echo e(HTML::script('admin/js/jquery-ui.js')); ?>

	    <?php echo e(HTML::script('admin/js/jquery.multiselect.js')); ?>

	    <?php echo e(HTML::script('https://code.jquery.com/ui/1.12.1/jquery-ui.js')); ?>

	    <?php echo e(HTML::script('admin/js/main.js')); ?>

	    <script>
		    $(function () {
		        $('select[multiple].active.3col').multiselect({
		            columns: 1,
		            placeholder: 'Select Courses',
		            search: true,
		            searchOptions: {
		                'default': 'Search Courses'
		            },
		            selectAll: true
		        });

			    $( ".datepicker" ).datepicker({
			    	dateFormat: 'yy-mm-dd',
			    	changeYear: true,
			    	changeMonth: true,
			    	yearRange: '-100:+10'
			    });
			    $( ".datepicker_no_future" ).datepicker({
			    	dateFormat: 'yy-mm-dd',
			    	maxDate: -3650,
			    	changeYear: true,
			    	changeMonth: true,
			    	yearRange: '-100:-10'
			    });
			  });
		</script>
	</body>
</html><?php /**PATH /home3/rtportfolios/public_html/online-exam/resources/views/backend/common/footer.blade.php ENDPATH**/ ?>