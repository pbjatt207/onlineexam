<div class="page-content">
	<div class="container-fluid">
		<h1>Topic</h1>
		<div class="row">
			<div class="col-sm-12">
				<div class="card form-group">
					<h3 class="card-title"><i class="icon-filter"></i> Filter By</h3>
					<div class="card-body">
						<form>
							<?php echo csrf_field(); ?>
							<div class="row">
								<div class="col-sm-2">
									<div class="form-group">
										<label>By Keyword</label>
										<input type="text" name="search[keyword]" placeholder="By Keyword..." class="form-control" value="<?php echo e(@$search['keyword']); ?>">
									</div>
								</div>
								<!-- <div class="col-sm-2">
									<div class="form-group">
										<label>By Langauage</label>
										<select name="search[que_language]" class="form-control">
											<option value="">Both</option>
											<option value="HI" <?php echo e((@$search['que_language'] == 'HI') ? ' selected' : ''); ?>>Hindi</option>
											<option value="EN" <?php echo e((@$search['que_language'] == 'EN') ? ' selected' : ''); ?>>English</option>
										</select>
									</div>
								</div> -->
								<div class="col-sm-2">
									<div class="form-group">
										<label>By Course</label>
										<select name="search[topic_course]" class="form-control course" data-target="#searchSubject">
											<option value="">Select Course</option>
											<?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $course): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<option value="<?php echo e($course->course_id); ?>" <?php echo e((@$search['topic_course'] == $course->course_id) ? ' selected' : ''); ?>><?php echo e($course->course_name); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
										<label>By Subject</label>
										<select name="search[topic_subject]" class="form-control subject" id="searchSubject" data-target="#searchTopic">
											<option value="">Select Subject</option>
											<?php $__currentLoopData = $subjects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subject): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<option value="<?php echo e($subject->subject_id); ?>" <?php echo e((@$search['topic_subject'] == $subject->subject_id) ? ' selected' : ''); ?>><?php echo e($subject->subject_name); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
									</div>
								</div>
								<!-- <div class="col-sm-2">
									<div class="form-group">
										<label>By Topic</label>
										<select name="search[topic_topic]" class="form-control" id="searchTopic">
											<option value="">Select Topic</option>
											<?php $__currentLoopData = $topics; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $topic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<option value="<?php echo e($topic->topic_id); ?>" <?php echo e((@$search['que_topic'] == $topic->topic_id) ? ' selected' : ''); ?>><?php echo e($topic->topic_name); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
									</div>
								</div> -->
								<div class="col-sm-2">
									<label>&nbsp;</label>
									<button class="btn btn-block btn-success">Search</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="card">
					<form method="post">
						<?php echo csrf_field(); ?>
						<h3 class="card-title"><?php echo e(empty($edit->topic_id) ? "Add" : "Edit"); ?> Topic</h3>
						<div class="card-body">
							<?php if(\Session::has('success')): ?>
							    <div class="alert alert-success">
								    <?php echo \Session::get('success'); ?></li>
								</div>
							<?php endif; ?>
							<div class="form-group">
								<label>Select Course</label>
								<select name="record[topic_course]" class="form-control course" data-target="#topic_subject" required>
									<option value="">Select Course</option>
									<?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $crs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<option value="<?php echo e($crs->course_id); ?>" <?php if($crs->course_id == @$edit->topic_course): ?> selected <?php endif; ?>><?php echo e($crs->course_name); ?></option>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</select>
							</div>
							<div class="form-group">
								<label>Select Subject</label>
								<select name="record[topic_subject]" id="topic_subject" class="form-control" required>
									<option value="">Select Subject</option>
									<?php $__currentLoopData = $subjects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subj): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<option value="<?php echo e($subj->subject_id); ?>" <?php if($subj->subject_id == @$edit->topic_subject): ?> selected <?php endif; ?>><?php echo e($subj->subject_name); ?></option>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</select>
							</div>
							<div class="form-group">
								<label>Topic Name *</label>
								<input type="text" name="record[topic_name]" class="form-control" placeholder="Topic Name" value="<?php echo e(@$edit->topic_name); ?>" required>
							</div>

							<div class="form-group">
								<label>Topic Direction *</label>
								<input type="text" name="record[topic_direction]" class="form-control" placeholder="Topic Direction" value="<?php echo e(@$edit->topic_direction); ?>" required>
							</div>
							<div class="form-group">
								<label>Use Paragraph *</label>
								<div>
									<label class="animated-check">
										<input type="checkbox" name="record[topic_is_para]" value="Y" <?php if(@$edit->topic_is_para == "Y"): ?> <?php echo e('checked'); ?> <?php endif; ?>>
										<span class="label-text">Check if it is a paragraph topic.</span>
									</label>
								</div>
							</div>
							<div>
								<button class="btn btn-block btn-success">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="card">
					<form method="post">
						<?php echo csrf_field(); ?>
						<h3 class="card-title">
							<?php if(!$records->isEmpty()): ?>
							<a href="#delete_all" class="text-white float-right" title="Remove"><i class="icon-trash-o"></i></a>
							<?php endif; ?>
							View Topic
						</h3>
						<div class="card-body">
							<?php if(!$records->isEmpty()): ?>
							<div class="table-responsive">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th width="100">
												<label class="animated-check">
													<input type="checkbox" class="check_all">
													<span class="label-text">All</span>
												</label>
											</th>
											<th>SN.</th>
											<th>Name</th>
											<th>Course Name</th>
											<th>Subject Name</th>
											<th>Use Paragraph</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$sn = $offset;
										?>

										<?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rec): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<?php
											$exist = DB::table('questions')->where('que_topic', $rec->topic_id)->first();
										?>
										<tr>
											<td>
												<label class="animated-check <?php if(!empty($exist)): ?> disabled <?php endif; ?>">
													<input type="checkbox" name="check[]" value="<?php echo e($rec->topic_id); ?>" class="check" <?php if(!empty($exist)): ?> disabled <?php endif; ?>>
													<span class="label-text"></span>
												</label>
											</td>
											<td><?php echo e(++$sn); ?></td>
											<td><?php echo e($rec->topic_name); ?></td>
											<td><?php echo e($rec->course_name); ?></td>
											<td><?php echo e($rec->subject_name); ?></td>
											<td><?php echo e($rec->topic_is_para); ?></td>
											<td>
												<a href="<?php echo e(url('ea-xpanel/topic/'.$rec->topic_id)); ?>"><i class="icon-pencil"></i></a>
											</td>
										</tr>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									</tbody>
								</table>
							</div>
							<?php echo e($records->links()); ?>

							<?php else: ?>
							<div class="no_records_found">
								No record(s) found.
							</div>
							<?php endif; ?>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php /**PATH /home3/rtportfolios/public_html/online-exam/resources/views/backend/inc/topic.blade.php ENDPATH**/ ?>