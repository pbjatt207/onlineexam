<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
		<title>Login | English Aliens</title>

		<?php echo e(HTML::style('css/bootstrap.min.css')); ?>

		<?php echo e(HTML::style('icomoon/style.css')); ?>

		<?php echo e(HTML::style('admin/css/style.css')); ?>


		<link rel="icon" href="<?php echo e(url('imgs/ea_logo.ico')); ?>">
	</head>
	<body>
		<section class="full-section">
			<div class="half-section"></div>
			<div class="login-form">
				<div class="text-center form-group">
					<img src="<?php echo e(url('imgs/logo-2.png')); ?>">
				</div>
				<form id="loginForm" action="<?php echo e(url('ea-xpanel/ajax/user_login')); ?>" method="post">
					<?php echo csrf_field(); ?>
					<div class="form-msg"></div>
					<div class="form-group">
						<label>Username</label>
						<input type="text" name="record[user_login]" class="form-control" placeholder="Username" required autofocus autocomplete="off">
					</div>
					<div class="form-group">
						<label>Password</label>
						<input type="password" name="record[user_password]" class="form-control" placeholder="Password" required autocomplete="new-password">
					</div>
					<div class="form-group">
						<button class="btn btn-success btn-block">Log Me In</button>
					</div>
					<div>
						If you don't have account, then <a href="<?php echo e(url('ea-xpanel/register')); ?>">Register here</a>
					</div>
				</form>
			</div>
		</section>

		<?php echo e(HTML::script('js/jquery.min.js')); ?>

		<?php echo e(HTML::script('js/popper.min.js')); ?>

	    <?php echo e(HTML::script('js/bootstrap.min.js')); ?>

	    <?php echo e(HTML::script('js/sweetalert.min.js')); ?>

	    <?php echo e(HTML::script('js/validation.js')); ?>

	    <?php echo e(HTML::script('admin/tinymce/js/tinymce/tinymce.min.js')); ?>

	    <?php echo e(HTML::script('admin/js/main.js')); ?>

	</body>
</html><?php /**PATH /home3/rtportfolios/public_html/online-exam/resources/views/backend/login.blade.php ENDPATH**/ ?>