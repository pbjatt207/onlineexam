<div class="page-content">
	<div class="container-fluid">
		<h1>
			<a href="<?php echo e(url('ea-xpanel/test/add')); ?>" class="btn btn-success btn-sm float-right"><i class="icon-plus"></i> Add More</a>
			View Test
		</h1>
		<div class="row">
			<div class="col-sm-12">
				<div class="card form-group">
					<h3 class="card-title"><i class="icon-filter"></i> Filter By</h3>
					<div class="card-body">
						<form>
							<?php echo csrf_field(); ?>
							<div class="row">
								<div class="col-sm-2">
									<div class="form-group">
										<label>By Keyword</label>
										<input type="text" name="search[keyword]" placeholder="By Keyword..." class="form-control" value="<?php echo e(@$search['keyword']); ?>">
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
										<label>By Course</label>
										<select name="search[que_course]" class="form-control course" data-target="#searchSubject">
											<option value="">Select Course</option>
											<?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $course): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<option <?php echo e((@$search['que_course'] == $course->course_name) ? ' selected' : ''); ?> value="<?php echo e($course->course_name); ?>"><?php echo e($course->course_name); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
										<label>Difficulty Level</label>
										<select name="search[defficulty_level]" class="form-control">
											<option value="">Select Level</option>
											<option <?php echo e((@$search['defficulty_level'] == 'Beginner' ) ? ' selected' : ''); ?> value="Beginner">Beginner</option>
											<option <?php echo e((@$search['defficulty_level'] == 'Intermediate' ) ? ' selected' : ''); ?> value="Intermediate">Intermediate</option>
											<option <?php echo e((@$search['defficulty_level'] == 'Expert' ) ? ' selected' : ''); ?> value="Expert">Expert</option>
										</select>
									</div>
								</div>
								<div class="col-sm-2">
									<label>&nbsp;</label>
									<button class="btn btn-block btn-success">Search</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="card">
					<form method="post">
						<?php echo csrf_field(); ?>
						<h3 class="card-title">
							<a href="#delete_all" class="text-white float-right" title="Remove"><i class="icon-trash-o"></i></a>
							Test
						</h3>
						<div class="card-body">
							<?php if(\Session::has('success')): ?>
							    <div class="alert alert-success">
								    <?php echo \Session::get('success'); ?></li>
								</div>
							<?php endif; ?>

							<?php if(!$records->isEmpty()): ?>

							<div class="table-responsive">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th width="100">
												<label class="animated-check">
													<input type="checkbox" class="check_all">
													<span class="label-text">All</span>
												</label>
											</th>
											<th>SN.</th>
											<th>Name</th>
											<th>course</th>
											<th>Defficulty Level</th>
											<th>Duration</th>
											<th>No. of Questions</th>
											<th>Total Marks</th>
											<th>Positive Marks</th>
											<th>Negative Marks</th>
											<th>Questions</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$sn = $records->firstItem();
										?>
										<?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rec): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php
												$total_rec = DB::table('test_questions')->where('test_questions_test_id', $rec->test_id)->where('test_questions_is_deleted', 'N')->count();
											?>
										<tr>
											<td>
												<label class="animated-check">
													<input type="checkbox" name="check[]" value="<?php echo e($rec->test_id); ?>" class="check">
													<span class="label-text"></span>
												</label>
											</td>
											<td><?php echo e($sn++); ?></td>
											<td><?php echo e($rec->test_name); ?></td>
											<td><?php echo e($rec->course_name); ?></td>
											<td><?php echo e($rec->test_defficulty_level); ?></td>
											<td><?php echo e($rec->test_duration); ?></td>
											<td><?php echo e($rec->test_no_of_question); ?></td>
											<td><?php echo e($rec->test_total_marks); ?></td>
											<td><?php echo e($rec->test_positive_marks); ?></td>
											<td><?php echo e($rec->test_negative_marks); ?></td>
											<td>
												<ul style="padding-left: 0;" class="ulmore">
													<li><a href="<?php echo e(url('ea-xpanel/test/test_questions/'.$rec->test_id)); ?>"> Add |</a></li>
													<li><a href="<?php echo e(url('ea-xpanel/test/view_questions/'.$rec->test_id)); ?>"> View(<?php echo e($total_rec); ?>)</a></li>
												</ul>
											</td>
											<td>
												<a href="<?php echo e(url('ea-xpanel/test/add/'.$rec->test_id)); ?>"><i class="icon-pencil"></i></a>

												<?php if($rec->test_is_visible == 'Y'): ?>
												<a href="<?php echo e(url('ea-xpanel/test/status/N/'.$rec->test_id)); ?>" class="text-success"><i class="icon-eye"></i></a>
												<?php else: ?>
												<a href="<?php echo e(url('ea-xpanel/test/status/Y/'.$rec->test_id)); ?>" class="text-danger"><i class="icon-eye-slash"></i></a>
												<?php endif; ?>
											</td>
										</tr>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									</tbody>
								</table>
							</div>
							<?php
				                $get_param = request()->input();
				                if(isset($get_param['page'])) {
				                    unset($get_param['page']);
				                }
				            ?>
				            <?php echo e($records->appends($get_param)->links()); ?>

							<?php else: ?>
							<div class="no_records_found">
								No record(s) found.
							</div>
							<?php endif; ?>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php /**PATH /home3/rtportfolios/public_html/online-exam/resources/views/backend/inc/test.blade.php ENDPATH**/ ?>