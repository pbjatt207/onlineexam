<section class="greybg">
	<div class="container-fluid">
		<div class="m-3">
			<div class="row">
				<div class="col-12 mb-3">
					<div class="card">
						<div class="row  pt-3 pb-3">
							<div class="col-sm-6">
								<div class="card-heading pull-left ml-4">
									<h3>Result</h3>
								</div>
							</div>
							<!-- <div class="col-sm-6">
								<div class="card-heading pull-right mr-4">
									<div class="text-center">
										<i class="icon-print"></i><br>
										<a target="_blank" href="pdf-result/">Print & Save</a>
									</div>
								</div>
								<div class="card-heading pull-right">
									<div class="text-center">
										<i class="icon-envelope-o"></i><br>
										<a target="_blank" href="result/">Email Result</a>
									</div>
								</div>
							</div> -->
						</div>


						<div class="clearfix"></div>
					</div>
				</div>

				<div class="col-6 col-sm-6 col-md-6 col-lg-3 mb-3">
					<div class="card text-center">
						<div class="card-heading">
							<h3><?php echo e($correct); ?></h3>
						</div>
						<p></p>
						<div>Correct</div>
						<p>&nbsp;</p>
					</div>
				</div>

				<div class="col-6 col-sm-6 col-md-6 col-lg-3 mb-3">
					<div class="card text-center">
						<div class="card-heading">
							<h3><?php echo e($incorrect); ?></h3>
						</div>
						<p></p>
						<div>Incorrect</div>
						<p>&nbsp;</p>
					</div>
				</div>

				<div class="col-6 col-sm-6 col-md-6 col-lg-3 mb-3">
					<div class="card text-center">
						<div class="card-heading">
							<h3><?php echo e($notAnswered); ?></h3>
						</div>
						<p></p>
						<div>Not Attempted</div>
						<p>&nbsp;</p>
					</div>
				</div>

				<div class="col-6 col-sm-6 col-md-6 col-lg-3 mb-3">
					<div class="card text-center">
						<div class="card-heading">
							<h3> <?php echo e($gainmarks); ?> out of	<?php echo e($total_marks); ?> </h3>
						</div>
						<p></p>
						<div>Total Marks</div>
						<p>&nbsp;</p>
					</div>
				</div>
			</div>

			<div class="card res-summary mb-3">
				<h3 class="card-title mb-3">Result Summary</h3>
				<div class="card-heading">
					<?php $__currentLoopData = $questions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $que): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<div>
						<div class="pull-left">Que. </div>
						<div class="ml-5"><?php echo $que->que_name; ?></div>
					</div><br>
					<div class="options">
						<div class="row">
							<div class=" col-md-6 col-lg-6">
								<div>
									<div class="pull-left">a. </div>
									<div class="pull-left"><?php echo $que->que_opt1; ?></div><span class=" pull-right"></span>
								</div>
							</div>
							<div class=" col-md-6 col-lg-6">
								<div>
									<div class="pull-left">b. </div>
									<div class="pull-left"><?php echo $que->que_opt2; ?></div>
									<!-- <span class="icon-close pull-right"></span> -->
								</div>
							</div>
						</div>
						<div class="row">
							<div class=" col-md-6 col-lg-6">
								<div>
									<div class="pull-left">c. </div>
									<div class="pull-left"><?php echo $que->que_opt3; ?></div>
									<span class="pull-right"></span>
								</div>
							</div>
							<div class=" col-md-6 col-lg-6">
								<div>
									<div class="pull-left">d. </div>
									<div class="pull-left"><?php echo $que->que_opt4; ?></div>
									<div class="pull-right"></div>
								</div>
							</div>
						</div>
						<div class="mt-1">
							<strong>Correct Answer: <?php echo $que->que_answer; ?> </strong>
						</div>
					</div>
					<hr>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php /**PATH /home3/rtportfolios/public_html/online-exam/resources/views/backend/user/result.blade.php ENDPATH**/ ?>