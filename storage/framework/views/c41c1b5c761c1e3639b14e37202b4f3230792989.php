<div class="page-content">
	<div class="container-fluid">
		<h1>Welcome to Candidate Panel</h1>
		<div class="row">
			<?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $course): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<div class="col-md-6 col-lg-3">
				<div class="dashboard-box form-group">
					<div class="inrcntntdash">
						<div class="row">
							<div class="col-md-4">
								<div class="lytcntnt text-center">
									<img class="img-fluid" src="<?php echo e(url('imgs/courses.png')); ?>">
								</div>
							</div>
							<div class="col-md-8">
								<div class="rytcntnt">
									<h3><?php echo e($course->course_name); ?></h3>
									<!-- <p>Admit card of Test 1</p> -->
								</div>
							</div>
						</div>
					</div>
					<div class="greenbtn">
						<a  href="<?php echo e(url('ea-xpanel/test_details/'.$course->course_slug )); ?>" class="btn btn-green"><i class="icon-cloud_download"></i> Take a Test</a>
					</div>
				</div>
			</div>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</div>
	</div>
</div>
<?php /**PATH /home3/rtportfolios/public_html/online-exam/resources/views/backend/user/dashboard.blade.php ENDPATH**/ ?>