<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
		<title>Register | English Aliens</title>

		<?php echo e(HTML::style('css/bootstrap.min.css')); ?>

		<?php echo e(HTML::style('icomoon/style.css')); ?>

		<?php echo e(HTML::style('admin/css/jquery-ui.css')); ?>

		<?php echo e(HTML::style('admin/css/jquery.multiselect.css')); ?>

		<?php echo e(HTML::style('admin/css/style.css')); ?>


		<link rel="icon" href="<?php echo e(url('imgs/ea_logo.ico')); ?>">
	</head>
	<body>
		<section class="full-section">
			<div class="half-section"></div>
			<div class="login-form">
				<div class="text-center form-group">
					<img src="<?php echo e(url('imgs/logo-2.png')); ?>">
				</div>
				<form method="post">
					<?php echo csrf_field(); ?>
					<div class="form-msg"></div>
					<input type="hidden" value="<?php echo e($setting->site_roll_pre); ?><?php echo e($roll_number); ?>" name="record[candidate_roll_number]">
					<div class="form-group">
						<label>Name</label>
						<input type="text" name="record[user_name]" class="form-control" placeholder="Name" required autofocus autocomplete="off">
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="text" name="record[user_email]" class="form-control" placeholder="E-mail" required autofocus autocomplete="off">
					</div>
					<div class="form-group">
						<label>Mobile No.</label>
						<input type="text" name="record[user_mobile]" class="form-control" placeholder="Mobile No." required autofocus autocomplete="off">
					</div>

					<div class="form-group">
						<label>Password</label>
						<input type="password" name="record[user_password]" class="form-control password" placeholder="Password" required autocomplete="new-password">
					</div>
					<div class="form-group">
						<label>Confirm Password</label>
						<input type="password" name="record[user_confirm_password]" class="form-control confirm-password" placeholder="Confirm Password" required autocomplete="new-password">
					</div>
					<div class="form-group">
						<label>Courses</label>
						<select name="record[user_courses][]" multiple="multiple" class="3col active">
							<?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $course): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					        <option value="<?php echo e($course->course_id); ?>"><?php echo e($course->course_name); ?></option>
					        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					    </select>
					</div>
					<div class="form-group">
						<button class="btn btn-success btn-block">Register</button>
					</div>
					<div>
						If already have an account, then <a href="<?php echo e(url('ea-xpanel')); ?>">Login here</a>
					</div>
				</form>
			</div>
		</section>

		<?php echo e(HTML::script('js/jquery.min.js')); ?>

		<?php echo e(HTML::script('js/popper.min.js')); ?>

	    <?php echo e(HTML::script('js/bootstrap.min.js')); ?>

	    <?php echo e(HTML::script('js/sweetalert.min.js')); ?>

	    <?php echo e(HTML::script('js/validation.js')); ?>

	    <?php echo e(HTML::script('admin/tinymce/js/tinymce/tinymce.min.js')); ?>

	    <?php echo e(HTML::script('admin/js/jquery-ui.js')); ?>

	    <?php echo e(HTML::script('admin/js/jquery.multiselect.js')); ?>

	    <?php echo e(HTML::script('admin/js/main.js')); ?>


	    <script>
		    $(function () {
		        $('select[multiple].active.3col').multiselect({
		            columns: 1,
		            placeholder: 'Select Courses',
		            search: true,
		            searchOptions: {
		                'default': 'Search Courses'
		            },
		            selectAll: true
		        });

		    });
		</script>
	</body>
</html><?php /**PATH /home3/rtportfolios/public_html/online-exam/resources/views/backend/register.blade.php ENDPATH**/ ?>