<div class="page-content">
	<div class="container-fluid">
		<h1>Courses</h1>
		<div class="row">
			<div class="col-sm-4">
				<div class="card">
					<form method="post">
						<?php echo csrf_field(); ?>
						<h3 class="card-title"><?php echo e(empty($edit->course_id) ? "Add" : "Edit"); ?> Course</h3>
						<div class="card-body">
							<?php if(\Session::has('success')): ?>
							    <div class="alert alert-success">
								    <?php echo \Session::get('success'); ?></li>
								</div>
							<?php endif; ?>
							<div class="form-group">
								<label>Course Name *</label>
								<input type="text" name="record[course_name]" class="form-control" placeholder="Course Name" value="<?php echo e(@$edit->course_name); ?>" required>
							</div>
							<div>
								<button class="btn btn-block btn-success">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="card">
					<form method="post">
						<?php echo csrf_field(); ?>
						<h3 class="card-title">
							<?php if(!$records->isEmpty()): ?>
							<a href="#delete_all" class="text-white float-right" title="Remove"><i class="icon-trash-o"></i></a>
							<?php endif; ?>
							View Course
						</h3>
						<div class="card-body">
							<?php if(!$records->isEmpty()): ?>
							<div class="table-responsive">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th width="100">
												<label class="animated-check">
													<input type="checkbox" class="check_all">
													<span class="label-text">All</span>
												</label>
											</th>
											<th>SN.</th>
											<th>Name</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$sn = $offset;
										?>

										<?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rec): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<?php
											$exist = DB::table('questions')->where('que_course', $rec->course_id)->first();
										?>
										<tr>
											<td>
												<label class="animated-check <?php if(!empty($exist)): ?> disabled <?php endif; ?>">
													<input type="checkbox" name="check[]" value="<?php echo e($rec->course_id); ?>" class="check" <?php if(!empty($exist)): ?> disabled <?php endif; ?>>
													<span class="label-text"></span>
												</label>
											</td>
											<td><?php echo e(++$sn); ?></td>
											<td><?php echo e($rec->course_name); ?></td>
											<td>
												<a href="<?php echo e(url('ea-xpanel/courses/'.$rec->course_id)); ?>"><i class="icon-pencil"></i></a>
											</td>
										</tr>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									</tbody>
								</table>
							</div>
							<?php echo e($records->links()); ?>

							<?php else: ?>
							<div class="no_records_found">
								No record(s) found.
							</div>
							<?php endif; ?>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div><?php /**PATH /home3/rtportfolios/public_html/online-exam/resources/views/backend/inc/course.blade.php ENDPATH**/ ?>