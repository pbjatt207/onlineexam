<?php
$user_id = session('user_auth');
$user_info = DB::table('candidates')->where('candidate_user', $user_id)->first();
$curQue 	= 1;
$totsec 	= $test->test_duration * 60;
$bgQue 		= $questions[0]->que_id;
$btnText 	= "Start Test";

$user_auth = session('user_auth');

$jsonfile = "tests/{$test->test_id}/{$user_auth}.json";

if(file_exists($jsonfile)) {
	$json = file_get_contents($jsonfile);
	$arr  = json_decode($json, true);

	if(!empty($arr) && !empty($arr['totalRemains'])) {
		$totsec = $arr['totalRemains'];
	}

	if(isset($arr['option']) && !empty($arr['current_question'])) {
		$curQue = $arr['current_question'];
	}

	$btnText 	= "Resume Test";
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<base href="<?php echo e(url('/').'/'); ?>">
	<title>Test</title>
	<?php echo e(HTML::style('css/bootstrap.min.css')); ?>

	<?php echo e(HTML::style('icomoon/style.css')); ?>

	<?php echo e(HTML::style('css/style.css')); ?>

	<?php echo e(HTML::style('css/responsive.css')); ?>

	<link rel="icon" href="<?php echo e(url('site/favicon.ico')); ?>">
</head>
<body>


<div id="testInstruction">
	<div class="container-fluid">
		<div class="row">
			<div class="card">
				<div class="card_inst">
					<?php echo $inst->inst_description; ?>

				</div>
				<div class="resume_test mb-3">
					<a href="#start-test" onclick="toggleFullScreen(document.body)" id="startTestBtn" class="btn btn-success btn-sm ml-3 mt-3" style="display: none;"><?php echo e($btnText); ?> <i class="icon-keyboard_arrow_right"></i></a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php if($test->test_template == 1): ?>
	<?php echo $__env->make('backend.templates.template1', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php else: ?>
	<?php echo $__env->make('backend.templates.template2', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php endif; ?>

<div class="timeout text-center">
	<div class="timeout-bg"></div>
	<div class="timeout-body">
		<div style="height: 70%;">
			<div><img src="imgs/timeup.png" alt="Timeout" class="img"></div>
			<h2>Timeout</h2>
		</div>
		<hr>
		<p>&nbsp;</p>
		<div><a href="<?php echo e(url('ea-xpanel/feedback/'.$test->test_id)); ?>" class="btn btn-primary btn-lg" style="width: 150px;">SUBMIT TEST</a></div>
	</div>
</div>


	<?php echo e(HTML::script('js/jquery.min.js')); ?>

	<?php echo e(HTML::script('js/popper.min.js')); ?>

    <?php echo e(HTML::script('js/bootstrap.min.js')); ?>

    <?php echo e(HTML::script('js/sweetalert.min.js')); ?>

    <?php echo e(HTML::script('js/validation.js')); ?>

    <?php echo e(HTML::script('admin/tinymce/js/tinymce/tinymce.min.js')); ?>

    <?php echo e(HTML::script('admin/js/main.js')); ?>


    <script type="text/javascript">

    	<!-- Language Select -->
    	$(function(){

			$(window).on("load", function() {
				$('#startTestBtn').show();
			});

    		// Ajax Request
		    $.ajaxSetup({
		      headers: {
		          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      }
		    });
    		// Get Subject by course
    		$('.que_hindi').hide();
		    $('#Lang').on('change', function() {
		    	var val = $(this).val();
		    	if (val == 'HI') {
		    		$('.language_val').text("1");
		    	} else {
		    		$('.language_val').text("2");
		    	}

		    });
		});

    	function saveTest() {
			$("#onlineExam").trigger("submit");
		}

		function preventContent() {
			// Disable Right Click
		    // $("body").on("contextmenu",function(e){
		    //     return false;
		    // });

		    //Disable Cut Copy Paste
		    $('body').bind('cut copy paste', function (e) {
		        e.preventDefault();
		    });

		    //Disable Key Press
		    document.onkeydown = function (e) {
				e.preventDefault();
				return false;
			}
		}

		var count 	= <?php echo intval($totsec); ?> ;

		$(function() {
			// $( ".p:visible" ).css( "background", "yellow" );
			// $("#exp:visible").removeAttr('id');
			// alert();
			// Switch Language
			$(".switch-lang").on("change", function(e) {
				var lang = $(this).val();
				if(lang == "en") {
					$(".en").show();
					$(".hi").hide();
				} else if(lang == "hi") {
					$(".hi").show();
					$(".en").hide();
				} else {
					$(".hi,.en").show();
				}
			});
			// Start Test
			$("a[href='#start-test']").on("click", function(e) {
				e.preventDefault();

			     $('.p').each(function() {
					if($(this).css('display') == 'none') {
						$(this).removeAttr('id');
					}
				});
				GoInFullscreen(document.body);


				$("#testInstruction").addClass("d-none");
				$("#onlineTest").removeClass("d-none");

				var counter = setInterval(timer, 1000); //1000 will  run it every 1 second

				setInterval(saveTest, 5000);

				preventContent();
			});

			// Countdown Timer

			function timer() {
			    count = count - 1;
			    if (count == -1) {
			        clearInterval(counter);
			        return;
			    }

			    var seconds = count % 60;
			    var minutes = Math.floor(count / 60);
			    var hours = Math.floor(minutes / 60);
			    minutes %= 60;
			    hours %= 60;

			    hours 	= ("0" + hours).slice(-2);
			    minutes = ("0" + minutes).slice(-2);
			    seconds = ("0" + seconds).slice(-2);

			    document.getElementById("demo").innerHTML = hours + "h " + minutes + "m " + seconds + "s"; // watch for spelling

			    count = count >= 0 ? count : 0;

			    $("#totalRemains").val(count);

			    if(count == 0) {
			    	$(".timeout").show();
			    }
			}

			// Save Test to JSON

			$("#onlineExam").on("submit", function(e) {
				// alert();
				e.preventDefault();
				$.ajax({
					url: "ea-xpanel/ajax/save_test",
					type: "POST",
					data: $(this).serialize(),
					success: function() {
						$(".save-test").fadeIn().fadeOut(500);
					}
				});
			});

			// Number Palete
			$('[id^="num-btn-"]').on("click",function(e) {
				e.preventDefault();
				var id = $(this).attr("data-id");
				$(".que-ans").removeClass("active").addClass('d-none');
				$("#question-block-"+id).addClass("active").removeClass('d-none');

				$("#current_question").val($(this).text().slice(1));
			});

			// Clear Response
			$("#clear").on("click", function(e) {
				e.preventDefault();
				var current = $(".que-ans.active");
				var c_que 	= $("#current_question").val();

				current.find("input[type=radio], input[type=checkbox]").prop("checked", false);

				$("#num-btn-"+c_que).addClass("btn-default").removeClass("btn-danger check btn-success btn-info");
			});

			$("#clear-bank").on("click", function(e) {
				e.preventDefault();
				var current = $(".que-ans.active");
				var c_que 	= $("#current_question").val();

				current.find("input[type=radio], input[type=checkbox]").prop("checked", false);

				$("#num-btn-"+c_que).addClass("btn-default").removeClass("bank_que_box-a bank_que_box-na bank_que_box-m");
			});


			// Change Question (Mark for Review & Next)
			$("#review-next").on("click", function(e) {
				e.preventDefault();
				var current = $(".que-ans.active");
				var next 	= current.next(".que-ans");
				var c_que 	= $("#current_question").val();
				var totQue = $("#total_questions").val();

				c_que 		= parseInt(c_que);
				totQue 		= parseInt(totQue);

				console.log(c_que+'/'+totQue);

				if(totQue > c_que) {
					current.removeClass("active").addClass('d-none');
					next.addClass("active").removeClass('d-none');

					$("#current_question").val(parseInt(c_que) + 1);
				}

				if(current.find("input[type=radio]:checked").length > 0) {
					$("#num-btn-"+c_que).addClass("btn-info check").removeClass("btn-danger btn-success btn-default");
				} else {
					$("#num-btn-"+c_que).addClass("btn-info").removeClass("btn-success check btn-danger btn-default");
				}

				$("#is_marked_"+c_que).val(1);

				saveTest();
			});

			$("#review-prev").on("click", function(e) {
				e.preventDefault();
				var current = $(".que-ans.active");
				var prev 	= current.prev(".que-ans");
				var c_que 	= $("#current_question").val();
				var totQue = $("#total_questions").val();

				c_que 		= parseInt(c_que);
				totQue 		= parseInt(totQue);

				if(c_que > 1) {
					current.removeClass("active").addClass('d-none');
					prev.addClass("active").removeClass('d-none');

					$("#current_question").val(parseInt(c_que) - 1);
				}
			});

			// Save & Submit
			$("button.submit-test").on("click",function(e) {
				e.preventDefault();
				swal({
				  title: "Are you sure?",
				  text: "Once submited, you will not be able to retest this paper!",
				  icon: "warning",
				  buttons: true,
				  dangerMode: true,
				}).then((willSubmit) => {
				  if (willSubmit) {
				  	saveTest();

				    window.location = "feedback/<?php echo $test->test_id; ?>";
				  }
				});
			});

			// Change Question (Save & Next)
			$("#next-save").on("click", function(e) {
				e.preventDefault();
				var current = $(".que-ans.active");
				var next 	= current.next(".que-ans");

				var c_que 	= $("#current_question").val();
				var totQue  = $("#total_questions").val();

				c_que 		= parseInt(c_que);
				totQue 		= parseInt(totQue);

				if(totQue > c_que) {
					current.removeClass("active").addClass("d-none");
					next.addClass("active").removeClass("d-none");

					$("#current_question").val(parseInt(c_que) + 1);
				}

				if(current.find("input[type=radio]:checked").length > 0) {
					$("#num-btn-"+c_que).addClass("btn-success").removeClass("btn-danger btn-info btn-default");
				} else {
					$("#num-btn-"+c_que).addClass("btn-danger").removeClass("btn-success btn-info btn-default");
				}

				saveTest();

				if((next.length == 0 || next == undefined) && totQue > c_que) {
					var active_li = $('.subject-list').find('li.active');
					active_li.removeClass('active');
					active_li.next().trigger('click');
				}
			});

			$(document).bind('webkitfullscreenchange mozfullscreenchange fullscreenchange', function(e) {
			    var state = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen;
			    var event = state ? 'FullscreenOn' : 'FullscreenOff';
			    if(!state) {
			    	location.reload();
			    }
			});
		});

		function GoInFullscreen(element) {
			if(element.requestFullscreen)
				element.requestFullscreen();
			else if(element.mozRequestFullScreen)
				element.mozRequestFullScreen();
			else if(element.webkitRequestFullscreen)
				element.webkitRequestFullscreen();
			else if(element.msRequestFullscreen)
				element.msRequestFullscreen();
			window.moveTo(0, 0);
		}
		// var set = '<?php $i = 0; ?>
		// <?php $__currentLoopData = $questions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $que): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		// <?php $class = ""; ?>
		// <?php if(!empty($arr['option'][$que->que_id])): ?>
		// <?php
		// $class = $arr['is_marked'][$que->que_id] ? "btn-info check" : "btn-success";
		// ?>
		// <?php elseif(!empty($arr['is_marked'][$que->que_id])): ?>
		// <?php
		// $class = "btn-info";
		// ?>
		// <?php endif; ?>
		// <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?><div class="que_box <?php echo e($class); ?> dh" id="num-btn-<?php echo e(++$i); ?>" data-id="<?php echo e($que->que_id); ?>">Q<?php echo e($i); ?></div>';
	// 	$(function() {
	// 		$(".exp:visible").removeAttr('id');
	// 	// 	$(".dh:visible").css("background-color", "yellow");
	// 	// 	 // $(".dh:visible").css("background-color", "yellow");
	// 	// 	 // alert();
	// 	// 	// if($(".dh").is(":visible")){
	// 	// 	//     alert("The paragraph  is visible.");
	// 	// 	//     alert();
	// 	// 	// }
	// });
	// $(document).ready(function(){
	//
	// });
		// $(document).ready(function(){
		//   $("#dh:visible").hide();
		// });
    </script>
</body>
</html>
<?php /**PATH /home3/rtportfolios/public_html/online-exam/resources/views/backend/user/online_exam.blade.php ENDPATH**/ ?>