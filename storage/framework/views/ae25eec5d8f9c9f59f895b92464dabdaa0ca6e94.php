<div class="page-content">
	<div class="container-fluid">
		<div class="row">
			<?php if(!$test->isEmpty()): ?>
			<?php $__currentLoopData = $test; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rec): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<?php
				$isAttempted = DB::table('test_results')
						   ->where('tres_tid', $rec->test_id)
						   ->where('tres_uid', $profile->user_id)
						   ->first();
			?>
			<div class="col-md-6 col-lg-3">
				<div class="dashboard-box form-group">
					<div class="inrcntntdash">
						<div class="row">
							<div class="col-md-4">
								<div class="lytcntnt text-center">
									<img class="img-fluid" src="<?php echo e(url('imgs/courses.png')); ?>">
								</div>
							</div>
							<div class="col-md-8">
								<div class="rytcntnt">
									<h3><?php echo e($rec->test_name); ?></h3>
									<!-- <p>Admit card of Test 1</p> -->
								</div>
							</div>
						</div>
					</div>
					<div class="greenbtn">
						<?php if(!empty($isAttempted) && $profile->user_mobile != "9571342990"): ?>
						<a  href="<?php echo e(url('ea-xpanel/result/'.$isAttempted->tres_uid.'/'.$isAttempted->tres_tid)); ?>" class="btn btn-green"><i class="icon-cloud_download"></i> Test attempted</a>
						<?php else: ?>
						    <?php
						        if($profile->user_mobile != "9571342990") {
    						        DB::table('test_results')
            						   ->where('tres_tid', $rec->test_id)
            						   ->where('tres_uid', $profile->user_id)
            						   ->delete();
            				    }
						    ?>
						<a  href="<?php echo e(url('ea-xpanel/online-exam/'.$rec->test_slug)); ?>" class="btn btn-green"><i class="icon-cloud_download"></i> Start test</a>
						<?php endif; ?>

					</div>
				</div>
			</div>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			<?php else: ?>
			<div class="container-fluid">
				<div class="no_records_found">
					No test(s) found in course <strong><?php echo e($course->course_name); ?></strong>.
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php /**PATH /home3/rtportfolios/public_html/online-exam/resources/views/backend/user/test.blade.php ENDPATH**/ ?>