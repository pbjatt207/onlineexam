$(document).ready(function () {
    var baseurl   = $("#base_url").val();
    var wbaseurl  = $("#wbase_url").val();

    $('[data-toggle="tooltip"]').tooltip();

    $(".nav-icon").on("click", function() {
         	$(this).toggleClass("nav-open");
         	$(".main-navbar").toggleClass("open");

    });
    $("page-content").on('click', function() {
         alert();
         // $('.nav-icon').removeClass("nav-open");
    });

    $(".file-upload input[type=file]").change(function() {
        var target = $(this).closest(".file-upload").find("img");
        readURL(this, target);
    });

    $(".check_all").on("click", function() {
        var form = $(this).closest("form");

        form.find(".check:not([disabled])").prop( "checked", $(this).prop("checked") );
    });

    $(".check").on("click", function() {
        var form    = $(this).closest("form");
        var checked = form.find(".check:not([disabled])").length == form.find(".check:checked").length;

        form.find(".check_all").prop( "checked", checked );
    });

    $("a[href='#delete_all']").on("click", function(e) {
        e.preventDefault();

        var form = $(this).closest("form");
        if(form.find(".check:checked").length > 0) {
            swal({
              title: "Are you sure?",
              text: "Once deleted, you will not be able to recover record(s)!",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                form.trigger("submit");
              }
            });
        } else {
            swal("Warning", "Select at least one record to delete", "warning");
        }

    });

    // Add test questions
    $("#save_question").on("click", function(e) {
        e.preventDefault();

        var form = $(this).closest("form");

        if(form.find(".check:checked").length == 0) {
            swal({
              title: "Warning",
              text: "Please select at least one question to add into your test",
              icon: "warning",
            });
        } else {
          form.trigger("submit");
        }

    });

    $("a[href='#copy_all']").on("click", function(e) {
        e.preventDefault();

        var form    = $(this).closest("form");

        if(form.find(".check:checked").length > 0) {
          form.trigger("submit");
        } else {
            swal("Warning", "Select at least one record to copy", "warning");
        }
    });

    $('.qtype').on('change', function() {
        var type = $(this).val();
        if(type == 'Paragraph') {
            $('.para').removeClass('d-none');
            $('#que_topic').trigger('change');
        } else {
            $('.para').addClass('d-none');
        }
    });

    // Ajax Request
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    // Editor
    //////////// TinyMCE Editor
    tinyMCE.PluginManager.add('stylebuttons', function(editor, url) {
      ['div', 'pre', 'p', 'code', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'].forEach(function(name){
        editor.addButton("style-" + name, {
            tooltip: "Toggle " + name,
              text: name.toUpperCase(),
              onClick: function() { editor.execCommand('Open Sans', false, name); },
              onPostRender: function() {
                  var self = this, setup = function() {
                      editor.formatter.formatChanged(name, function(state) {
                          self.active(state);
                      });
                  };
                  editor.formatter ? setup() : editor.on('init', setup);
            }
        })
      });
    });
    // console.log(wbaseurl+'/public/admin/tinymce/themes/advanced/fonts/stylesheet.css');
    tinymce.init({
        selector: '.editor',
        plugins: "code, link, image, textcolor, emoticons, hr, lists, stylebuttons, charmap",
        fontsizeselect: true,
        browser_spellcheck: true,
        menubar: false,
        toolbar: 'fontsizeselect | fontselect | bold italic underline strikethrough | style-h1 style-h2 style-h3 | hr forecolor backcolor superscript subscript | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | insertfile | charmap code' ,
        // forced_root_block : 'div',
        branding: false,
        protect: [
            /\<\/?(if|endif)\>/g,  // Protect <if> & </endif>
            /\<xsl\:[^>]+\>/g,  // Protect <xsl:...>
            /\<script\:[^>]+\>/g,  // Protect <xsl:...>
            /<\?php.*?\?>/g  // Protect php code
        ],
        images_upload_credentials: true,
        file_browser_callback_types: 'image',
        image_dimensions: true,
        automatic_uploads: true,
        images_upload_url: baseurl+'/ajax/upload-image',
        relative_urls : false,
        remove_script_host : false,
        document_base_url: wbaseurl+'/public/uploads/',
        images_reuse_filename: true,
        content_css:  wbaseurl+'/public/admin/tinymce/themes/advanced/fonts/stylesheet.css',
        font_formats: "Open Sans=open_sansregular;Devlys 010=devlys_010normal;Kruti Dev 10=kruti_dev_010regular",
        images_upload_handler: function (blobInfo, success, failure) {
          var xhr, formData;

          xhr = new XMLHttpRequest();
          xhr.withCredentials = false;
          xhr.open('POST', baseurl+'/ajax/upload-image');

          var token = $('meta[name="csrf-token"]').attr('content');
          xhr.setRequestHeader("X-CSRF-Token", token);

          xhr.onload = function() {
            var json;

            if (xhr.status != 200) {
              failure('HTTP Error: ' + xhr.status);
              return;
            }

            json = JSON.parse(xhr.responseText);

            if (!json || typeof json.location != 'string') {
              failure('Invalid JSON: ' + xhr.responseText);
              return;
            }

            success(json.location);
          };

          formData = new FormData();
          formData.append('file', blobInfo.blob(), blobInfo.filename());

          xhr.send(formData);

      },
      setup: function(ed) {
        ed.on('init', function()
        {
            this.getDoc().body.style.fontSize = '12px';
            this.getDoc().body.style.fontFamily = 'open_sansregular';
        });
      }
      // sizeselect: "8pt 10pt 12pt 14pt 18pt 24pt 36pt"
    });

    // Get Subject by course
    $('.course').on('change', function() {
      var target = $(this).data('target');

      $(target).attr('disabled', 'disabled').html( $('<option />').val('').text('Loading') );

      $.ajax({
        url:  baseurl+'/ajax/get_subjects',
        type: 'POST',
        data: {
          'id': $(this).val()
        },
        success: function(res) {
          $(target).removeAttr('disabled').html( $('<option />').val('').text('Select Subject') );

          $.each(res.subjects, function(i, row) {
            $(target).append( $('<option />').val(row.subject_id).text(row.subject_name) );
          });
        }
      });
    });

    // Get Topics by subject
    $('.subject').on('change', function() {
      var target = $(this).data('target');

      $(target).attr('disabled', 'disabled').html( $('<option />').val('').text('Loading') );

      $.ajax({
        url:  baseurl+'/ajax/get_topics',
        type: 'POST',
        data: {
          'id': $(this).val()
        },
        success: function(res) {
          $(target).removeAttr('disabled').html( $('<option />').val('').text('Select Topic') );

          $.each(res.topics, function(i, row) {
            $(target).append( $('<option />').val(row.topic_id).text(row.topic_name) );
          });
        }
      });
    });

    // Get Para by topic
    $('.topic').on('change', function() {
      var target = $(this).data('target');

      $(target).attr('disabled', 'disabled').html( $('<option />').val('').text('Loading') );

      $.ajax({
        url:  baseurl+'/ajax/get_para',
        type: 'POST',
        data: {
          'id': $(this).val()
        },
        success: function(res) {
          $(target).removeAttr('disabled').html( $('<option />').val('').text('Select Essay / Paragraph') );

          $.each(res.paras, function(i, row) {
            $(target).append( $('<option />').val(row.para_id).text(row.para_name) );
          });
        }
      });
    });

    $('a[href="#save"]').on('click', function(e) {
      e.preventDefault();

      $(this).closest('form').trigger('submit');
    });


    $("#loginForm").on("submit", function(e) {
      e.preventDefault();
      var form     = $(this);
      var is_valid = form.is_valid();
      var fmsg     = form.find('.form-msg');
      var action   = form.attr('action');

      if(is_valid) {
        fmsg.addClass('alert alert-info').removeClass('alert-danger alert-success').html('Progressing, please wait...');

        $.ajax({
          url: action,
          type: 'POST',
          data: form.serialize(),
          success: function(res) {
            res = JSON.parse(res);
            console.log(res);
            if(res.status) {
              fmsg.removeClass('alert-info').addClass('alert-success').html(res.message);
              location.reload();
            } else {
              fmsg.removeClass('alert-info').addClass('alert-danger').html(res.message);
            }
          }
        });
      }
    });

    $(".file-upload input[type=file]").change(function() {
      var target = $(this).closest(".file-upload").find("img");
      readURL(this, target);
    });
    $('#test').on('change', function() {
      //  alert( this.value ); // or $(this).val()
      if(this.value == "english") {
        $('.english').show();
        $('.hindi').hide();
      } else {
        $('.english').hide();
        $('.hindi').show();
      }
    });

    // Test Sections switch
    $(document).on('click', '.subject-list ul li', function(e) {
        var section = $(this).data('section');

        $('.'+section).find('.que_box').first().trigger('click');

        $(this).addClass('active');
        $('.subject-list ul li').not(this).removeClass('active');

        $('.section-questions').not('.'+section).removeClass('active');
        $('.'+section).addClass('active');
    });
    // End Test Section switch
});

function readURL(input, target) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $(target).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function toggleFullScreen(elem) {
    // ## The below if statement seems to work better ## if ((document.fullScreenElement && document.fullScreenElement !== null) || (document.msfullscreenElement && document.msfullscreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
    if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
        if (elem.requestFullScreen) {
            elem.requestFullScreen();
        } else if (elem.mozRequestFullScreen) {
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullScreen) {
            elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        } else if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen();
        }
    } else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    }
}
