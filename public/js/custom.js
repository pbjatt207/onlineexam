(function(){
    $('#navone>li>a').on('click', function(e) {
        e.preventDefault();

        var parent = $(this).closest('li');

        parent.find('ul').slideToggle();
        $('#navone>li>ul').not(parent.find('ul')).slideUp();

        $(this).find('i.acc-icon').toggleClass('icon-plus icon-minus');
        $('#navone>li>a').not(this).find('i.acc-icon').addClass('icon-plus').removeClass('icon-minus');
    })

    $('.viewAns').on('click', function(e) {
        e.preventDefault();

        var parent = $(this).closest('.queans');

        if($(parent.find('.explanationDiv')).is(":visible")) {
            $(this).html('View Answer');
        } else {
            $(this).html('Hide Answer');
        }

        parent.find('.explanationDiv').slideToggle();
    });

     $(window).scroll(function () {
        var sticky = $('body'),
            scroll = $(window).scrollTop();

        if (scroll >= 200) {
            sticky.addClass('sticky');
            $(".logo").hide();
            $(".logo2").show();
            $(".nav-icon > span").addClass('icon-span');
            $(".navbar-nav").addClass('nav-background');
            $(".nav-icon").addClass('icon-scroll');
        } else {
            sticky.removeClass('sticky');
            $(".logo").show();
            $(".logo2").hide();
             $(".nav-icon > span").removeClass('icon-span');
            $(".navbar-nav").removeClass('nav-background');
            $(".nav-icon").removeClass('icon-scroll');
        }
    });


	 $('#return').click(function(e){ 
        e.preventDefault();
        $('html,body').animate({ scrollTop: 3 }, 1000);
        return false; 
    });

})();

$(function() {
    $(".nav-icon").on("click",function() {
       $(this).toggleClass("nav-open");
       $(".navbar-nav").toggleClass("translate"); 
       $(".navbar-brand").toggleClass("brand-translate"); 
    });
        // $(window).click(function() {
        //     $(".navbar-nav").removeClass("translate"); 
        //    $(".navbar-brand").removeClass("brand-translate");
        
        // });

        $(".nav-item > ul").slideUp();
    $(".nav-item").on("click",function() {
        $(".nav-item > ul").slideToggle();
    });
});
