<div class="page-content exams">
	<div class="container-fluid">
		<h2>My Attemped Exams</h2>
		<div class="table-responsive">
			<table class="table table-stripped table-bordered">
				<thead>
					<tr>
						<th>SN.</th>
						<th>Test Name</th>
						<th>Level</th>
						<th>Total Questions</th>
						<th>Gain Score</th>
						<th>Rank</th>
						<th>Details</th>
					</tr>
				</thead>
				<tbody>
					@php $sn = 1; @endphp
					@foreach($test as $rec)
					<tr>
						<td>{{ $sn++ }}</td>
						<td>{{ $rec->test_name }}</td>
						<td>{{ $rec->test_defficulty_level }}</td>
						<td>{{ $rec->test_no_of_question }}</td>
						<td>{{ $rec->tres_gain_marks }} out of {{ $rec->test_total_marks }}</td>
						<td>{{ $sn++ }}</td>
						<td>
							<a style="color: #555;" href="{{ url('ea-xpanel/result/'.$rec->tres_uid.'/'.$rec->tres_tid) }}">
								<strong>View Details</strong>
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>